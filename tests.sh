
test_directory="VM_Programs/tests"
asm_extension="lvm"
out_extension="out"

#
#   List of files to test - Should remain in this order, as some things rely on
#   the instructions of those preceding it
#
test_items=(
    asrt_tests
    mov_tests
    constants_tests
    directive_length_tests
    stack_tests
    stack_extended_tests
    load_store_tests
    hex_tests
    jmp_tests
    jmp_extended_tests
    std_branch_tests
    flt_branch_tests
    goto_tests
    goto_extended_tests
    math_tests
    proc_tests
    memory_core
    misc_tests
    device_tests
    bms_tests
)

#   
#   Assemble and execute each test item - Check for failures and exit fail if detected
#
function test_item {

    echo -e "<TEST ITEM : $1>\n\t"

    ./target/release/lvm asm -t "${test_directory}/${1}.${asm_extension}"

    if [ $? -eq 1 ]; then
        echo -e "\n\t<ASSEMBLER FAILURE>\n"
        exit 1
    fi

    ./target/release/lvm -r "${1}.${out_extension}"

    if [ $? -eq 1 ]; then
        echo -e "\n\t<TEST FAILURE>\n"

        # Remove the test item
        rm "${1}.${out_extension}"
        
        exit 1
    fi

    echo -e "\n<SUCCESS>\n"

    # Remove the test item
    rm "${1}.${out_extension}"
}

#
#   Iterate over each test item and call test_item
#
for item in ${test_items[@]}; do

    test_item $item 

done 

#
#   Perform the special test for extern items
#
echo -e "<TEST EXTERNS>\n\t"
./target/release/lvm asm -t "${test_directory}/extern/extern_test.${asm_extension}" -i "${test_directory}/extern/lib0.${asm_extension}" "${test_directory}/extern/sub/lib.${asm_extension}" "${test_directory}/extern/sub/lib1.${asm_extension}"

if [ $? -eq 1 ]; then
    echo -e "\n\t<ASSEMBLER FAILURE>\n"
    exit 1
fi

./target/release/lvm -r "extern_test.${out_extension}"

if [ $? -eq 1 ]; then
    echo -e "\n\t<TEST FAILURE>\n"

    # Remove the test item
    rm "extern_test.${out_extension}"
    
    exit 1
fi

echo -e "\n<SUCCESS>\n"
rm "extern_test.${out_extension}"

#
#   END
#
exit 0