/// The Lambda Virtual Machine
pub use vm::Vm;

/// An instruction generator
pub use bytegen;

/// Instructions list
pub use instructions;

/// Basic assembler for .lvm files
pub use assembler;