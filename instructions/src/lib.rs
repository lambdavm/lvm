pub const DEVICE_ID_RESP:   u32 = 0xFF_FF_FF_FF;
pub const DEVICE_ID_IO:     u32 = 0x00_00_00_00;


pub const INS_START_CODE:   u8 = 0xB1;
pub const INS_LOAD_VAR:     u8 = 0xB0;

pub const INS_LOAD_DEV:     u8 = 0xB3;

pub const INS_NOP:   u8 = 0x00;
pub const INS_DBP:   u8 = 0x01;
pub const INS_PROC:  u8 = 0x02;

pub const INS_LDB:   u8 = 0x10;
pub const INS_LDQ:   u8 = 0x11;
pub const INS_LDH:   u8 = 0x12;
pub const INS_LDW:   u8 = 0x13;

pub const INS_STB:   u8 = 0x15;
pub const INS_STQ:   u8 = 0x16;
pub const INS_STH:   u8 = 0x17;
pub const INS_STW:   u8 = 0x18;

pub const INS_MOV:   u8 = 0xAA;     // Move - The heavy lifter

pub const INS_MS:    u8 = 0xAB;     // Mark Stack
pub const INS_RS:    u8 = 0xAC;     // Reset Stack
pub const INS_UFP:   u8 = 0xAD;     // Update Frame Pointer

pub const INS_PUSHW:  u8 = 0x20;
pub const INS_POPW:   u8 = 0x21;
pub const INS_PUSHB:  u8 = 0x22;
pub const INS_POPB:   u8 = 0x23;
pub const INS_PUSHQ:  u8 = 0x24;
pub const INS_POPQ:   u8 = 0x25;
pub const INS_PUSHH:  u8 = 0x26;
pub const INS_POPH:   u8 = 0x27;
pub const INS_SIZE:  u8 = 0x28;     // Size (Of stack)
pub const INS_CAP:   u8 = 0x29;     // Capacity (Of stack)

pub const INS_LSH:   u8 = 0x30;
pub const INS_RSH:   u8 = 0x31;
pub const INS_AND:   u8 = 0x32;
pub const INS_OR:    u8 = 0x33;
pub const INS_XOR:   u8 = 0x34;
pub const INS_NOT:   u8 = 0x35;

pub const INS_ADD:   u8 = 0x40;
pub const INS_SUB:   u8 = 0x41;
pub const INS_MUL:   u8 = 0x42;
pub const INS_DIV:   u8 = 0x43;
pub const INS_POW:   u8 = 0x44;
pub const INS_MOD:   u8 = 0x45;
pub const INS_ADDF:  u8 = 0x46;
pub const INS_SUBF:  u8 = 0x47;
pub const INS_MULF:  u8 = 0x48;
pub const INS_DIVF:  u8 = 0x49;

pub const INS_BLT:   u8 = 0x50;
pub const INS_BGT:   u8 = 0x51;
pub const INS_BEQ:   u8 = 0x52;
pub const INS_BNE:   u8 = 0x53;
pub const INS_BLTF:  u8 = 0x54;
pub const INS_BGTF:  u8 = 0x55;
pub const INS_BEQF:  u8 = 0x56;
pub const INS_BNEF:  u8 = 0x57;

pub const INS_JMP:  u8 = 0x60;
pub const INS_GOTO: u8 = 0x61;
pub const INS_RET:  u8 = 0x62;

pub const INS_DIRQ: u8 = 0x70;      // Disable interrupt requests
pub const INS_EIRQ: u8 = 0x71;      // Enable interrupt requests

pub const INS_LDSB : u8 = 0x80;
pub const INS_LDSQ : u8 = 0x81;
pub const INS_LDSH : u8 = 0x82;
pub const INS_LDSW : u8 = 0x83;
pub const INS_STSB : u8 = 0x84;
pub const INS_STSQ : u8 = 0x85;
pub const INS_STSH : u8 = 0x86;
pub const INS_STSW : u8 = 0x87;
pub const INS_ES   : u8 = 0x88;     // Extend Stack
pub const INS_FP   : u8 = 0x89;     // Frame Pointer

pub const INS_LOAD  : u8 = 0x90;    // Load a memory core
pub const INS_STORE : u8 = 0x91;    // Store a memory core

pub const INS_BMS:  u8 = 0xFA;      // Beginning of Memory Space
pub const INS_WAIT: u8 = 0xFB;
pub const INS_ASNE: u8 = 0xFC;      // Assert Not Eq
pub const INS_ASEQ: u8 = 0xFD;      // Assert Eq
pub const INS_HALT: u8 = 0xFE;      // HALT
pub const INS_EXIT: u8 = 0xFF;      // EXIT