extern crate instructions;
use instructions::*;

extern crate derive_more;
use derive_more::{Display};

#[derive(Display)]
pub enum MemoryInstructions {

    #[display(fmt = "LDB >> lhs: {} | rhs: {}", lhs, rhs)]
    Ldb{ lhs: u8, rhs: u8 },

    #[display(fmt = "LDQ >> lhs: {} | rhs: {}", lhs, rhs)]
    Ldq{ lhs: u8, rhs: u8 },

    #[display(fmt = "LDH >> lhs: {} | rhs: {}", lhs, rhs)]
    Ldh{ lhs: u8, rhs: u8 },

    #[display(fmt = "LDW >> lhs: {} | rhs: {}", lhs, rhs)]
    Ldw{ lhs: u8, rhs: u8 },

    #[display(fmt = "STB >> lhs: {} | rhs: {}", lhs, rhs)]
    Stb{ lhs: u8, rhs: u8 },

    #[display(fmt = "STQ >> lhs: {} | rhs: {}", lhs, rhs)]
    Stq{ lhs: u8, rhs: u8 },

    #[display(fmt = "STH >> lhs: {} | rhs: {}", lhs, rhs)]
    Sth{ lhs: u8, rhs: u8 },

    #[display(fmt = "STW >> lhs: {} | rhs: {}", lhs, rhs)]
    Stw{ lhs: u8, rhs: u8 },

    #[display(fmt = "LOAD >> SIZE: {} | POS: {}", size, pos)]
    Load{ size: u8, pos: u8 },

    #[display(fmt = "STORE >> SIZE: {} | POS: {}", size, pos)]
    Store{ size: u8, pos: u8 },
}

#[derive(Display)]
pub enum MoveInstructions {

    #[display(fmt = "MOV : Variant '1' >> dest: {} | source reg: {}", dest_reg, source_reg)]
    MovVariantOne{ dest_reg: u8, source_reg: u8},
    
    #[display(fmt = "MOV : Variant '2' >> dest: {} | numerical : {}", dest_reg, numerical)]
    MovVariantTwo{ dest_reg: u8, numerical: i64}
}

#[derive(Display)]
pub enum StackInstructions {

    #[display(fmt = "PUSHW >> reg: {}", reg)]
    Pushw { reg: u8 },

    #[display(fmt = "POPW >> reg: {}", reg)]
    Popw  { reg: u8 },

    #[display(fmt = "PUSHB >> reg: {}", reg)]
    Pushb { reg: u8 },

    #[display(fmt = "POPB >> reg: {}", reg)]
    Popb  { reg: u8 },

    #[display(fmt = "PUSHQ >> reg: {}", reg)]
    Pushq { reg: u8 },

    #[display(fmt = "POPQ >> reg: {}", reg)]
    Popq  { reg: u8 },

    #[display(fmt = "PUSHH >> reg: {}", reg)]
    Pushh { reg: u8 },

    #[display(fmt = "POPH >> reg: {}", reg)]
    Poph  { reg: u8 },

    #[display(fmt = "SIZE >> reg: {}", reg)]
    Size { reg: u8 },

    #[display(fmt = "CAP >> reg: {}", reg)]
    Cap  { reg: u8 },

    #[display(fmt = "MS")]
    Ms,

    #[display(fmt = "RS")]
    Rs,

    #[display(fmt = "Es >> reg: {}", reg)]
    Es  { reg: u8 },

    #[display(fmt = "FP >> reg: {}", reg)]
    Fp  { reg: u8 },

    #[display(fmt = "UFP >> reg: {}", reg)]
    Ufp  { reg: u8 },

    #[display(fmt = "LDSB >> lhs: {} | rhs: {}", lhs, rhs)]
    Ldsb{ lhs: u8, rhs: u8 },

    #[display(fmt = "LDSQ >> lhs: {} | rhs: {}", lhs, rhs)]
    Ldsq{ lhs: u8, rhs: u8 },

    #[display(fmt = "LDSH >> lhs: {} | rhs: {}", lhs, rhs)]
    Ldsh{ lhs: u8, rhs: u8 },

    #[display(fmt = "LDSW >> lhs: {} | rhs: {}", lhs, rhs)]
    Ldsw{ lhs: u8, rhs: u8 },

    #[display(fmt = "STSB >> lhs: {} | rhs: {}", lhs, rhs)]
    Stsb{ lhs: u8, rhs: u8 },

    #[display(fmt = "STSQ >> lhs: {} | rhs: {}", lhs, rhs)]
    Stsq{ lhs: u8, rhs: u8 },

    #[display(fmt = "STSH >> lhs: {} | rhs: {}", lhs, rhs)]
    Stsh{ lhs: u8, rhs: u8 },

    #[display(fmt = "STSW >> lhs: {} | rhs: {}", lhs, rhs)]
    Stsw{ lhs: u8, rhs: u8 },
}

#[derive(Display)]
pub enum BitwiseInstructions {

    #[display(fmt = "LSH >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Lsh  { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "RSH >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Rsh  { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "AND >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    And  { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "OR >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Or   { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "XOR >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Xor  { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "NOT >> dest: {} | source: {} ", dest, source)]
    Not  { dest: u8, source: u8 }
}

#[derive(Display)]
pub enum ArithmeticInstructions {

    #[display(fmt = "ADD >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Add  { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "SUB >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Sub  { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "MUL >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Mul  { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "DIV >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Div  { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "POW >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Pow  { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "MOD >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Mod  { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "ADDF >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Addf { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "SUBF >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Subf { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "MULF >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Mulf { dest: u8, lhs: u8, rhs: u8 },

    #[display(fmt = "DIVF >> dest: {} | lhs: {} | rhs: {} ", dest, lhs, rhs)]
    Divf { dest: u8, lhs: u8, rhs: u8 },
}

#[derive(Display)]
pub enum BranchInstructions {

    #[display(fmt = "BLT >> lhs: {} | rhs: {} | dest: {} ", lhs, rhs, dest)]
    Blt { lhs: u8, rhs: u8, dest: u64 },
    
    #[display(fmt = "BGT >> lhs: {} | rhs: {} | dest: {} ", lhs, rhs, dest)]
    Bgt { lhs: u8, rhs: u8, dest: u64 },
    
    #[display(fmt = "BEQ >> lhs: {} | rhs: {} | dest: {} ", lhs, rhs, dest)]
    Beq { lhs: u8, rhs: u8, dest: u64 },
    
    #[display(fmt = "BNE >> lhs: {} | rhs: {} | dest: {} ", lhs, rhs, dest)]
    Bne { lhs: u8, rhs: u8, dest: u64 },
    
    #[display(fmt = "BLTF >> lhs: {} | rhs: {} | dest: {} ", lhs, rhs, dest)]
    Bltf{ lhs: u8, rhs: u8, dest: u64 },
    
    #[display(fmt = "BGTF >> lhs: {} | rhs: {} | dest: {} ", lhs, rhs, dest)]
    Bgtf{ lhs: u8, rhs: u8, dest: u64 },
    
    #[display(fmt = "BEQF >> lhs: {} | rhs: {} | dest: {} ", lhs, rhs, dest)]
    Beqf{ lhs: u8, rhs: u8, dest: u64 },
    
    #[display(fmt = "BNEF >> lhs: {} | rhs: {} | dest: {} ", lhs, rhs, dest)]
    Bnef{ lhs: u8, rhs: u8, dest: u64 },
}

#[derive(Display)]
pub enum JumpReturnInstructions {

    #[display(fmt = "JMP : Variant '1' >> source reg: {}", source_reg)]
    JmpVariantOne{ source_reg: u8},
    
    #[display(fmt = "JMP : Variant '2' >> source label: {}", numerical)]
    JmpVariantTwo{ numerical: u64},
    
    #[display(fmt = "RET")]
    Ret,

    #[display(fmt = "GOTO : Variant '1' >> source reg: {}", source_reg)]
    GotoVariantOne{ source_reg: u8},
    
    #[display(fmt = "GOTO : Variant '2' >> source label: {}", numerical)]
    GotoVariantTwo{ numerical: u64},
}

#[derive(Display)]
pub enum MiscInstructions {

    #[display(fmt = "NOP")]
    Nop,

    #[display(fmt = "EXIT")]
    Exit,

    #[display(fmt = "HALT")]
    Halt,

    #[display(fmt = "DBP")]
    Dbp,

    #[display(fmt = "PROC >> dest: {} ", dest)]
    Proc { dest: u64 },

    #[display(fmt = "WAIT >> Millis: {} ", time)]
    Wait { time: u8 },

    #[display(fmt = "BMS >> Dest: {} ", dest)]
    Bms { dest: u8 },
}


#[derive(Display)]
pub enum AssertInstructions {

    #[display(fmt = "ASEQ >> lhs: {} | rhs: {}", lhs, rhs)]
    Aseq{ lhs: u8, rhs: u8 },

    #[display(fmt = "ASNE >> lhs: {} | rhs: {}", lhs, rhs)]
    Asne{ lhs: u8, rhs: u8 },
}

#[derive(Display)]
pub enum InterruptInstructions {

    #[display(fmt = "DIRQ")]
    Dirq,

    #[display(fmt = "EIRQ")]
    Eirq
}

/*
    The following two methods are pass-throughs that allow an assembler /
    code generator to rely only on the byte generator as-apposed to needing
    to work with raw instructions.
*/

pub fn load_var_instruction() -> u8 {
    return INS_LOAD_VAR;
}

pub fn start_code_instruction() -> u8 {
    return INS_START_CODE;
}

pub fn load_device_id(id: u32) -> Vec<u8> {
    let mut v = Vec::<u8>::new();
    v.push(INS_LOAD_DEV);
    v.extend(id.to_le_bytes().to_vec());
    return v;
}

pub fn load_irq_addr(addr: u64) -> Vec<u8> {
    let mut v = Vec::<u8>::new();
    v.extend(addr.to_le_bytes().to_vec());
    return v;
}

pub fn load_dev_flag(flag: u8) -> Vec<u8> {
    let mut v = Vec::<u8>::new();
    v.extend(flag.to_le_bytes().to_vec());
    return v;
}

pub fn load_dev_buff(buff_len: u32, buff_loc: u64) -> Vec<u8> {
    let mut v = Vec::<u8>::new();
    v.extend(buff_len.to_le_bytes().to_vec());
    v.extend(buff_loc.to_le_bytes().to_vec());
    return v;
}

impl MemoryInstructions {

    pub fn encode(&self) -> Vec<u8> {

        //println!("{}", *self);

        let ins : u8;
        let first : u8;
        let second : u8;
        match *self {
            MemoryInstructions::Ldb{ lhs,  rhs }    => { ins = INS_LDB;   first = lhs;  second = rhs; }
            MemoryInstructions::Ldq{ lhs,  rhs }    => { ins = INS_LDQ;   first = lhs;  second = rhs; }
            MemoryInstructions::Ldh{ lhs,  rhs }    => { ins = INS_LDH;   first = lhs;  second = rhs; }
            MemoryInstructions::Ldw{ lhs,  rhs }    => { ins = INS_LDW;   first = lhs;  second = rhs; }
            MemoryInstructions::Stb{ lhs,  rhs }    => { ins = INS_STB;   first = lhs;  second = rhs; }
            MemoryInstructions::Stq{ lhs,  rhs }    => { ins = INS_STQ;   first = lhs;  second = rhs; }
            MemoryInstructions::Sth{ lhs,  rhs }    => { ins = INS_STH;   first = lhs;  second = rhs; }
            MemoryInstructions::Stw{ lhs,  rhs }    => { ins = INS_STW;   first = lhs;  second = rhs; }
            MemoryInstructions::Store{ size,  pos } => { ins = INS_STORE; first = size; second = pos; }
            MemoryInstructions::Load { size,  pos } => { ins = INS_LOAD;  first = size; second = pos; }
        }
        let mut r : Vec<u8> = Vec::new();
        r.push(ins); 
        r.push(first); 
        r.push(second);
        return r;
    }
}

impl MoveInstructions {

    pub fn encode(&self) -> Vec<u8> {

        //println!("{}", *self);
        
        let mut r : Vec<u8> = Vec::new();
        r.push(INS_MOV);

        match *self {
            MoveInstructions::MovVariantOne{ dest_reg, source_reg } => { 
                r.push(0 as u8);
                r.push(dest_reg);
                r.push(source_reg);
             },

             MoveInstructions::MovVariantTwo{ dest_reg, numerical } => {
                r.push(1 as u8);
                r.push(dest_reg);
                r.extend(( numerical as u64 ).to_le_bytes().to_vec());
             }
        }
        return r;
    }
}

impl StackInstructions {

    pub fn encode(&self) -> Vec<u8> {

        //println!("{}", *self);

        let ins : u8;
        let register :u8;
        let mut r : Vec<u8> = Vec::new();
        match *self {
            StackInstructions::Pushw { reg } => { ins = INS_PUSHW;  register = reg; }
            StackInstructions::Popw  { reg } => { ins = INS_POPW;   register = reg; }
            StackInstructions::Pushb { reg } => { ins = INS_PUSHB;  register = reg; }
            StackInstructions::Popb  { reg } => { ins = INS_POPB;   register = reg; }
            StackInstructions::Pushq { reg } => { ins = INS_PUSHQ;  register = reg; }
            StackInstructions::Popq  { reg } => { ins = INS_POPQ;   register = reg; }
            StackInstructions::Pushh { reg } => { ins = INS_PUSHH;  register = reg; }
            StackInstructions::Poph  { reg } => { ins = INS_POPH;   register = reg; }
            StackInstructions::Size  { reg } => { ins = INS_SIZE;   register = reg; }
            StackInstructions::Cap   { reg } => { ins = INS_CAP;    register = reg; }
            StackInstructions::Ms            => { r.push(INS_MS); return r;  }
            StackInstructions::Rs            => { r.push(INS_RS); return r;  }
            StackInstructions::Es  { reg }       => { r.push(INS_ES);   r.push(reg); return r;  }
            StackInstructions::Fp  { reg }       => { r.push(INS_FP);   r.push(reg); return r;  }
            StackInstructions::Ufp { reg }       => { r.push(INS_UFP);  r.push(reg); return r;  }
            StackInstructions::Ldsb{ lhs,  rhs } => { r.push(INS_LDSB); r.push(lhs); r.push(rhs); return r; }
            StackInstructions::Ldsq{ lhs,  rhs } => { r.push(INS_LDSQ); r.push(lhs); r.push(rhs); return r; }
            StackInstructions::Ldsh{ lhs,  rhs } => { r.push(INS_LDSH); r.push(lhs); r.push(rhs); return r; }
            StackInstructions::Ldsw{ lhs,  rhs } => { r.push(INS_LDSW); r.push(lhs); r.push(rhs); return r; }
            StackInstructions::Stsb{ lhs,  rhs } => { r.push(INS_STSB); r.push(lhs); r.push(rhs); return r; }
            StackInstructions::Stsq{ lhs,  rhs } => { r.push(INS_STSQ); r.push(lhs); r.push(rhs); return r; }
            StackInstructions::Stsh{ lhs,  rhs } => { r.push(INS_STSH); r.push(lhs); r.push(rhs); return r; }
            StackInstructions::Stsw{ lhs,  rhs } => { r.push(INS_STSW); r.push(lhs); r.push(rhs); return r; }
        }
        r.push(ins);
        r.push(register);
        return r;
    }
}

impl BitwiseInstructions {

    pub fn encode(&self) -> Vec<u8> {

        //println!("{}", *self);

        let ins : u8;
        let dr  : u8;
        let lr  : u8;
        let rr  : u8;
        let mut r : Vec<u8> = Vec::new();
        match *self {
            BitwiseInstructions::Lsh { dest, lhs, rhs } => { ins = INS_LSH; dr = dest; lr = lhs; rr = rhs; }
            BitwiseInstructions::Rsh { dest, lhs, rhs } => { ins = INS_RSH; dr = dest; lr = lhs; rr = rhs; }
            BitwiseInstructions::And { dest, lhs, rhs } => { ins = INS_AND; dr = dest; lr = lhs; rr = rhs; }
            BitwiseInstructions::Or  { dest, lhs, rhs } => { ins = INS_OR;  dr = dest; lr = lhs; rr = rhs; }
            BitwiseInstructions::Xor { dest, lhs, rhs } => { ins = INS_XOR; dr = dest; lr = lhs; rr = rhs; }
            BitwiseInstructions::Not { dest,   source } => { 
                r.push(INS_NOT);
                r.push(dest);
                r.push(source);
                return r;
            }
        }
        r.push(ins);
        r.push(dr);
        r.push(lr);
        r.push(rr);
        return r;
    }
}

impl ArithmeticInstructions {

    pub fn encode(&self) -> Vec<u8> {

        //println!("{}", *self);

        let ins : u8;
        let dr  : u8;
        let lr  : u8;
        let rr  : u8;
        let mut r : Vec<u8> = Vec::new();
        match *self {
            ArithmeticInstructions::Add  { dest, lhs, rhs } => { ins = INS_ADD;  dr = dest; lr = lhs; rr = rhs; }
            ArithmeticInstructions::Sub  { dest, lhs, rhs } => { ins = INS_SUB;  dr = dest; lr = lhs; rr = rhs; }
            ArithmeticInstructions::Mul  { dest, lhs, rhs } => { ins = INS_MUL;  dr = dest; lr = lhs; rr = rhs; }
            ArithmeticInstructions::Div  { dest, lhs, rhs } => { ins = INS_DIV;  dr = dest; lr = lhs; rr = rhs; }
            ArithmeticInstructions::Pow  { dest, lhs, rhs } => { ins = INS_POW;  dr = dest; lr = lhs; rr = rhs; }
            ArithmeticInstructions::Mod  { dest, lhs, rhs } => { ins = INS_MOD;  dr = dest; lr = lhs; rr = rhs; }
            ArithmeticInstructions::Addf { dest, lhs, rhs } => { ins = INS_ADDF; dr = dest; lr = lhs; rr = rhs; }
            ArithmeticInstructions::Subf { dest, lhs, rhs } => { ins = INS_SUBF; dr = dest; lr = lhs; rr = rhs; }
            ArithmeticInstructions::Mulf { dest, lhs, rhs } => { ins = INS_MULF; dr = dest; lr = lhs; rr = rhs; }
            ArithmeticInstructions::Divf { dest, lhs, rhs } => { ins = INS_DIVF; dr = dest; lr = lhs; rr = rhs; }
        }
        r.push(ins);
        r.push(dr);
        r.push(lr);
        r.push(rr);
        return r;
    }
}

impl BranchInstructions {

    pub fn encode(&self) -> Vec<u8> {

        //println!("{}", *self);

        let ins : u8;
        let dr  : u64;
        let lr  : u8;
        let rr  : u8;
        let mut r : Vec<u8> = Vec::new();
        match *self {
            BranchInstructions::Blt { lhs, rhs, dest } => { ins = INS_BLT;  dr = dest; lr = lhs; rr = rhs; }
            BranchInstructions::Bgt { lhs, rhs, dest } => { ins = INS_BGT;  dr = dest; lr = lhs; rr = rhs; }
            BranchInstructions::Beq { lhs, rhs, dest } => { ins = INS_BEQ;  dr = dest; lr = lhs; rr = rhs; }
            BranchInstructions::Bne { lhs, rhs, dest } => { ins = INS_BNE;  dr = dest; lr = lhs; rr = rhs; }
            BranchInstructions::Bltf{ lhs, rhs, dest } => { ins = INS_BLTF; dr = dest; lr = lhs; rr = rhs; }
            BranchInstructions::Bgtf{ lhs, rhs, dest } => { ins = INS_BGTF; dr = dest; lr = lhs; rr = rhs; }
            BranchInstructions::Beqf{ lhs, rhs, dest } => { ins = INS_BEQF; dr = dest; lr = lhs; rr = rhs; }
            BranchInstructions::Bnef{ lhs, rhs, dest } => { ins = INS_BNEF; dr = dest; lr = lhs; rr = rhs; }
        }
        r.push(ins);
        r.push(lr);
        r.push(rr);
        r.extend(dr.to_le_bytes().to_vec());
        return r;
    }
}

impl JumpReturnInstructions {

    pub fn encode(&self) -> Vec<u8> {

     //   println!("{}", *self);

        let mut r : Vec<u8> = Vec::new();
        match *self {
            JumpReturnInstructions::JmpVariantOne{ source_reg } => { 
                r.push(INS_JMP);
                r.push(0 as u8);
                r.push(source_reg);
            },
             JumpReturnInstructions::JmpVariantTwo{ numerical } => {
                r.push(INS_JMP);
                r.push(1 as u8);
                r.extend(( numerical ).to_le_bytes().to_vec());
            }
            JumpReturnInstructions::GotoVariantOne{ source_reg } => { 
                r.push(INS_GOTO);
                r.push(0 as u8);
                r.push(source_reg);
            },
             JumpReturnInstructions::GotoVariantTwo{ numerical } => {
                r.push(INS_GOTO);
                r.push(1 as u8);
                r.extend(( numerical ).to_le_bytes().to_vec());
            }
            JumpReturnInstructions::Ret          => { r.push(INS_RET); },
        }

        return r;
    }
}

impl AssertInstructions {

    pub fn encode(&self) -> Vec<u8> {

        //println!("{}", *self);

        let mut r : Vec<u8> = Vec::new();
        match *self {
            AssertInstructions::Asne{ lhs, rhs } => { r.push(INS_ASNE); r.extend(lhs.to_le_bytes().to_vec()); r.extend(rhs.to_le_bytes().to_vec());  },
            AssertInstructions::Aseq{ lhs, rhs } => { r.push(INS_ASEQ); r.extend(lhs.to_le_bytes().to_vec()); r.extend(rhs.to_le_bytes().to_vec());  }
        }
        return r;
    }
}

impl MiscInstructions {

    pub fn encode(&self) -> Vec<u8> {

        //println!("{}", *self);

        let mut r : Vec<u8> = Vec::new();
        match *self {
            MiscInstructions::Nop  => { r.push(INS_NOP);  },
            MiscInstructions::Exit => { r.push(INS_EXIT); },
            MiscInstructions::Halt => { r.push(INS_HALT); },
            MiscInstructions::Dbp  => { r.push(INS_DBP);  },
            MiscInstructions::Proc{ dest }     => { r.push(INS_PROC); r.extend(dest.to_le_bytes().to_vec()); },
            MiscInstructions::Wait{ time }     => { r.push(INS_WAIT); r.extend(time.to_le_bytes().to_vec()); },
            MiscInstructions::Bms { dest }     => { r.push(INS_BMS);  r.extend(dest.to_le_bytes().to_vec()); },
        }
        return r;
    }
}

impl InterruptInstructions {

    pub fn encode(&self) -> Vec<u8> {

        //println!("{}", *self);

        let mut r : Vec<u8> = Vec::new();
        match *self {
            InterruptInstructions::Dirq  => { r.push(INS_DIRQ);  },
            InterruptInstructions::Eirq  => { r.push(INS_EIRQ);  },
        }
        return r;
    }
}