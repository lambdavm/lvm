# IO Device

The IO device is triggered by the device **.flg** and will issue interrupts while utilizing input and output buffers. 
This means that for proper usage all available device directives should be declared. 

**ID** : 0 

| Instruction    |    Description       |
|-               |-                     |
|  Read          |     Read data in     |
|  Write         |     Write out data   |

## Read

OpCode : 0x10

Expected Input Buffer : 

              [ ---- Unsigned 32-bit (le) num chars ---- ]
    OPCODE    00000000 | 00000000 | 00000000 | 00000000 

Resulting input will be within the device's output buffer following the reader's OpCode. If no input is had. The output buffer will remain empty. When complete the device will issue an interrupt.

## Write 

OpCode : 0x11

Destinations : 

| Destination  | Code   |
|-             |-       |
| Stdout       | 0x00   |
| StdErr       | 0x01   |

Expected Input Buffer :

    OPCODE  |  DESTINATION | CALLBACK | ... | ... | ...

Upon writing the data out, the device will write (1) to the first byte of the output buffer if success and (0) if failure. 

The write command will only issue a callback if the CALLBACK field is set to (1) any other value will be interpreted as no callback requested.

## Callbacks 

Since only one callback is permitted per device the IO Device will place the first byte of the output buffer to the corresponding OpCode of the command that filled it. Therefore if the reader issues the callback the first byte will be **0x10**, and similarly if the writer issues the callback the first byte will be **0x11**.