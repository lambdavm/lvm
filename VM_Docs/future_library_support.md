To enable the easy building of libraries on the LVM the Assembler will need to be updated to change its output. We will need to come up with a file format for that describes the files and puts it into a "staging" area. The new output would not be directly runnable, rather, a separate tool will then be ran on this staged file to build the final resulting executable. The reasons for this is the means by which the bytecode is generated no doesn't lend well to being merged with existing bytecode files. 

So, we either :

    *.lvm -> Assembler -> *.saf -> Builder -> *.out 

Or 

We can not do any of this with the assembler, and instead, leave the assembly / linking for the higher level language that will be built on top of the lvm. 

I'm leaning towards the latter as the current assembler / lvm files were just to help get the VM running and to test its functionality. 

The HLL could have an assembler that takes care of the .saf and .out translations of files.