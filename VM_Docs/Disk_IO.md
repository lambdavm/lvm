```

        NOTE: 


        This device has not been implemented. It is the documentation of how I expect the DISK device to work.


```


# Disk IO Device

The IO device is triggered by the device **.flg** and will issue interrupts while utilizing input and output buffers. 
This means that for proper usage all available device directives should be declared. 

**ID** : 1

| Instruction    |    Description       |
|-               |-                     |
|  Read-All      |     Read data in     |
|  Write         |     Write out data   |
|  Append        |     Append out data  |

## Read

OpCode : 0x10

Expected Read-All Buffer : 

             [-------------------------------- File Name Address ---------------------------------]
    OPCODE  | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 
    
            [-------------- File Name Len (Bytes)------]
            |  00000000 | 00000000 | 00000000 | 00000000 

            [-------------------------------- Memory Address Output ------------------------------]
            | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 


The first word of the buffer will be where in memory the string for the file location exists. 
The next half-word will be an unsigned 32-bit integer detailing the length of the path string.
The last word of the buffer will be the start address of where data read-in will be stored

Upon Completion The output buffer will contain the following:

   OPCODE  SUCCESS  [---------- Data read-in len (Bytes) ----]
           0000000  00000000 | 00000000 | 00000000 | 00000000 

The read will trigger an interrupt stating with the first byte the reader's opcode so the caller knows that its a reader response,
followed by a byte stating if writing was a success (1=success|0=failure). The last half word will be a 32-bit unsigned integer 
stating the length of the data (in bytes) that was read in.

## Write 

OpCode : 0x20

Expected Input Buffer :

             [-------------------------------- File Name Address ---------------------------------]
    OPCODE  | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 
    
            [-------------- File Name Len (Bytes)------]
            |  00000000 | 00000000 | 00000000 | 00000000 

            [-------------------------------- Memory Address Input -------------------------------]
            | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 

            [-------------- Data Len (Bytes) --0-------]
            |  00000000 | 00000000 | 00000000 | 00000000 

The first word will be the address where the file name starts, followed by a half-word representing a 32-bit unsigned word stating
how long the file name is. The next word will contain the source of the data to write out followed by a a 32-bit unsigned word stating
how many bytes to retrieve from memory for writing.

Upon Completion The output buffer will contain the following:

   OPCODE  SUCCESS 
           0000000 

The write will trigger an interrupt stating with the first byte the writer's opcode so the caller knows that its a writer response,
followed by a byte stating if writing was a success (1=success|0=failure).

## Append 

OpCode : 0x30

Expected Input Buffer :

             [-------------------------------- File Name Address ---------------------------------]
    OPCODE  | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 
    
            [-------------- File Name Len (Bytes)------]
            |  00000000 | 00000000 | 00000000 | 00000000 

            [-------------------------------- Memory Address Input -------------------------------]
            | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 | 00000000 

            [-------------- Data Len (Bytes) --0-------]
            |  00000000 | 00000000 | 00000000 | 00000000 

The first word will be the address where the file name starts, followed by a half-word representing a 32-bit unsigned word stating
how long the file name is. The next word will contain the source of the data to write out followed by a a 32-bit unsigned word stating
how many bytes to retrieve from memory for writing.

Upon Completion The output buffer will contain the following:

   OPCODE  SUCCESS 
           0000000 

The write will trigger an interrupt stating with the first byte the appender's opcode so the caller knows that its a appender response,
followed by a byte stating if writing was a success (1=success|0=failure).

## Callbacks 

Since only one callback is permitted per device the Disk IO Device will place the first byte of the output buffer to the corresponding OpCode of the command that filled it. Therefore if the reader issues the callback the first byte will be **0x10**, and similarly if the writer issues the callback the first byte will be **0x20**, etc.