# Memory Core


The VM "Memory Core" MC is processor-specific memory outside of the stack which allows chunks of the stack to be written out, saved, and then later restored to the stack as it was found. The Memory core is represented by a HashMap of unsigned 32 bit integers that map to a vector of bytes. This means that any  number between 0 and u32::MAX can be used to store any number of bytes off the stack. 

Again, this memory is processor specific, which means a couple of things. It means that it can't be accessed by a different processor, which also means it doesn't have any need to check for thread safety. its fast local memory that can be used to preserve the stack. If the MC is accessed at an index that holds nothing, it will return nothing, if it is asked to retrieve more bytes from an index that it currently contains, the bytes it DOES contain will be written to the stack.

The MC can be accessed by the *LOAD* and *STORE* commands shown in the bytecode markdown file.