Importing files on the ASM levels should consist of the following changes to directives : 

- .extern should be used to denote the use of an external .lvm file. 

- Imported files should not have a .init, rather they should have a .lib that specifies its a lib. Handing a name to the .lib at this time doesn't seem required

Aside from the addition of .extern and .lib instructions, no more instructions should be required. 

The imports of LVM to LVM files are not meant to be anything more than just specifying external locations of other assembly code . 