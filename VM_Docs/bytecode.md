
# LVM


## Program Encoding 

**All** bytecode must be in 'little endian' format to be considered compliant. 

The first 8 bytes of an executable must point to the .code directive. 
The next 8 bytes must be the entry point address
The next byte must be the processor count

Iff there are any directives indicating the storage of variables, the wil be be next in the bytecode.

Each variable directive must start with the Opcode **0xB0** and then a 4-byte unsigned number will indicate the length (in bytes) to be read in for that particular variable. 

Iff there is any device information to setup the OpCode **0xB3** will be present here, followed by the following information:

     4 byte device ID 
     8 byte address for interrupt request address
     1 byte device flag
     4 bytes indicating length of buffer 
     8 bytes indicating address of buffer

If the device does not have an interrupt address assigned, a value of all FFs should be present.
Similarly, if no flag is set, the device flag should be set to FF.

(Note: the above information is for project encoding. If the device is not to be used, do not include the directives)

Following all [0xB1, LENGTH, VAR_DATA, DEV_DATA] information the **0xB1** Opcode must exist to mark the beginning of code (i.e the .code directive)

All data after the encoded entry point encoding will be instructions that follow instruction definitions. The LVM will be able to use the instruction Opcodes to determine length of any following information. 

---

## Directives 

Required directives are 'init' 'scap' 'hcap' and 'code' directives. 

| Directive | Argument 1  | Argument 2  | Description
|-          |-            |-            |- 
| .init     | Label Name  |    NA       | Marks first execution area within block
| .proc     |   Value     |    NA       | Unsigned 8-bit Integer describing processor count (default = 1)
| .i8       |    Name     |   Value     | 8-bit Integer
| .i16      |    Name     |   Value     | 16-bit Integer
| .i32      |    Name     |   Value     | 32-bit Integer
| .i64      |    Name     |   Value     | 64-bit Integer
| .flt      |    Name     |   Value     | Float
| .str      |    Name     |   Value     | String
| .res      |    Name     |   Value     | Reserve 'Value' number of bytes encoded as all zeros for general use
| .const    |    Name     |   Value     | Ephemeral constant used for dropping any value by name into a mov command
| .code     |     NA      |     NA      | Indicate the beginning of code section of block

All variables that are loaded will be encoded into the asm file with the "INS_LOAD_VAR" (0xB0) precessing it, followed by 4 bytes representing an unsigned 32-bit integer to describe how many bytes represent said constant, followed then by the constant.

After all variables have been loaded, the "INS_LOAD_VAR" instruction will be present to indicate the start of the instruction set.

Variables (Items where Argument 1 has a 'Name' value) can have their address loaded via a **mov** instruction in-place of a numerical literal. This is done by writing the variable name after a '&' symbol. The variable's memory location will be placed into the destination register allowing load instructions to access that particular section of memory.

To obtain the length (in bytes) of a variable **mov** can be given '#' in front of a variable name. This will load the corresponding variable's number of bytes to the destination register.

Constants declared with **.const** can be dropped into either argument of a **mov** command using '*' before the constant name. 
Constants can hold any value, but the **mov** command must still be formed correctly. This means that any constant dropped into
the first argument of a **mov** command must still be a register, and any dropped into the second argument must be formatted to
conform to its corresponding rules (below) 

Arguments where 'value' is given can either be in base 10, or base 16. Formatting for base 16 require the leading '0x' prefixed to the value. 

## Multi Processing

When there are sufficient processors declared using the **.proc** directive the **proc** instruction (below) can be used to to spawn on a different processor executing at a given address. When a new processor is spawned it contains its own set of registers and contains its own stack. Memory accessed by load/store commands however, is shared. Great care should be taken to ensure that memory is being manually segmented between processes. 

If there are multiple processes running, the VM will not shut down until all processes have completed regardless of what process started the program unless a **halt** instruction is discovered. If a **halt** instruction is executed by any of the processes, the VM will shut down.

## Device Directives

Taking place before the **.code** directive, these special directives setup a shared memory block for ***external*** devices. 

| Directive | Argument 1 | Argument 2  | Argument 3 |   Description
|-          |-           |-            |- |-
|  .dev     |   Integer  |    NA       | NA   |   Marks the beginning of a device declaration, and assigns an id
|  .irq     |   Dev ID   | Label Name  | NA |  An interrupt request identifier that calls the label code when active
|  .flg     |   Dev ID   |    Name     | NA |   Flag designator that sets aside a shared byte so the application can signal data-out on device
|  .buf     |   Dev ID   |  Buf Len    | Buf Location | Sets the length and address of the device buffer


It is up to the user to determine how the device will communicate and if these need to be set. The assembler can not anticipate how the device should work as this is an abstraction for some 'potential' device. Device IDs do NOT directly or inherently correlate to a particular implementation of a particular device.

If the irq directive is set, the **dirq** instruction should be set as to ensure other interrupts don't interfere. At the end of
the interrupt section the **eirq** should be set to re-enable interrupts on the system.  

### Working With Devices

Not all directives will be required for devices, but for some it makes sense to use them. It is up to the user to figure out what device they are using and what is required. 

All devices will have the following requirements: 
 - They must all respond in some way if their respective **.flg** memory is set to '1' and once completed must then set that flag back to '0', 

The user will be responsible for enabling or disabling interrupts during an interrupt handle / once the interrupt is handled. If interrupts are not re-enabled interrupts will no longer be triggered. The system will not figure out if interrupts are handled.

---


## Code Instructions

Instructions are variable length instructions with 1 byte indicating which instruction is to be executed, with the remainder of the instruction data specific to the instruction-group. 

Abbreviations : 
| Abbreviation | Meaning                                |
|---           |---                                     |
|      r       | register                               |
|      n       | in-place numerical value               |

**Note**: Memory offsets refer to 'byte' offsets. so $7(sp) is to refer to the 7th byte stored within the stack


---

## Interrupt Instructions

| Instruction  | Description
|-             |- 
|   dirq       | Disable interrupts 
|   eirq       | Enable interrupts


**dirq**

Opcode : 0x70

```
     OPCODE  

```

**eirq**

Opcode : 0x71

```
     OPCODE  

```

## Memory Instructions

|  Instruction     |  Arg1     |  Arg2    |  Description                                 |
|---               |---        |---       |---                                           |
|      ldb         |    r      |   r      |  Load byte from Arg2 (address) into Arg1     |
|      ldq         |    r      |   r      |  Load 2 bytes from Arg2 (address) into Arg1     |
|      ldh         |    r      |   r      |  Load 4 bytes from Arg2 (address) into Arg1     |
|      ldw         |    r      |   r      |  Load word from Arg2 (address) into Arg1     |
|      stb         |    r      |   r      |  Store byte from Arg2 into Arg1     |
|      stq         |    r      |   r      |  Store 2 bytes from Arg2 into Arg1  |
|      sth         |    r      |   r      |  Store 4 bytes from Arg2 into Arg1  |
|      stw         |    r      |   r      |  Store word from Arg2 into Arg1     |
|      load        |    r      |   r      |  Load size Arg1 from Arg2 onto stack from memory core |
|      store       |    r      |   r      |  Store size Arg1 into Arg2 into memory core from stack |


**ldb**

Opcode : 0x10

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**ldq** 

Opcode : 0x11

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**ldh**

Opcode : 0x12

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**ldw**

Opcode : 0x13

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**stb**

Opcode : 0x15

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**stq** 

Opcode : 0x16

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**sth**

Opcode : 0x17

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**stw**

Opcode : 0x18

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**Load**

Opcode : 0x90

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**Store**

Opcode : 0x91

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

## Move Instruction

|  Instruction     |  Arg1     |  Arg2    |  Description                        |
|---               |---        |---       |---                                  |
|      mov         |    r      |    r     |  Move data from Arg2 into Arg1      |
|      mov         |    r      |    n     |  Move data from Arg2 into Arg1      |

**mov**

Opcode : 0xAA

Variant One:
```
     OPCODE  [VARIANT]   [ REGISTER ] [ REGISTER ]
             00000000      00000000  |  00000000
```

Variant Two:
```
     OPCODE  [VARIANT]  [ REGISTER ] [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
              00000001    00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

The ***mov*** instruction can be used to load variable memory addresses in addition to a variable's length (bytes) in memory. 

|  Symbol  |   Description 
|-         |-
|    &     |   When prefixed to a variable name in Argument 2 of **mov** the variable's address will be substituted in place 
|    #     |   When prefixed to a variable name in Argument 2 of **mov** the variable's byte length will be substituted in place
|    *     |   Substitute a **const** into either Argument 1 or Argument 2
|    @     |   Substitute a label's memory address in for Argument to (Note: Only works with labels)

The substitution symbols here will generate a **mov** instruction of variant 'Two' iff the variable was actually declared. 

The use of values in variant two can be in base 10 or base 16. For values in base 16 the value must have '0x' prefixed to it.

(Note: Variant, while only needing a single bit has been made into a byte so the more generalized functions can operate on it. Time V.S Space trade off - Time was given priority)


## Stack Instructions

|  Instruction      |  Arg1     |   Arg2   |  Description                     |
|---                |---        |---       |---                               |
|      pushw        |    r      |   NA     |  Push word from Arg1 onto stack  |
|      popw         |    r      |   NA     |  Pop word from stack onto Arg1   |
|      pushb        |    r      |   NA     |  Push byte from Arg1 onto stack  |
|      popb         |    r      |   NA     |  Pop byte from stack onto Arg1   |
|      pushq        |    r      |   NA     |  Push quarter word from Arg1 onto stack  |
|      popq         |    r      |   NA     |  Pop quarter word from stack onto Arg1   |
|      pushh        |    r      |   NA     |  Push half word from Arg1 onto stack  |
|      poph         |    r      |   NA     |  Pop half word from stack onto Arg1   |
|      size         |    r      |   NA     |  Get size (bytes) of usable stack       |
|      cap          |    r      |   NA     |  Get capacity (bytes) of stack   |
|      ms           |    NA     |   NA     |  Mark the stack at current size  |
|      rs           |    NA     |   NA     |  Reset stack to last mark - If no known last mark, stack is cleared |
|      fp           |    r      |   NA     |  Retrieve the last marked index of the stack  |
|      ufp          |    r      |   NA     |  Update the frame pointer to something specific   |
|      es           |    r      |   NA     |  Extend the usable stack by the amount found in Arg1  |
|      ldsb         |    r      |   r      |  Load byte from Arg2 (address) into Arg1     |
|      ldsq         |    r      |   r      |  Load 2 bytes from Arg2 (address) into Arg1     |
|      ldsh         |    r      |   r      |  Load 4 bytes from Arg2 (address) into Arg1     |
|      ldsw         |    r      |   r      |  Load word from Arg2 (address) into Arg1     |
|      stsb         |    r      |   r      |  Store byte from Arg2 into Arg1     |
|      stsq         |    r      |   r      |  Store 2 bytes from Arg2 into Arg1  |
|      stsh         |    r      |   r      |  Store 4 bytes from Arg2 into Arg1  |
|      stsw         |    r      |   r      |  Store word from Arg2 into Arg1     |

**pushw**

Opcode : 0x20

```
     OPCODE    [ REGISTER ]
             |   00000000  
```

**popw**

Opcode : 0x21

```
     OPCODE    [ REGISTER ]
             |   00000000  
```


**pushb**

Opcode : 0x22

```
     OPCODE    [ REGISTER ]
             |   00000000  
```

**popb**

Opcode : 0x23

```
     OPCODE    [ REGISTER ]
             |   00000000  
```

**pushq**

Opcode : 0x24

```
     OPCODE    [ REGISTER ]
             |   00000000  
```

**popq**

Opcode : 0x25

```
     OPCODE    [ REGISTER ]
             |   00000000  
```

**pushh**

Opcode : 0x26

```
     OPCODE    [ REGISTER ]
             |   00000000  
```

**poph**

Opcode : 0x27

```
     OPCODE    [ REGISTER ]
             |   00000000  
```

**size**

Opcode : 0x28

```
     OPCODE    [ REGISTER ]
             |   00000000  
```

**cap**

Opcode : 0x29

```
     OPCODE    [ REGISTER ]
             |   00000000  
```

**ms**

Opcode : 0xAB

```
     OPCODE  
             
```

**rs**

Opcode : 0xAC

```
     OPCODE  
             
```

**ufp**

Opcode : 0xAD

```
     OPCODE  
             
```

**ldsb**

Opcode : 0x80

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**ldsq** 

Opcode : 0x81

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**ldsh**

Opcode : 0x82

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**ldsw**

Opcode : 0x83

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**stsb**

Opcode : 0x84

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**stsq** 

Opcode : 0x85

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**stsh**

Opcode : 0x86

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**stsw**

Opcode : 0x87

```
     OPCODE    [ REGISTER ]  [ REGISTER ]  
                 00000000  |   00000000
```

**es**

Opcode : 0x88

```
     OPCODE  
             
```

**fp**

Opcode : 0x89

```
     OPCODE  
             
```

## Asserts

|  Instruction  |  Arg1     |  Arg2   |  Description                        |
|---            |---        |---      |---                                  |
|      asne     |    r      |    r    |  Assert Arg1 != Arg2                | 
|      aseq     |    r      |    r    |  Assert Arg1 == Arg2                | 


**asne**

Opcode : 0xFC

```
     OPCODE      [ ARG1 ]      [ ARG2 ] 
             |   00000000  |   00000000 
```

**aseq**

Opcode : 0xFD

```
     OPCODE      [ ARG1 ]      [ ARG2 ] 
             |   00000000  |   00000000 
```


## Bitwise Instructions

|  Instruction  |  Arg1     |  Arg2     |  Arg3     |  Description                        |
|---            |---        |---        |---        |---                                  |
|      lsh      |    r      |    r      |    r      |  Left shift Arg2 by Arg3 into Arg1  | 
|      rsh      |    r      |    r      |    r      |  Right shift Arg2 by Arg4 into Arg1 |
|      and      |    r      |    r      |    r      |  And Arg2 and Arg3 into Arg1        |
|      or       |    r      |    r      |    r      |  Or  Arg2 and Arg3 into Arg1        |
|      xor      |    r      |    r      |    r      |  Xor Arg2 and Arg3 into Arg1        |
|      not      |    r      |    r      |    NA     |  Not Arg2 into Arg1                 |

**lsh**

Opcode : 0x30

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**rsh**

Opcode : 0x31

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**and**

Opcode : 0x32

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**or**

Opcode : 0x33

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**xor**

Opcode : 0x34

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**not**

Opcode : 0x35

```
     OPCODE      [ ARG1 ]      [ ARG2 ]
             |   00000000  |   00000000
```

## Arithmetic Instructions

|  Instruction  |  Arg1     |  Arg2     |  Arg3     |  Description                  |
|---            |---        |---        |---        |---                            |
|      add      |    r      |    r      |    r      |  Add Arg2 and Arg3 into Arg1  | 
|      sub      |    r      |    r      |    r      |  Sub Arg2 and Arg3 into Arg1  | 
|      mul      |    r      |    r      |    r      |  Mul Arg2 and Arg3 into Arg1  | 
|      div      |    r      |    r      |    r      |  Div Arg2 and Arg3 into Arg1  | 
|      pow      |    r      |    r      |    r      |  Raise Arg2 to Arg3 into Arg1  | 
|      mod      |    r      |    r      |    r      |  Modulus Arg2 by Arg3 into Arg1  | 
|      add.f    |    r      |    r      |    r      |  Add Arg2 and Arg3 into Arg1  | 
|      sub.f    |    r      |    r      |    r      |  Sub Arg2 and Arg3 into Arg1  | 
|      mul.f    |    r      |    r      |    r      |  Mul Arg2 and Arg3 into Arg1  | 
|      div.f    |    r      |    r      |    r      |  Div Arg2 and Arg3 into Arg1  | 

**add**

Opcode : 0x40

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**sub**

Opcode : 0x41

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**mul**

Opcode : 0x42

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**div**

Opcode : 0x43

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**pow**

Opcode : 0x44

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**mod**

Opcode : 0x45

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```



**add.f**

Opcode : 0x46

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**sub.f**

Opcode : 0x47

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**mul.f**

Opcode : 0x48

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

**div.f**

Opcode : 0x49

```
     OPCODE      [ ARG1 ]      [ ARG2 ]      [ ARG3 ]  
             |   00000000  |   00000000  |   00000000  
```

## Branch Instructions

|  Instruction  |  Arg1     |  Arg2     |  Arg3        |  Description                                 |
|---            |---        |---        |---           |---                                           |
|      blt      |    r      |    r      |   LABEL      |  Branch to LABEL iff Arg1 less than Arg2     | 
|      bgt      |    r      |    r      |   LABEL      |  Branch to LABEL iff Arg1 greater than Arg2  | 
|      beq      |    r      |    r      |   LABEL      |  Branch to LABEL iff Arg1 equal to Arg2      | 
|      bne      |    r      |    r      |   LABEL      |  Branch to LABEL iff Arg1 not equal to Arg2  | 
|      blt.f    |    r      |    r      |   LABEL      |  Branch to LABEL iff Arg1 less than Arg2     | 
|      bgt.f    |    r      |    r      |   LABEL      |  Branch to LABEL iff Arg1 greater than Arg2  | 
|      beq.f    |    r      |    r      |   LABEL      |  Branch to LABEL iff Arg1 equal to Arg2      | 
|      bne.f    |    r      |    r      |   LABEL      |  Branch to LABEL iff Arg1 not equal to Arg2  | 


**blt**

Opcode : 0x50

```
     OPCODE      [ ARG1 ]      [ ARG2 ]  [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
             |   00000000  |   00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

**bgt**

Opcode : 0x51

```
     OPCODE      [ ARG1 ]      [ ARG2 ]  [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
             |   00000000  |   00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

**beq**

Opcode : 0x52

```
     OPCODE      [ ARG1 ]      [ ARG2 ]  [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
             |   00000000  |   00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

**bne**

Opcode : 0x53

```
     OPCODE      [ ARG1 ]      [ ARG2 ]  [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
             |   00000000  |   00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

**blt.f**

Opcode : 0x54

```
     OPCODE      [ ARG1 ]      [ ARG2 ]  [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
             |   00000000  |   00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

**bgt.f**

Opcode : 0x55

```
     OPCODE      [ ARG1 ]      [ ARG2 ]  [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
             |   00000000  |   00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

**beq.f**

Opcode : 0x56

```
     OPCODE      [ ARG1 ]      [ ARG2 ]  [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
             |   00000000  |   00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

**bne.f**

Opcode : 0x57

```
     OPCODE      [ ARG1 ]      [ ARG2 ]  [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
             |   00000000  |   00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

## Jump/Return Instructions

|  Instruction  |  Arg1         |  Description
|---            |---            |---   
|      jmp      |      r        |    Store current location on jump stack and jump to address in given register
|      jmp      |    LABEL      |    Store current location on jump stack and jump to the given label          
|      goto     |      r        |    Goto address in given register without storing current location on jump stack
|      goto     |    LABEL      |    Goto address in given label without storing current location on jump stack   
|      ret      |    NA         |    Return the address on top of the jump stack

**jmp**

Opcode : 0x60

Variant One:
```
     OPCODE  [VARIANT]   [ REGISTER ] [ REGISTER ]
             00000000      00000000  |  00000000
```

Variant Two:
```
     OPCODE  [VARIANT]  [ REGISTER ] [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
              00000001    00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

**goto**

Opcode : 0x61

Variant One:
```
     OPCODE  [VARIANT]   [ REGISTER ] [ REGISTER ]
             00000000      00000000  |  00000000
```

Variant Two:
```
     OPCODE  [VARIANT]  [ REGISTER ] [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
              00000001    00000000  | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

**ret**

Opcode : 0x62

```
     OPCODE 
            
```

## NOP Instructions

|  Instruction  |  Description
|---            |---   
|      nop      |    No Operation

**nop**

Opcode : 0x00

```
     OPCODE 
            
```

## Exit Instructions

|  Instruction   |  Description
|---             |---   
|      exit      |    Exit

**exit**

Opcode : 0xFF

```
     OPCODE 
            
```

## Exit Instructions

|  Instruction   |  Description
|---             |---   
|      halt      |    Halt all processes and exit

**exit**

Opcode : 0xFE

```
     OPCODE 
            
```

## Proc Instructions

|  Instruction   | Argument |  Description
|---             |---       |---   
|      proc      |  Label   |    Attempt to execute label marked are on a different processor

**proc**

Opcode : 0x02

```
     OPCODE  [ -------------------------------- MEMORY ADDRESS ------------------------------------ ]
             | 00000000 | 00000000 | 00000000 | 00000000 |  00000000 | 00000000 | 00000000 | 00000000 
```

## Wait Instructions

|  Instruction   | Argument |  Description
|---             |---       |---   
|      wait      |     r    |    Issue a wait for 'value' millis in current proc

**wait**

Opcode : 0xFB

```
     OPCODE   REGISTER
              00000000
```

## BMS Instructions

|  Instruction   | Argument |  Description
|---             |---       |---   
|      bms       |     r    |   Retrieve the beginning of memory space (post instruction code)

**bms**

Opcode : 0xFA

```
     OPCODE   REGISTER
              00000000 
```

## Debug-Point Instructions

|  Instruction   |  Description
|---             |---   
|      dbp       |    Emits debug information over a VM channel (iff setup) showing instruction pointer and register information

**dbp**

Opcode : 0x01

```
     OPCODE 
            
```

