# Notes and about

The LVM can support an arbitrary number of threads on the underlying hardware using the *proc* command. The entry address will be started on a processor on the program's main thread, and any *proc* call will be spawned on a new thread. It is important to note however that using LVM devices from these threads must be done with care. The primary thread is responsible for handling interrupts from devices, and the devices themselves execute requests in the main thread. This means that calling the IO device from a thread will block the main thread's execution while the IO device is handling the request, and the caller thread will not be blocked at all meaning a manual wait that keys off of a memory segment update must be written for the caller thread. 

## Main thread and live expectancy

The program will run until the main processor executes an *exit* instruction, or a *halt* instruction is executed by any thread. This means that if the main thread dies before any *proc* calls, the program will come to a natural death.