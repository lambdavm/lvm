#[cfg(not(target_pointer_width = "64"))]
compile_error!("LVM's Assembler crate only supports 64-bit architectures (currently)");

extern crate derive_more;
use derive_more::{Display};

#[macro_use] 
extern crate lazy_static;

use std::{
    fs::File,
    path::Path,
    io::{prelude::*, BufReader},
    collections::{
        HashMap,
        HashSet
    }
};

use regex::Regex;

extern crate bytegen;

macro_rules! ensure_len {
    ($parts:expr, $ins:expr, $line:expr, $len:expr) => {
        if $parts.len() != $len { 
            eprintln!("Error : Malformed '{}' instruction : \"{}\"", $ins, $line); 
            return Err(AssemblerError::MalformedInstruction); 
        }
    };
}

macro_rules! call_and_build {
    ($func:ident, $parts:expr, $result:expr) => {
        match $func($parts[1].as_str(), $parts[2].as_str()) {
            Ok(data) => { $result.byte_code.extend(data); }
            Err(e)   => { return Err(e); }
        }
    };
}

macro_rules! rr {
    ($line:expr, $parts:expr, $result:expr, $ins:expr, $func:ident) => {
        
        ensure_len!($parts, $ins, $line, 3);

        if !is_register($parts[1].as_str()) { 
            eprintln!("Error : Expected register for argument one of '{}' instruction. Got \"{}\" instead", $ins, $parts[1]);
            return Err(AssemblerError::MalformedInstruction); 
        }
        if !is_register($parts[2].as_str()) { 
            eprintln!("Error : Expected register for argument two of '{}' instruction. Got \"{}\" instead", $ins, $parts[2]);
            return Err(AssemblerError::MalformedInstruction); 
        }

        call_and_build!($func, $parts, $result);
    }
}

macro_rules! singr {
    ($line:expr, $parts:expr, $result:expr, $ins:expr, $func:ident) => {

        ensure_len!($parts, $ins, $line, 2);
        
        if !is_register($parts[1].as_str()) { 
            eprintln!("Error : Expected register for argument one of '{}' instruction. Got \"{}\" instead", $ins, $parts[1]);
            return Err(AssemblerError::MalformedInstruction); 
        }
        match $func($parts[1].as_str()) {
            Ok(data) => { $result.byte_code.extend(data); }
            Err(e)   => { return Err(e); }
        }
    }
}

macro_rules! rrr {
    ($line:expr, $parts:expr, $result:expr, $ins:expr, $func:ident) => {

        ensure_len!($parts, $ins, $line, 4);
        
        if !is_register($parts[1].as_str()) { 
            eprintln!("Error : Expected register for argument one of '{}' instruction. Got \"{}\" instead", $ins, $parts[1]);
            return Err(AssemblerError::MalformedInstruction); 
        }

        if !is_register($parts[2].as_str()) { 
            eprintln!("Error : Expected register for argument two of '{}' instruction. Got \"{}\" instead", $ins, $parts[2]);
            return Err(AssemblerError::MalformedInstruction); 
        }

        if !is_register($parts[3].as_str()) { 
            eprintln!("Error : Expected register for argument three of '{}' instruction. Got \"{}\" instead", $ins, $parts[3]);
            return Err(AssemblerError::MalformedInstruction); 
        }

        match $func($parts[1].as_str(),$parts[2].as_str(),$parts[3].as_str()) {
            Ok(data) => { $result.byte_code.extend(data); }
            Err(e)   => { return Err(e); }
        }
    }
}

macro_rules! rrl {
    ($line:expr, $parts:expr, $result:expr, $ins:expr, $program:expr, $func:ident) => {

        ensure_len!($parts, $ins, $line, 4);
        
        if !is_register($parts[1].as_str()) { 
            eprintln!("Error : Expected register for argument one of '{}' instruction. Got \"{}\" instead", $ins, $parts[1]);
            return Err(AssemblerError::MalformedInstruction); 
        }

        if !is_register($parts[2].as_str()) { 
            eprintln!("Error : Expected register for argument two of '{}' instruction. Got \"{}\" instead", $ins, $parts[2]);
            return Err(AssemblerError::MalformedInstruction); 
        }

        if !is_called_label(&String::from($parts[3].as_str())) { 
            eprintln!("Error : Expected label for argument three of '{}' instruction. Got \"{}\" instead", $ins, $parts[3]);
            return Err(AssemblerError::MalformedInstruction); 
        }

        match $func($program, $parts[1].as_str(),$parts[2].as_str(),$parts[3].as_str()) {
            Ok(data) => { $result.byte_code.extend(data); }
            Err(e)   => { return Err(e); }
        }
    }
}

macro_rules! encode_const_numerical_directive {
    ($ins:expr, $program:expr, $var:expr, $parts:expr, $line:expr, $cast_to:tt, $value:expr) => {
        
        ensure_len!($parts, $ins, $line, 3);

        $value= match $parts[2].to_string().parse::<$cast_to>() {
            Ok(value) => value,
            Err(_)    => {

                match $cast_to::from_str_radix($parts[2].trim_start_matches("0x"), 16) {
                    Ok(value) => {
                        value 
                    },
                    Err(_) => {
                        eprintln!("Error: '{}' value failed to parse. Type : {}", $parts[2], $var.vtype);
                        return Err(AssemblerError::MalformedDirective);
                    }
                }
            }
        };

        $var.name = $parts[1].to_string();

        if !is_called_label(&$var.name) {
            eprintln!("Error: Malformed numerical name : {}", $var.name);
            return Err(AssemblerError::MalformedName);
        }

        $var.encoding = $value.to_le_bytes().to_vec();

        if $program.constants.contains_key(&$var.name) {
            eprintln!("Error : Item '{}' given to variable declaration is already declared as a const", $var.name);
            return Err(AssemblerError::MalformedInstruction);
        }

        if $program.known_variables.contains_key(&$var.name) {
            eprintln!("Error : Duplicate variable name : {}", $var.name);
            return Err(AssemblerError::DuplicateVariable);
        }
        $program.known_variables.insert($var.name.clone(), $var);

        //println!("Encoded : {} as type {}", $parts[1].to_string(), $ins);
    };
}

macro_rules! encode_const_flt_numerical_directive {
    ($program:expr, $var:expr, $parts:expr, $line:expr, $value:expr) => {
        
        ensure_len!($parts, "flt", $line, 3);

        $value= match $parts[2].to_string().parse::<f64>() {
            Ok(value) => value,
            Err(_)    => {
                eprintln!("Error: '{}' value failed to parse. Type : {}", $parts[2], $var.vtype);
                return Err(AssemblerError::MalformedDirective);
            }
        };

        $var.name = $parts[1].to_string();
        $var.encoding = $value.to_le_bytes().to_vec();

        if !is_called_label(&$var.name) {
            eprintln!("Error: Malformed numerical float name : {}", $var.name);
            return Err(AssemblerError::MalformedName);
        }

        if $program.constants.contains_key(&$var.name) {
            eprintln!("Error : Item '{}' given to variable declaration is already declared as a const", $var.name);
            return Err(AssemblerError::MalformedInstruction);
        }

        if $program.known_variables.contains_key(&$var.name) {
            eprintln!("Error : Duplicate variable name : {}", $var.name);
            return Err(AssemblerError::DuplicateVariable);
        }
        $program.known_variables.insert($var.name.clone(), $var);

        //println!("Encoded : {} as type {}", $parts[1].to_string(), $ins);
    };
}

macro_rules! encode_const_numerical_reserved {
    ($ins:expr, $program:expr, $var:expr, $parts:expr, $line:expr, $cast_to:tt, $value:expr) => {
        
        ensure_len!($parts, $ins, $line, 3);

        $value= match $parts[2].to_string().parse::<$cast_to>() {
            Ok(value) => value,
            Err(_)    => {

                match $cast_to::from_str_radix($parts[2].trim_start_matches("0x"), 16) {
                    Ok(value) => {
                        value 
                    },
                    Err(_) => {
                        eprintln!("Error: '{}' value failed to parse. Type : {}", $parts[2], $var.vtype);
                        return Err(AssemblerError::MalformedDirective);
                    }
                }
            }
        };

        $var.name = $parts[1].to_string();

        if !is_called_label(&$var.name) {
            eprintln!("Error: Malformed numerical name : {}", $var.name);
            return Err(AssemblerError::MalformedName);
        }

        $var.encoding = vec![0; $value as usize];

        if $program.constants.contains_key(&$var.name) {
            eprintln!("Error : Item '{}' given to variable declaration is already declared as a const", $var.name);
            return Err(AssemblerError::MalformedInstruction);
        }

        if $program.known_variables.contains_key(&$var.name) {
            eprintln!("Error : Duplicate variable name : {}", $var.name);
            return Err(AssemblerError::DuplicateVariable);
        }
        $program.known_variables.insert($var.name.clone(), $var);

        //println!("Encoded : {} as type {}", $parts[1].to_string(), $ins);
    };
}

/// Take in a label name and attempt to get the location in memory. If the label does not yet exist, add it to the
/// referenced labels so they can be resolved once the assembler has completed parsing the file
macro_rules! get_label_or_add_reference {
    ($program:expr, $label:expr, $label_loc:expr, $offset:expr) => {
        if $program.known_labels.contains_key($label) {
            $label_loc = $program.known_labels.get($label).unwrap().clone();
        } else {
            $program.referenced_labels.push((String::from($label), 
                $program.byte_code.len()+$offset));
            $label_loc = 0;
        }
    };
}

macro_rules! jmp_or_goto {
    ($program:expr, $part:expr, $ins:expr, $variant_one_func:expr, $variant_two_func:expr) => {
        if is_register($part.as_str()) {
            match $variant_one_func($part.as_str()) {
                Ok(bytes) => { $program.byte_code.extend(bytes); }
                Err(e)    => { return Err(e); }
            }
        } else if is_called_label(&$part) {
            let label_loc : u64;
            get_label_or_add_reference!($program, &$part, label_loc, 2);
            match $variant_two_func(label_loc) {
                Ok(bytes) => { $program.byte_code.extend(bytes); }
                Err(e)    => { return Err(e); }
            }
        } else {
            eprintln!("Error : Expected label or register for '{}' instruction, got '{}'", $ins, $part);
            return Err(AssemblerError::MalformedInstruction);
        }   
    };
}

#[derive(Display)]
pub enum AssemblerError {
    UnableToOpenFile,
    MalformedDirective,
    MissingRequiredDirectives,
    UndeclaredLabelInUse,
    UnknownInstruction,
    MalformedInstruction,
    ParseError,
    UndefinedLabelReference,
    MalformedName,
    DuplicateVariable,
    DuplicateLabel,
    DuplicateDeviceId,
    DuplicateConstant,
    UnknownVariable,
    UnknownConstant,
    InsufficientMemory,
    BadIncludePath,
    InvalidPathName
}

#[derive(Display, Clone)]
enum VarType {
    Signed8,
    Signed16,
    Signed32,
    Signed64,
    Flt,
    Str,
    Reserved
}

#[derive(Clone)]
struct Var {
    name: String,
    vtype: VarType,
    encoding: Vec<u8>,
    memory_location: u64
}

#[derive(Clone)]
struct DeviceSetup {
    id:                u32,
    interrupt_addr:    u64,
    flag:              u8,

    irq_label:         String,
    flag_label:        String,

    irq_loc:            usize,
    flag_loc:           usize,
    buffer_len:         u32,
    buffer_loc:         u64
}

impl DeviceSetup {
    fn new(id: u32) -> Self {
        Self {
            id:                 id,
            interrupt_addr:     0xFFFF_FFFF_FFFF_FFFF,
            flag:               0xFF,
            irq_label:          String::new(),
            flag_label:         String::new(),
            irq_loc:            0,
            flag_loc:           0,
            buffer_len:         0xFFFF_FFFF,
            buffer_loc:         0xFFFF_FFFF_FFFF_FFFF
        }
    }
}

struct ProgramInfo {
    lines: Vec<String>,
    init:  String,
    known_labels: HashMap<String, u64>,           // Labels and the index they jump to
    referenced_labels: Vec<(String, usize)>,    // Labels used before declaration, and where in the resulting byte vector an update must occur
    byte_code: Vec<u8>,
    known_variables: HashMap<String, Var>,
    devices: HashMap<String, DeviceSetup>,
    proc_count: u8,
    constants: HashMap<String, String>,
    imports: HashSet<String>
}

fn read_file(filename: String) -> Result<Vec<String>, AssemblerError> {
    let file = match File::open(filename) {
        Ok(c) => c,
        Err(_) => return Err(AssemblerError::UnableToOpenFile)
    };
    let buf = BufReader::new(file);
    Ok(buf.lines()
        .map(|l| l.expect("Failed to parse line"))
        .collect())
}

pub fn assemble(file: String, includes: Vec<&str>) -> Result<Vec<u8>, AssemblerError> {

    let contents = match read_file(file) {
        Ok(c) => c,
        Err(_) => return Err(AssemblerError::UnableToOpenFile)
    };

    let mut program_info = ProgramInfo{
        lines: Vec::new(),
        init:  String::new(),
        known_labels: HashMap::new(),
        referenced_labels: Vec::new(),
        byte_code: Vec::new(),
        known_variables: HashMap::new(),
        devices: HashMap::new(),
        proc_count: 1,
        constants: HashMap::new(),
        imports: HashSet::new()
    };

    if let Err(e) = pre_scan(&mut program_info, &contents, &includes) {
        return Err(e)
    };

    program_info.byte_code.extend((u64::MAX as u64).to_le_bytes().to_vec());    // Ins start (update later)
    program_info.byte_code.extend((u64::MAX as u64).to_le_bytes().to_vec());    // Entry Point (update later)
    program_info.byte_code.extend(program_info.proc_count.to_le_bytes().to_vec());

    pack_variables(&mut program_info);

    if let Err(e) = pack_devices(&mut program_info) {
        return Err(e);
    }

    // Update word first appearing in pack_variables with where start_code_instruction will be
    let target : u64 = program_info.byte_code.len() as u64;
    let mut shift : u64 = 0;
    for location in 0 .. 8 {
        program_info.byte_code[location as usize] = (target >> shift) as u8;
        if shift < 56 { 
            shift = shift + 8;
        }
    }

    // Now that all of the variables and meta information is encoded
    // we need to mark the beginning of the code section
    program_info.byte_code.push(bytegen::start_code_instruction());

    return assemble_program(&mut program_info);
}

fn pack_variables(program: &mut ProgramInfo) {

    for variable in program.known_variables.iter_mut() {

        // Indicate that a var needs to be loaded
        program.byte_code.push(bytegen::load_var_instruction());

        // Insert how many bytes the constant is represented by
        program.byte_code.extend(
            (variable.1.encoding.len() as u32).to_le_bytes().to_vec()
        );

        // Mark the variable's location in memory so we can sub it in
        // if the user uses a '&' in their code
        variable.1.memory_location = program.byte_code.len() as u64;

        // Insert the constant
        program.byte_code.extend(variable.1.encoding.clone());
    }
}

fn pack_devices(program: &mut ProgramInfo) -> Result<(), AssemblerError> {

    for device in program.devices.iter_mut() {
        program.byte_code.extend(
            bytegen::load_device_id(device.1.id)
        );
        if device.1.irq_label != String::new() {
            // Irq is a label, not a variable so we add it to referenced labels
            // The address that it corresponds to will be updated at the end of the assembly
            program.referenced_labels.push((device.1.irq_label.clone(), program.byte_code.len()));
        }
        program.byte_code.extend(
            bytegen::load_irq_addr(device.1.interrupt_addr)
        );
        if device.1.flag_label != String::new() {
            device.1.flag_loc = program.byte_code.len();
        }
        program.byte_code.extend(
            bytegen::load_dev_flag(device.1.flag)
        );
        program.byte_code.extend(
            bytegen::load_dev_buff(device.1.buffer_len, device.1.buffer_loc)
        );
    }
    Ok(())
}

#[inline(always)]
fn is_label(item: &String) -> bool {
    lazy_static! {
        static ref RE_IS_LABEL: Regex = Regex::new(r"^[a-zA-Z0-9_]+:$").unwrap();
    }
    RE_IS_LABEL.is_match(item)
}

#[inline(always)]
fn is_called_label(item: &String) -> bool {
    lazy_static! {
        static ref RE_IS_LABEL: Regex = Regex::new(r"^[a-zA-Z0-9_]+$").unwrap();
    }
    RE_IS_LABEL.is_match(item)
}

#[inline(always)]
fn is_register(item: &str) -> bool {
    lazy_static! {
        static ref RE_IS_REGISTER: Regex = Regex::new(r"^r{1}([0-9]|1[0-5])$").unwrap();
    }
    RE_IS_REGISTER.is_match(item)
}

#[inline(always)]
fn is_numerical_constant(item: &str) -> bool {
    lazy_static! {
        static ref RE_IS_NUM_CONST: Regex = Regex::new(r"(^\$[0-9]+$)|(^\$\-[0-9]+$)").unwrap();
    }
    RE_IS_NUM_CONST.is_match(item)
}

#[inline(always)]
fn is_numerical_hex_constant(item: &str) -> bool {
    lazy_static! {
        static ref RE_IS_HEX_CONST: Regex = Regex::new(r"(^\$0[x][0-9a-fA-F]+$)|(^\$\-[0-9]+$)").unwrap();
    }
    RE_IS_HEX_CONST.is_match(item)
}

#[inline(always)]
fn is_variable_constant(item: &str) -> bool {
    lazy_static! {
        static ref RE_IS_VAR: Regex = Regex::new(r"^\&[a-zA-Z0-9_]+$").unwrap();
    }
    RE_IS_VAR.is_match(item)
}

#[inline(always)]
fn is_constant(item: &str) -> bool {
    lazy_static! {
        static ref RE_IS_VAR: Regex = Regex::new(r"^\*[a-zA-Z0-9_]+$").unwrap();
    }
    RE_IS_VAR.is_match(item)
}

#[inline(always)]
fn is_var_len_instruction(item: &str) -> bool {
    lazy_static! {
        static ref RE_VAR_LEN: Regex = Regex::new(r"^\#[a-zA-Z0-9_]+$").unwrap();
    }
    RE_VAR_LEN.is_match(item)
}

#[inline(always)]
fn is_label_addr_instruction(item: &str) -> bool {
    lazy_static! {
        static ref RE_VAR_LEN: Regex = Regex::new(r"^@[a-zA-Z0-9_]+$").unwrap();
    }
    RE_VAR_LEN.is_match(item)
}

/// Take a line from an assemble file and break it into its parts, preserving string 
/// definitions and removing any commented out information (';')
#[inline(always)]
fn chunk_line(item: &mut String) -> Result<Vec<String>, AssemblerError> {

    if item.len() > 0 {
        let last = item.chars().last().unwrap();

        if last == '\r' {
            item.pop();
        }
    }

    if item.chars().next().unwrap() == ';' {
        return Ok(Vec::new());
    }

    if item.len() == 0 {
        return Err(AssemblerError::MalformedDirective);
    }
    let mut token = String::new();
    let mut result : Vec<String> = Vec::new();
    let mut in_str = false;
    let mut last : char = ' ';
    for c in item.chars() {
        if !in_str && c == ';' {
            break;
        }
        if c == '"' && last != '\\' {
            in_str = if in_str { false } else { true };
        }
        if (c == ' ' || c == '\t' || c == '\n') && !in_str {
            if token.len() > 0 {
                result.push(token.clone());
                token.clear();
            }
        } else {
            token.push(c);
        }
        last = c;
    }

    if token.len() > 0 {
        result.push(token);
    }
    return Ok(result);
}

fn pre_scan(pi: &mut ProgramInfo, contents: & Vec<String>, includes: &Vec<&str>) -> Result<(), AssemblerError> {

    //println!("File : {:?}", contents);

    let mut in_code = false;
    let mut entry_found = false;
    let mut in_lib = false;

    for line in contents.iter() {
        let trimmed = line.trim().to_string();
        if trimmed.len() > 0 {

            let parts : Vec<String> = match chunk_line(&mut line.clone()) {
                Ok(r) => { r },
                Err(e) => return Err(e)
            };

            if parts.len() == 0 {
                continue;
            }

            if in_code {
                pi.lines.push(trimmed.clone());

                if !in_lib && is_label(&trimmed) {

                    let chopped = &trimmed[0..trimmed.len()-1];

                    if chopped == pi.init {
                        entry_found = true;
                    }
                }
            }

            if parts.len() > 0 && !in_code {

                match parts[0].as_str() {

                    ".init" => {
                        if parts.len() == 2 {
                            pi.init = parts[1].to_string();
                        } else {
                            eprintln!("Error : '.init' directive missing label name");
                            return Err(AssemblerError::MalformedDirective);
                        }

                        if in_lib {
                            eprintln!("Can not have an 'init' in a file declared with 'lib' directive file");
                            return Err(AssemblerError::MalformedDirective);
                        }
                    }

                    ".lib" => {
                        in_lib = true;
                    }

                    ".extern" => {
                        ensure_len!(parts, ".extern", trimmed, 2);
                        if !pi.imports.contains(&parts[1]) {
                            'search: for item in includes.iter() {
                                let file_name = match Path::new(&item).file_name() {
                                    Some(path) => {
                                        match path.to_str() {
                                            Some(path_string) => { path_string},
                                            None              => { return Err(AssemblerError::InvalidPathName); }
                                        }
                                    },
                                    None => {
                                        return Err(AssemblerError::BadIncludePath);
                                    }
                                };

                                if file_name == parts[1] {

                                    pi.imports.insert(parts[1].clone());

                                    match read_file(item.to_string()) {
                                        Ok(lines) => {
                                            if let Err(e) = pre_scan(pi, &lines, includes) {
                                                return Err(e);
                                            }
                                        },
                                        Err(e) => {
                                            return Err(e);
                                        }
                                    }
                                    break 'search;
                                }

                            }

                        }
                    }

                    ".proc" => {
                        ensure_len!(parts, ".proc", trimmed, 2);
                        let value= match parts[1].to_string().parse::<u8>() {
                            Ok(value) => value,
                            Err(_)    => {
                                match u8::from_str_radix(parts[1].trim_start_matches("0x"), 16) {
                                    Ok(value) => {
                                        value 
                                    },
                                    Err(_) => {
                                        eprintln!("Error: '{}' value failed to parse for '.proc' directive.", parts[1]);
                                        return Err(AssemblerError::MalformedDirective);
                                    }
                                }
                            }
                        };
                        pi.proc_count = value;
                    }

                    ".i8" => {
                        let value : i8;
                        let mut v : Var = Var{ 
                            name: String::new(),
                            vtype: VarType::Signed8,
                            encoding: Vec::new(),
                            memory_location: 0
                        };

                        encode_const_numerical_directive!("i8", pi, v, parts, trimmed, i8, value);
                    }

                    ".i16" => {
                        let value : i16;
                        let mut v : Var = Var{ 
                            name: String::new(),
                            vtype: VarType::Signed16,
                            encoding: Vec::new(),
                            memory_location: 0
                        };

                        encode_const_numerical_directive!("i16", pi, v, parts, trimmed, i16, value);
                    }

                    ".i32" => {
                        let value : i32;
                        let mut v : Var = Var{ 
                            name: String::new(),
                            vtype: VarType::Signed32,
                            encoding: Vec::new(),
                            memory_location: 0
                        };

                        encode_const_numerical_directive!("i32", pi, v, parts, trimmed, i32, value);
                    }

                    ".i64" => {
                        let value : i64;
                        let mut v : Var = Var{ 
                            name: String::new(),
                            vtype: VarType::Signed64,
                            encoding: Vec::new(),
                            memory_location: 0
                        };

                        encode_const_numerical_directive!("i64", pi, v, parts, trimmed, i64, value);
                    }

                    ".res" => {
                        let value : i64;
                        let mut v : Var = Var{ 
                            name: String::new(),
                            vtype: VarType::Reserved,
                            encoding: Vec::new(),
                            memory_location: 0
                        };

                        encode_const_numerical_reserved!("i64", pi, v, parts, trimmed, i64, value);
                    }

                    ".flt" => {
                        let value : f64;
                        let mut v : Var = Var{ 
                            name: String::new(),
                            vtype: VarType::Flt,
                            encoding: Vec::new(),
                            memory_location: 0
                        };

                        encode_const_flt_numerical_directive!(pi, v, parts, trimmed, value);
                    }

                    ".str" => {
                        ensure_len!(parts, "str", trimmed, 3);

                        let mut v : Var = Var{ 
                            name: String::new(),
                            vtype: VarType::Str,
                            encoding: Vec::new(),
                            memory_location: 0
                        };
                        v.name = parts[1].to_string();
                        v.encoding = String::from(parts[2].trim_matches('"')).as_bytes().to_vec();

                        if pi.constants.contains_key(&v.name) {
                            eprintln!("Error : Item '{}' given to variable declaration is already declared as a const", v.name);
                            return Err(AssemblerError::MalformedInstruction);
                        }
                        if pi.known_variables.contains_key(&v.name) {
                            eprintln!("Error : Duplicate variable name : {}", v.name);
                            return Err(AssemblerError::DuplicateVariable);
                        }
                        pi.known_variables.insert(v.name.clone(), v);
                    }

                    ".const" => {
                        ensure_len!(parts, "const", trimmed, 3);

                        if !is_called_label(&parts[1]) {
                            eprintln!("Error: Malformed constant name : {}", parts[1]);
                            return Err(AssemblerError::MalformedName);
                        }
                        if pi.known_variables.contains_key(&parts[1]) {
                            eprintln!("Error : Item '{}' given to const declaration is already declared as a variable", parts[1]);
                            return Err(AssemblerError::MalformedInstruction);
                        }
                        if pi.constants.contains_key(&parts[1]) {
                            eprintln!("Error : Duplicate constant name : {}", parts[1]);
                            return Err(AssemblerError::DuplicateConstant);
                        }

                        pi.constants.insert(parts[1].clone(), parts[2].clone());
                    }

                    ".dev" => {
                        ensure_len!(parts, "dev", trimmed, 2);

                        let value = match parts[1].to_string().parse::<u32>() {
                            Ok(value) => value,
                            Err(_)    => {
                                eprintln!("Error: '{}' value failed to parse. Type : u32", parts[1]);
                                return Err(AssemblerError::MalformedDirective);
                            }
                        };

                        if pi.devices.contains_key(&(value).to_string()) {
                            eprintln!("Error: Duplicate device id '{}'", value);
                            return Err(AssemblerError::DuplicateDeviceId);
                        }

                        pi.devices.insert(parts[1].clone(), DeviceSetup::new(value));
                    }

                    ".irq" => {
                        ensure_len!(parts, "irq", trimmed, 3);

                        let id = match parts[1].to_string().parse::<u32>() {
                            Ok(value) => value,
                            Err(_)    => {
                                eprintln!("Error: '{}' value failed to parse. Type : u32", parts[1]);
                                return Err(AssemblerError::MalformedDirective);
                            }
                        };

                        if !pi.devices.contains_key(&(id).to_string()) {
                            eprintln!("Error: Unknown device id '{}'", id);
                            return Err(AssemblerError::DuplicateDeviceId);
                        }

                        if !is_called_label(&parts[2]) {
                            eprintln!("Error: Expected label for device {}'s '.irq' got : '{}'", id, parts[3]);
                            return Err(AssemblerError::MalformedDirective);
                        }

                        let mut device = pi.devices.get_mut(&(id).to_string()) .unwrap();

                        device.interrupt_addr = 0;
                        device.irq_label      = parts[2].clone();
                    }

                    ".flg" => {
                        ensure_len!(parts, "flg", trimmed, 3);

                        let id = match parts[1].to_string().parse::<u32>() {
                            Ok(value) => value,
                            Err(_)    => {
                                eprintln!("Error: '{}' value failed to parse. Type : u32", parts[1]);
                                return Err(AssemblerError::MalformedDirective);
                            }
                        };

                        if !pi.devices.contains_key(&(id).to_string())  {
                            eprintln!("Error: Unknown device id '{}'", id);
                            return Err(AssemblerError::DuplicateDeviceId);
                        }

                        if !is_called_label(&parts[2]) {
                            eprintln!("Error: Expected label for device {}'s '.flg' got : '{}'", id, parts[3]);
                            return Err(AssemblerError::MalformedDirective);
                        }

                        let mut device = pi.devices.get_mut(&(id).to_string()) .unwrap();

                        device.flag = 0;
                        device.flag_label = parts[2].clone();
                    }
                    
                    ".buf" => {
                        ensure_len!(parts, "buf", trimmed, 4);

                        let id = match parts[1].to_string().parse::<u32>() {
                            Ok(value) => value,
                            Err(_)    => {
                                eprintln!("Error: '{}' value failed to parse. Type : u32", parts[1]);
                                return Err(AssemblerError::MalformedDirective);
                            }
                        };

                        if !pi.devices.contains_key(&(id).to_string()) {
                            eprintln!("Error: Unknown device id '{}'", id);
                            return Err(AssemblerError::DuplicateDeviceId);
                        }

                        let buff_len = match parts[2].to_string().parse::<u32>() {
                            Ok(value) => value,
                            Err(_)    => {
                                eprintln!("Error: '{}' value failed to parse. Type : u32", parts[2]);
                                return Err(AssemblerError::MalformedDirective);
                            }
                        };

                        let buff_loc = match parts[3].to_string().parse::<u64>() {
                            Ok(value) => value,
                            Err(_)    => {
                                eprintln!("Error: '{}' value failed to parse. Type : u64", parts[3]);
                                return Err(AssemblerError::MalformedDirective);
                            }
                        };

                        let mut device = pi.devices.get_mut(&(id).to_string()).unwrap();

                        device.buffer_len = buff_len;
                        device.buffer_loc = buff_loc;
                    }

                    ".code" => {
                        in_code = true;
                    }

                    item => {
                        eprintln!("Unknown item while scanning directives '{}'", item);
                        return Err(AssemblerError::MalformedDirective);
                    }
                }
            }
        }
    }

    if !in_code {
        eprintln!("Error : Failed to find '.code' directive");
        return Err(AssemblerError::MissingRequiredDirectives);
    }
    if !in_lib && !entry_found {
        eprintln!("Error : Failed to find label marked as entry point : {}", pi.init);
        return Err(AssemblerError::UndeclaredLabelInUse);
    }
    return Ok(());
}

fn assemble_program(program: &mut ProgramInfo) -> Result<Vec<u8>, AssemblerError> {

    //println!("Assemble Program");

    for line in program.lines.clone().iter() {

        let mut parts : Vec<String> = match chunk_line(&mut line.clone()) {
            Ok(r) => { r },
            Err(e) => return Err(e)
        };

        //println!("Parts: {:?}", parts);

        match parts[0].as_str() {

            //  This MOV instruction is a grös. I was going for mvp. Once its all set and the device
            //  stuff is ironed out AND tested this should be revisited to make it... not this. Then as long
            //  as tests are in place we can be confident that the more 'elegant' solution later is working
            "mov" => {
                ensure_len!(parts, "mov", line, 3);

                if is_constant(&parts[1]) {

                    let var_name = String::from(&parts[1][1..parts[1].len()]);

                    if program.constants.contains_key(&var_name) {

                        parts[1] = program.constants.get(&var_name).unwrap().clone();

                    } else {
                        eprintln!("Error : Unknown constant '{}'", var_name);
                        return Err(AssemblerError::UnknownConstant);
                    }
                }

                if !is_register(parts[1].as_str()) { 
                    eprintln!("Error : Expected register for argument one of 'mov' instruction. Got \"{}\" instead", parts[1]);
                    return Err(AssemblerError::MalformedInstruction); 
                }

                if is_constant(&parts[2]) {

                    let var_name = String::from(&parts[2][1..parts[2].len()]);

                    if program.constants.contains_key(&var_name) {

                        parts[2] = program.constants.get(&var_name).unwrap().clone();

                    } else {
                        eprintln!("Error : Unknown constant '{}'", var_name);
                        return Err(AssemblerError::UnknownConstant);
                    }
                }


                if is_register(parts[2].as_str())                    { call_and_build!(generate_mov_variant_one_instruction, parts, program); }
                else if is_numerical_constant(parts[2].as_str())     { call_and_build!(generate_mov_variant_two_instruction, parts, program); }
                else if is_numerical_hex_constant(parts[2].as_str()) { 
                    parts[2] = match i64::from_str_radix(parts[2].trim_start_matches("$0x"), 16) {
                        Ok(value) => {
                            ("$".to_string() + &value.to_string()).to_string()
                        },
                        Err(_) => {
                            return Err(AssemblerError::MalformedDirective);
                        }
                    };
                    call_and_build!(generate_mov_variant_two_instruction, parts, program); 
                }
                else if is_variable_constant( parts[2].as_str()) { 

                    let var_name = String::from(&parts[2][1..parts[2].len()]);
                    
                    let mut located  = false;
                    // Check if its a variable, and get the length if it is
                    if program.known_variables.contains_key(&var_name) {
                        let location = program.known_variables.get(&var_name).unwrap().memory_location;
                        let placement = format!("${}", location.to_string());
                        parts[2] = placement;
                        call_and_build!(generate_mov_variant_two_instruction, parts, program);
                        located = true;
                    } else {
                        for device in program.devices.iter() {
                            if device.1.flag_label == var_name {
                                let location = device.1.flag_loc;
                                let placement = format!("${}", location.to_string());
                                parts[2] = placement;
                                call_and_build!(generate_mov_variant_two_instruction, parts, program);
                                located = true;
                            }
                        }
                    }
                    if!located {
                        eprintln!("Error : Unknown item reference in mov instruction '{}' ", var_name);
                        return Err(AssemblerError::UnknownVariable);
                    }
                }
                else if is_label_addr_instruction(parts[2].as_str()) {

                    let label = String::from(&parts[2][1..parts[2].len()]);

                    let label_loc : u64;
                    if program.known_labels.contains_key(&label) {
                        label_loc = program.known_labels.get(&label).unwrap().clone();
                    } else {
                        // Store the location where the updated label value will be inserted at the end of this process so
                        // we can update it later
                        program.referenced_labels.push((String::from(label), 
                            program.byte_code.len()+3));
                        label_loc = 0;
                    }
                    let placement = format!("${}", label_loc.to_string());
                    parts[2] = placement;
                    call_and_build!(generate_mov_variant_two_instruction, parts, program);

                }
                else if is_var_len_instruction(parts[2].as_str()) {

                    let var_name = String::from(&parts[2][1..parts[2].len()]);

                    let mut located  = false;
                    // Check if its a variable, and get the length if it is
                    if program.known_variables.contains_key(&var_name) {

                        let var_size = program.known_variables.get(&var_name).unwrap().encoding.len() as u64;
                        let placement = format!("${}", var_size.to_string());
                        parts[2] = placement;
                        call_and_build!(generate_mov_variant_two_instruction, parts, program);
                        located = true;
                    } else {
                        for device in program.devices.iter() {
                            if device.1.flag_label == var_name {
                                let location = 1;
                                let placement = format!("${}", location.to_string());
                                parts[2] = placement;
                                call_and_build!(generate_mov_variant_two_instruction, parts, program);
                                located = true;
                            }
                        }
                    }

                    if!located{
                        eprintln!("Error : Unknown item references in mov instruction '{}' ", var_name);
                        return Err(AssemblerError::UnknownVariable);
                    }

                }
                else {  eprintln!("Error : Expected register OR numerical value in argument two of 'mov' instruction. Got \"{}\" instead", parts[2]); }
            }
            "ldb"  => { rr!(line, parts, program, "ldb" , generate_ldb_instruction); }
            "ldq"  => { rr!(line, parts, program, "ldq" , generate_ldq_instruction); }
            "ldh"  => { rr!(line, parts, program, "ldh" , generate_ldh_instruction); }
            "ldw"  => { rr!(line, parts, program, "ldw" , generate_ldw_instruction); }
            "stb"  => { rr!(line, parts, program, "stb" , generate_stb_instruction); }
            "stq"  => { rr!(line, parts, program, "stq" , generate_stq_instruction); }
            "sth"  => { rr!(line, parts, program, "sth" , generate_sth_instruction); }
            "stw"  => { rr!(line, parts, program, "stw" , generate_stw_instruction); }
            "ldsb" => { rr!(line, parts, program, "ldsb", generate_ldsb_instruction); }
            "ldsq" => { rr!(line, parts, program, "ldsq", generate_ldsq_instruction); }
            "ldsh" => { rr!(line, parts, program, "ldsh", generate_ldsh_instruction); }
            "ldsw" => { rr!(line, parts, program, "ldsw", generate_ldsw_instruction); }
            "stsb" => { rr!(line, parts, program, "stsb", generate_stsb_instruction); }
            "stsq" => { rr!(line, parts, program, "stsq", generate_stsq_instruction); }
            "stsh" => { rr!(line, parts, program, "stsh", generate_stsh_instruction); }
            "stsw" => { rr!(line, parts, program, "stsw", generate_stsw_instruction); }
            "load"  => { rr!(line, parts, program, "load" , generate_load_instruction); }
            "store" => { rr!(line, parts, program, "store", generate_store_instruction); }
            "pushw"  => { singr!(line, parts, program, "pushw",  generate_pushw_instruction); }
            "popw"   => { singr!(line, parts, program, "popw",   generate_popw_instruction);  }
            "pushb"  => { singr!(line, parts, program, "pushb",  generate_pushb_instruction); }
            "popb"   => { singr!(line, parts, program, "popb",   generate_popb_instruction);  }
            "pushq"  => { singr!(line, parts, program, "pushq",  generate_pushq_instruction); }
            "popq"   => { singr!(line, parts, program, "popq",   generate_popq_instruction);  }
            "pushh"  => { singr!(line, parts, program, "pushh",  generate_pushh_instruction); }
            "poph"   => { singr!(line, parts, program, "poph",   generate_poph_instruction);  }
            "size"   => { singr!(line, parts, program, "size",   generate_size_instruction);  }
            "es"     => { singr!(line, parts, program, "ef",     generate_es_instruction);    }
            "fp"     => { singr!(line, parts, program, "fp",     generate_fp_instruction);    }
            "ufp"    => { singr!(line, parts, program, "ufp",    generate_ufp_instruction);   }
            "cap"    => { singr!(line, parts, program, "cap",    generate_cap_instruction);   }
            "bms"    => { singr!(line, parts, program, "bms",    generate_bms_instruction);   }
            "lsh"   => { rrr!(line, parts, program, "lsh",   generate_lsh_instruction); }
            "rsh"   => { rrr!(line, parts, program, "lsh",   generate_rsh_instruction); }
            "and"   => { rrr!(line, parts, program, "and",   generate_and_instruction); }
            "or"    => { rrr!(line, parts, program, "or",    generate_or_instruction);  }
            "xor"   => { rrr!(line, parts, program, "xor",   generate_xor_instruction); }
            "not"   => {  rr!(line, parts, program, "not",   generate_not_instruction); }
            "add"   => { rrr!(line, parts, program, "add",   generate_add_instruction); }
            "sub"   => { rrr!(line, parts, program, "sub",   generate_sub_instruction); }
            "mul"   => { rrr!(line, parts, program, "mul",   generate_mul_instruction); }
            "div"   => { rrr!(line, parts, program, "div",   generate_div_instruction); }
            "pow"   => { rrr!(line, parts, program, "pow",   generate_pow_instruction); }
            "mod"   => { rrr!(line, parts, program, "mod",   generate_mod_instruction); }
            "add.f" => { rrr!(line, parts, program, "add.f", generate_addf_instruction); }
            "sub.f" => { rrr!(line, parts, program, "sub.f", generate_subf_instruction); }
            "mul.f" => { rrr!(line, parts, program, "mul.f", generate_mulf_instruction); }
            "div.f" => { rrr!(line, parts, program, "div.f", generate_divf_instruction); }
            "blt"   => { rrl!(line, parts, program, "blt",   program, generate_blt_instruction); }
            "bgt"   => { rrl!(line, parts, program, "bgt",   program, generate_bgt_instruction); }
            "beq"   => { rrl!(line, parts, program, "beq",   program, generate_beq_instruction); }
            "bne"   => { rrl!(line, parts, program, "bne",   program, generate_bne_instruction); }
            "blt.f" => { rrl!(line, parts, program, "blt.f", program, generate_bltf_instruction); }
            "bgt.f" => { rrl!(line, parts, program, "bgt.f", program, generate_bgtf_instruction); }
            "beq.f" => { rrl!(line, parts, program, "beq.f", program, generate_beqf_instruction); }
            "bne.f" => { rrl!(line, parts, program, "bne.f", program, generate_bnef_instruction); }
            "asne"  => { rr!(line, parts, program, "asne",   generate_asne_instruction); }
            "aseq"  => { rr!(line, parts, program, "aseq",   generate_aseq_instruction); }
            "jmp"   => { jmp_or_goto!(program, parts[1], "jmp",  generate_jmp_variant_one_instruction, generate_jmp_variant_two_instruction); }
            "goto" =>  { jmp_or_goto!(program, parts[1], "goto", generate_goto_variant_one_instruction, generate_goto_variant_two_instruction); }
            "ret"  =>  {
                ensure_len!(parts, "ret", line, 1);
                match generate_ret_instruction() {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }
            "nop" => {
                ensure_len!(parts, "nop", line, 1);
                match generate_nop_instruction() {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }
            "dbp" => {
                ensure_len!(parts, "dbp", line, 1);
                match generate_dbp_instruction() {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }
            "exit" => {
                ensure_len!(parts, "exit", line, 1);
                match generate_exit_instruction() {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }
            "halt" => {
                ensure_len!(parts, "halt", line, 1);
                match generate_halt_instruction() {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }
            "proc" => {
                ensure_len!(parts, "proc", line, 2);
                if ! is_called_label(&String::from(parts[1].as_str())) {
                    eprintln!("Error : Expected a label in argument one of 'proc' instruction. Got '{}' instead", parts[1]);
                    return Err(AssemblerError::MalformedInstruction);
                }
                match generate_proc_instruction(program, parts[1].as_str()) {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }
            "dirq" => {
                ensure_len!(parts, "dirq", line, 1);
                match generate_dirq_instruction() {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }
            "eirq" => {
                ensure_len!(parts, "eirq", line, 1);
                match generate_eirq_instruction() {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }

            "wait" => {
                ensure_len!(parts, "wait", line, 2);

                if !is_register(parts[1].as_str()) {
                    eprintln!("Error : Expected a register in argument 1 of 'wait' instruction. Got '{}' instead", parts[1]);
                    return Err(AssemblerError::MalformedInstruction);
                }

                match generate_wait_instruction(parts[1].as_str()) {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }

            "ms" => {
                ensure_len!(parts, "ms", line, 1);
                match generate_ms_instruction() {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }

            "rs" => {
                ensure_len!(parts, "rs", line, 1);
                match generate_rs_instruction() {
                    Ok(data) => { program.byte_code.extend(data); }
                    Err(e)   => { return Err(e); }
                }
            }
            
            item => {
                let p = parts[0].to_string();

                if is_label(&p) {
                    let label_name = &p[0..p.len()-1];

                    if program.known_variables.contains_key(&String::from(label_name)) {
                        eprintln!("Error : Label '{}' is already declared as a variable", label_name);
                        return Err(AssemblerError::MalformedInstruction);
                    }

                    if program.constants.contains_key(&String::from(label_name)) {
                        eprintln!("Error : Label '{}' is already declared as a constant", label_name);
                        return Err(AssemblerError::MalformedInstruction);
                    }

                    if program.known_labels.contains_key(&String::from(label_name)) {
                        eprintln!("Duplicate label name : {}", label_name);
                        return Err(AssemblerError::DuplicateLabel);
                    }
                    program.known_labels.insert(String::from(label_name), program.byte_code.len() as u64);

                    // Update the second word of the program to insert the address of the entry point
                    if program.init == label_name {
                        let mut i = 8;
                        let b = (program.byte_code.len() as u64).to_le_bytes().to_vec();
                        for x in b.iter() {
                            program.byte_code[i] = x.clone();
                            i += 1;
                        }

                        #[cfg(debug)]
                        println!("Init '{}' is at position '{}", p, program.byte_code.len());
                    }
                    continue;
                }

                eprintln!("Error: Unknown Instruction : {}", item);
                return Err(AssemblerError::UnknownInstruction);
            }
        }
    }

    /*
        Here we go though any potential unresolved labels (jmps or branches that occur before label definition)
        We attempt to find the location that the item was inserted in within the program bytecode ((item.1.clone() as u64) .. ((item.1+8) as u64))
        And we copy in the actual value of the label. 
    */
    if program.referenced_labels.len() > 0 {

        #[cfg(debug)]
        println!("Resolving {} labels", program.referenced_labels.len());

        for item in program.referenced_labels.iter() {

            if !program.known_labels.contains_key(&item.0) {

                eprintln!("Error : Unresolved label '{}'", item.0);
                return Err(AssemblerError::UndefinedLabelReference);
            }

            let target : u64 = program.known_labels.get(&item.0).unwrap().clone();
            
            let mut shift : u64 = 0;
            for location in (item.1.clone() as u64) .. ((item.1+8) as u64) {

                program.byte_code[location as usize] = (target >> shift) as u8;
                if shift < 56 { 
                    shift = shift + 8;
                }
            }
        }
    }

    Ok(program.byte_code.clone())
}

#[inline(always)]
fn extract_reg_number(reg: &str) -> Option<u8> {

    let item = &reg[1..reg.len()];
    match item.to_string().parse::<u8>() {
        Ok(value) => return Some(value),
        Err(_)    => return None
    }
}

#[inline(always)]
fn extract_const_signed_number(reg: &str) -> Option<i64> {

    let item = &reg[1..reg.len()];
    match item.to_string().parse::<i64>() {
        Ok(value) => return Some(value),
        Err(_)    => return None
    }
}

#[inline(always)]
fn generate_mov_variant_one_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {

    let r1 = if let Some(r) = extract_reg_number(arg1) { r } else { 
        eprintln!("Argument one for 'mov' instruction failed to get register number into type 'u8' from register entry '{}'", arg1 );
        return Err(AssemblerError::ParseError); 
    }; 
    let r2 = if let Some(r) = extract_reg_number(arg2) { r } else { 
        eprintln!("Argument two for 'mov' instruction failed to get register number into type 'u8' from register entry '{}'", arg2 );
        return Err(AssemblerError::ParseError); 
    }; 

    return Ok(bytegen::MoveInstructions::MovVariantOne{ dest_reg: r1, source_reg: r2 }.encode());
}

#[inline(always)]
fn generate_mov_variant_two_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {

    let r1 = if let Some(r) = extract_reg_number(arg1) { r } else { 
        eprintln!("Argument one for 'mov' instruction failed to get register number into type 'u8' from register entry '{}'", arg1 );
        return Err(AssemblerError::ParseError); 
    }; 
    let r2 = if let Some(r) = extract_const_signed_number(arg2) { r } else { 
        eprintln!("Argument two for 'mov' instruction failed to get const number from entry '{}'", arg2 );
        return Err(AssemblerError::ParseError); 
    }; 

    return Ok(bytegen::MoveInstructions::MovVariantTwo{ dest_reg: r1, numerical: r2 }.encode());
}

macro_rules! extract_two_registers {
    ($ins:expr, $arg1:expr, $arg2:expr, $r1:expr, $r2:expr) => {
        $r1 = if let Some(r) = extract_reg_number($arg1) { r } else { 
            eprintln!("Argument one for '{}' instruction failed to get register number into type 'u8' from register entry '{}'", $ins, $arg1 );
            return Err(AssemblerError::ParseError); 
        }; 
    
        $r2 = if let Some(r) = extract_reg_number($arg2) { r } else { 
            eprintln!("Argument two for '{}' instruction failed to get register number into type 'u8' from register entry '{}'", $ins, $arg2 );
            return Err(AssemblerError::ParseError); 
        }; 
    
    };
}

#[inline(always)]
fn generate_ldb_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("ldb", arg1, arg2, r1, r2);
    return Ok(bytegen::MemoryInstructions::Ldb{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_ldq_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("ldq", arg1, arg2, r1, r2);
    return Ok(bytegen::MemoryInstructions::Ldq{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_ldh_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("ldh", arg1, arg2, r1, r2);
    return Ok(bytegen::MemoryInstructions::Ldh{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_ldw_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("ldw", arg1, arg2, r1, r2);
    return Ok(bytegen::MemoryInstructions::Ldw{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_stb_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("stb", arg1, arg2, r1, r2);
    return Ok(bytegen::MemoryInstructions::Stb{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_stq_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("stq", arg1, arg2, r1, r2);
    return Ok(bytegen::MemoryInstructions::Stq{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_sth_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("sth", arg1, arg2, r1, r2);
    return Ok(bytegen::MemoryInstructions::Sth{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_stw_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("stw", arg1, arg2, r1, r2);
    return Ok(bytegen::MemoryInstructions::Stw{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_ldsb_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("ldsb", arg1, arg2, r1, r2);
    return Ok(bytegen::StackInstructions::Ldsb{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_ldsq_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("ldsq", arg1, arg2, r1, r2);
    return Ok(bytegen::StackInstructions::Ldsq{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_ldsh_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("ldsh", arg1, arg2, r1, r2);
    return Ok(bytegen::StackInstructions::Ldsh{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_ldsw_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("ldsw", arg1, arg2, r1, r2);
    return Ok(bytegen::StackInstructions::Ldsw{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_stsb_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("stsb", arg1, arg2, r1, r2);
    return Ok(bytegen::StackInstructions::Stsb{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_stsq_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("stsq", arg1, arg2, r1, r2);
    return Ok(bytegen::StackInstructions::Stsq{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_stsh_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("stsh", arg1, arg2, r1, r2);
    return Ok(bytegen::StackInstructions::Stsh{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_stsw_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("stsw", arg1, arg2, r1, r2);
    return Ok(bytegen::StackInstructions::Stsw{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_store_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("store", arg1, arg2, r1, r2);
    return Ok(bytegen::MemoryInstructions::Store{ size: r1, pos: r2 }.encode());
}

#[inline(always)]
fn generate_load_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    extract_two_registers!("load", arg1, arg2, r1, r2);
    return Ok(bytegen::MemoryInstructions::Load{ size: r1, pos: r2 }.encode());
}

macro_rules! extract_one_register {
    ($ins:expr, $arg1:expr, $r1:expr) => {
        $r1 = if let Some(r) = extract_reg_number($arg1) { r } else { 
            eprintln!("Argument for '{}' instruction failed to get register number into type 'u8' from register entry '{}'", $ins, $arg1 );
            return Err(AssemblerError::ParseError); 
        }; 
    };
}

#[inline(always)]
fn generate_pushw_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("pushw", arg1, r1);
    return Ok(bytegen::StackInstructions::Pushw{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_popw_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("popw", arg1, r1);
    return Ok(bytegen::StackInstructions::Popw{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_pushb_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("pushb", arg1, r1);
    return Ok(bytegen::StackInstructions::Pushb{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_popb_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("popb", arg1, r1);
    return Ok(bytegen::StackInstructions::Popb{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_pushq_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("pushq", arg1, r1);
    return Ok(bytegen::StackInstructions::Pushq{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_popq_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("popq", arg1, r1);
    return Ok(bytegen::StackInstructions::Popq{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_pushh_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("pushh", arg1, r1);
    return Ok(bytegen::StackInstructions::Pushh{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_poph_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("poph", arg1, r1);
    return Ok(bytegen::StackInstructions::Poph{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_size_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("size", arg1, r1);
    return Ok(bytegen::StackInstructions::Size{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_es_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("es", arg1, r1);
    return Ok(bytegen::StackInstructions::Es{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_fp_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("fp", arg1, r1);
    return Ok(bytegen::StackInstructions::Fp{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_ufp_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("ufp", arg1, r1);
    return Ok(bytegen::StackInstructions::Ufp{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_cap_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("cap", arg1, r1);
    return Ok(bytegen::StackInstructions::Cap{ reg: r1 }.encode());
}

#[inline(always)]
fn generate_bms_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    extract_one_register!("bms", arg1, r1);
    return Ok(bytegen::MiscInstructions::Bms{ dest: r1 }.encode());
}

macro_rules! extract_three_registers {
    ($ins:expr, $arg1:expr, $arg2:expr, $arg3:expr, $r1:expr, $r2:expr, $r3:expr) => {
        $r1 = if let Some(r) = extract_reg_number($arg1) { r } else { 
            eprintln!("Argument one for '{}' instruction failed to get register number into type 'u8' from register entry '{}'", $ins, $arg1 );
            return Err(AssemblerError::ParseError); 
        }; 
        $r2 = if let Some(r) = extract_reg_number($arg2) { r } else { 
            eprintln!("Argument two for '{}' instruction failed to get register number into type 'u8' from register entry '{}'", $ins, $arg2);
            return Err(AssemblerError::ParseError); 
        }; 
        $r3 = if let Some(r) = extract_reg_number($arg3) { r } else { 
            eprintln!("Argument three for '{}' instruction failed to get register number into type 'u8' from register entry '{}'", $ins, $arg3 );
            return Err(AssemblerError::ParseError); 
        }; 
    };
}

#[inline(always)]
fn generate_lsh_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("lsh", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::BitwiseInstructions::Lsh{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_rsh_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("rsh", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::BitwiseInstructions::Rsh{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_and_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("and", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::BitwiseInstructions::And{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_or_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("or", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::BitwiseInstructions::Or{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_xor_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("xor", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::BitwiseInstructions::Xor{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_not_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let _r3: u8;
    extract_three_registers!("not", arg1, arg2, arg2, r1, r2, _r3);
    return Ok(bytegen::BitwiseInstructions::Not{ dest: r1, source: r2 }.encode());
}

#[inline(always)]
fn generate_add_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("add", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::ArithmeticInstructions::Add{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_sub_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("sub", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::ArithmeticInstructions::Sub{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_div_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("div", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::ArithmeticInstructions::Div{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_mul_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("mul", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::ArithmeticInstructions::Mul{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_pow_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("pow", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::ArithmeticInstructions::Pow{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_mod_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("mod", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::ArithmeticInstructions::Mod{ dest: r1, lhs: r2, rhs: r3 }.encode());
}
#[inline(always)]
fn generate_addf_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("addf", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::ArithmeticInstructions::Addf{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_subf_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("subf", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::ArithmeticInstructions::Subf{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_divf_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("divf", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::ArithmeticInstructions::Divf{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_mulf_instruction(arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let r3: u8;
    extract_three_registers!("mulf", arg1, arg2, arg3, r1, r2, r3);
    return Ok(bytegen::ArithmeticInstructions::Mulf{ dest: r1, lhs: r2, rhs: r3 }.encode());
}

#[inline(always)]
fn generate_aseq_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let _r3: u8;
    extract_three_registers!("aseq", arg1, arg2, arg2, r1, r2, _r3);
    return Ok(bytegen::AssertInstructions::Aseq{ lhs: r1, rhs: r2 }.encode());
}

#[inline(always)]
fn generate_asne_instruction(arg1: &str, arg2: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let _r3: u8;
    extract_three_registers!("asne", arg1, arg2, arg2, r1, r2, _r3);
    return Ok(bytegen::AssertInstructions::Asne{ lhs: r1, rhs: r2 }.encode());
}

macro_rules! prep_branch_ins {
    ($program:expr, $ins:expr, $arg1:expr, $arg2:expr, $label:expr, $r1:expr, $r2:expr, $label_loc:expr) => {
        $r1 = if let Some(r) = extract_reg_number($arg1) { r } else { 
            eprintln!("Argument one for '{}' instruction failed to get register number into type 'u8' from register entry '{}'", $ins, $arg1 );
            return Err(AssemblerError::ParseError); 
        }; 
        $r2 = if let Some(r) = extract_reg_number($arg2) { r } else { 
            eprintln!("Argument two for '{}' instruction failed to get register number into type 'u8' from register entry '{}'", $ins, $arg2);
            return Err(AssemblerError::ParseError); 
        }; 

        if $program.known_labels.contains_key($label) {
            $label_loc = $program.known_labels.get($label).unwrap().clone();
        } else {
            // Store the location where the updated label value will be inserted at the end of this process so
            // we can update it later
            $program.referenced_labels.push((String::from($label), 
                $program.byte_code.len() + 3));
            $label_loc = 0;
        }
    };
}

#[inline(always)]
fn generate_blt_instruction(program: &mut ProgramInfo, arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let label_loc : u64;
    prep_branch_ins!(program, "blt", arg1, arg2, arg3, r1, r2, label_loc);
    return Ok(bytegen::BranchInstructions::Blt{ lhs: r1, rhs: r2, dest: label_loc}.encode());
}

#[inline(always)]
fn generate_bgt_instruction(program: &mut ProgramInfo, arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let label_loc : u64;
    prep_branch_ins!(program, "bgt", arg1, arg2, arg3, r1, r2, label_loc);
    return Ok(bytegen::BranchInstructions::Bgt{ lhs: r1, rhs: r2, dest: label_loc}.encode());
}

#[inline(always)]
fn generate_beq_instruction(program: &mut ProgramInfo, arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let label_loc : u64;
    prep_branch_ins!(program, "beq", arg1, arg2, arg3, r1, r2, label_loc);
    return Ok(bytegen::BranchInstructions::Beq{ lhs: r1, rhs: r2, dest: label_loc}.encode());
}

#[inline(always)]
fn generate_bne_instruction(program: &mut ProgramInfo, arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let label_loc : u64;
    prep_branch_ins!(program, "bne", arg1, arg2, arg3, r1, r2, label_loc);
    return Ok(bytegen::BranchInstructions::Bne{ lhs: r1, rhs: r2, dest: label_loc}.encode());
}

#[inline(always)]
fn generate_bltf_instruction(program: &mut ProgramInfo, arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let label_loc : u64;
    prep_branch_ins!(program, "bltf", arg1, arg2, arg3, r1, r2, label_loc);
    return Ok(bytegen::BranchInstructions::Bltf{ lhs: r1, rhs: r2, dest: label_loc}.encode());
}

#[inline(always)]
fn generate_bgtf_instruction(program: &mut ProgramInfo, arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let label_loc : u64;
    prep_branch_ins!(program, "bgtf", arg1, arg2, arg3, r1, r2, label_loc);
    return Ok(bytegen::BranchInstructions::Bgtf{ lhs: r1, rhs: r2, dest: label_loc}.encode());
}

#[inline(always)]
fn generate_beqf_instruction(program: &mut ProgramInfo, arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let label_loc : u64;
    prep_branch_ins!(program, "beqf", arg1, arg2, arg3, r1, r2, label_loc);
    return Ok(bytegen::BranchInstructions::Beqf{ lhs: r1, rhs: r2, dest: label_loc}.encode());
}

#[inline(always)]
fn generate_bnef_instruction(program: &mut ProgramInfo, arg1: &str, arg2: &str, arg3: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1: u8;
    let r2: u8;
    let label_loc : u64;
    prep_branch_ins!(program, "bnef", arg1, arg2, arg3, r1, r2, label_loc);
    return Ok(bytegen::BranchInstructions::Bnef{ lhs: r1, rhs: r2, dest: label_loc}.encode());
}

#[inline(always)]
fn generate_jmp_variant_one_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {

    let r1 = if let Some(r) = extract_reg_number(arg1) { r } else { 
        eprintln!("Argument one for 'jmp' instruction failed to get register number into type 'u8' from register entry '{}'", arg1 );
        return Err(AssemblerError::ParseError); 
    };
    return Ok(bytegen::JumpReturnInstructions::JmpVariantOne{ source_reg: r1 }.encode());
}

#[inline(always)]
fn generate_jmp_variant_two_instruction(dest: u64) -> Result<Vec<u8>, AssemblerError> {

    return Ok(bytegen::JumpReturnInstructions::JmpVariantTwo{ numerical: dest }.encode());
}

#[inline(always)]
fn generate_goto_variant_one_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {

    let r1 = if let Some(r) = extract_reg_number(arg1) { r } else { 
        eprintln!("Argument one for 'goto' instruction failed to get register number into type 'u8' from register entry '{}'", arg1 );
        return Err(AssemblerError::ParseError); 
    };
    return Ok(bytegen::JumpReturnInstructions::GotoVariantOne{ source_reg: r1 }.encode());
}

#[inline(always)]
fn generate_goto_variant_two_instruction(dest: u64) -> Result<Vec<u8>, AssemblerError> {

    return Ok(bytegen::JumpReturnInstructions::GotoVariantTwo{ numerical: dest }.encode());
}

#[inline(always)]
fn generate_proc_instruction(program: &mut ProgramInfo, arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let label_loc: u64;
    get_label_or_add_reference!(program, arg1, label_loc, 1);
    return Ok(bytegen::MiscInstructions::Proc{ dest: label_loc }.encode());
}

#[inline(always)]
fn generate_wait_instruction(arg1: &str) -> Result<Vec<u8>, AssemblerError> {
    let r1 = if let Some(r) = extract_reg_number(arg1) { r } else { 
        eprintln!("Argument one for 'wait' instruction failed to get register number into type 'u8' from register entry '{}'", arg1 );
        return Err(AssemblerError::ParseError); 
    }; 

    return Ok(bytegen::MiscInstructions::Wait{ time: r1 }.encode());
}

#[inline(always)]
fn generate_ret_instruction() -> Result<Vec<u8>, AssemblerError> {
    return Ok(bytegen::JumpReturnInstructions::Ret.encode());
}

#[inline(always)]
fn generate_nop_instruction() -> Result<Vec<u8>, AssemblerError> {
    return Ok(bytegen::MiscInstructions::Nop.encode());
}

#[inline(always)]
fn generate_exit_instruction() -> Result<Vec<u8>, AssemblerError> {
    return Ok(bytegen::MiscInstructions::Exit.encode());
}

#[inline(always)]
fn generate_halt_instruction() -> Result<Vec<u8>, AssemblerError> {
    return Ok(bytegen::MiscInstructions::Halt.encode());
}

#[inline(always)]
fn generate_dbp_instruction() -> Result<Vec<u8>, AssemblerError> {
    return Ok(bytegen::MiscInstructions::Dbp.encode());
}

#[inline(always)]
fn generate_dirq_instruction() -> Result<Vec<u8>, AssemblerError> {
    return Ok(bytegen::InterruptInstructions::Dirq.encode());
}

#[inline(always)]
fn generate_eirq_instruction() -> Result<Vec<u8>, AssemblerError> {
    return Ok(bytegen::InterruptInstructions::Eirq.encode());
}

#[inline(always)]
fn generate_ms_instruction() -> Result<Vec<u8>, AssemblerError> {
    return Ok(bytegen::StackInstructions::Ms.encode());
}

#[inline(always)]
fn generate_rs_instruction() -> Result<Vec<u8>, AssemblerError> {
    return Ok(bytegen::StackInstructions::Rs.encode());
}
