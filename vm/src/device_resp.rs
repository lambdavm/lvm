/*
    The RESP device is primarily used to ensure the VM device mechanisms are working
    correctly in automated tests, but it could be useful in real life maybe. 

    If the device is triggered with an irq defined it will callback on that memory location
    If the device is given a buffer it will overwrite the buffer with 0xFF for the entire
    length of the buffer
*/

use crate::device::DeviceWindow;
use crate::Memory;

use std::sync::Arc;
use std::sync::Mutex;

/// The Responder Device
#[allow(dead_code)]
pub(crate) struct RESPDevice {

    id: u32,
    device_window: DeviceWindow,
    memory:        Arc<Mutex<Memory>>
}

impl RESPDevice {

    /// Create a new device with the id memory and window information
    pub fn new(id: u32, dw: DeviceWindow, memory: Arc<Mutex<Memory>>) -> Self {

        Self {
            id: id,
            device_window: dw,
            memory: memory
        }
    }

    /// Tick the device to check if it needs to act
    #[inline(always)]
    pub fn tick(&mut self) -> (bool, usize) {

        // If the flag isn't declared then we can't be triggered
        let flag = match self.device_window.flag {
            Some(value) => { value },
            None        => { return (false, 0); }
        };

        {
            // Get a memory guard
            let mut memory_guard = match self.memory.lock() {
                Ok(guard) => guard,
                Err(_)    => { return (false, 0); }
            };

            // Get the flag value, if not set we return
            let _ = match memory_guard.get_byte(flag) {
                Ok(value) => {
                    if value == 1 { true } else { return (false, 0); }
                },
                Err(_) => {
                    return (false, 0);
                }
            };

            #[cfg(debug)]
            println!("RESP Device has had its flag set");

            // Reset flag
            let _ = memory_guard.store_byte(flag, 0x00);
        }

        // If an input buffer was assigned, write it over with all FF
        match self.device_window.buffer {
            Some((len, start)) => {

                // Get a memory guard
                let mut memory_guard = match self.memory.lock() {
                    Ok(guard) => guard,
                    Err(_)    => { return (false, 0); }
                };

                for x in start .. start+len as usize {
                    let _ = memory_guard.store_byte(x, 0xFF);
                }
             },
            None        => {}
        };

        // Respond by triggering interrupt
        return match self.device_window.interrupt_addr {
            Some(addr)  => { (true, addr) }
            None        => { (false,   0) }
        };
    }
}