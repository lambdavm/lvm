use crate::memory::{ Memory, MemoryError };

/// A basic stack that works on 64-bit 'word' values
#[derive(Debug, Clone)]
pub (crate) struct Stack {

    data : Memory,
    idx  : usize,
    capacity : usize,
    marks: Vec<usize>
}

impl Stack {

    /// Create a new stack with a given capacity (in words)
    pub (crate) fn new(capacity: usize) -> Self {
        Self {
            data: Memory::new(capacity),
            idx:  0,
            capacity: capacity,
            marks: Vec::new()
        }
    }

    /// Set a mark to the current stack length
    pub (crate) fn mark(&mut self) {
        self.marks.push(self.idx);
    }

    /// Reset stack to last known mark. If no marks present, the stack will be cleared
    pub (crate) fn reset(&mut self) {
        match self.marks.pop() {
            Some(value) => {
                self.idx = value;
            },
            None => {
                self.idx = 0;
            }
        }
    }

    /// Get the current frame pointer
    pub (crate) fn get_fp(&self) -> usize {
        if self.marks.is_empty() {
            return 0;
        } else {
            return self.marks[self.marks.len()-1].clone();
        }
    }

    /// Update the frame pointer - This will lose the current value
    /// If there is no 'current' frame pointer, a new one will be created
    pub (crate) fn update_fp(&mut self, new_value: usize) {

        if self.marks.is_empty() {
            self.marks.push(new_value);
            return;
        }

        let index = self.marks.len()-1;
        self.marks[index] = new_value;
    }

    /// Extend the stack by a given amount
    pub (crate) fn extend_frame(&mut self, amount: u64) -> Option<MemoryError> {

        if (self.capacity as u64) < self.idx as u64 + amount {
            return Some(MemoryError::InsufficientMemory);
        }

        self.idx = self.idx + amount as usize;
        None
    }

    /// Push a single byte
    pub (crate) fn push_byte(&mut self, data: u8) -> Result<(), MemoryError> {
        match self.data.store_byte(self.idx, data) {
            Ok(value) => {
                self.idx += 1;
                return Ok(value);
            }
            Err(e) => {
                return Err(e);
            }
        }
    }

    /// Pop a byte
    pub (crate) fn pop_byte(&mut self) -> Result<u8, MemoryError> {
        if self.idx < 1 {
            return Err(MemoryError::OutOfBounds);
        }
        match self.data.get_byte(self.idx-1) {
            Ok(value) => {
                if self.idx > 0 {
                    self.idx -= 1;
                }
                return Ok(value);
            },
            Err(e) => {
                return Err(e);
            }
        }
    }

    /// Push 2 bytes
    pub (crate) fn push_quarter(&mut self, data: u16) -> Result<(), MemoryError> {
        match self.data.store_quarter(self.idx, data) {
            Ok(value) => {
                self.idx += 2;
                return Ok(value);
            }
            Err(e) => {
                return Err(e);
            }
        }
    }

    /// Pop 2 bytes
    pub (crate) fn pop_quarter(&mut self) -> Result<u16, MemoryError> {
        if self.idx < 2 {
            return Err(MemoryError::OutOfBounds);
        }
        match self.data.get_quarter(self.idx-2) {
            Ok(value) => {
                if self.idx >= 2 {
                    self.idx -= 2;
                }
                return Ok(value);
            },
            Err(e) => {
                return Err(e);
            }
        }
    }

    /// Push 4 bytes
    pub (crate) fn push_half(&mut self, data: u32) -> Result<(), MemoryError> {
        match self.data.store_half(self.idx, data) {
            Ok(value) => {
                self.idx += 4;
                return Ok(value);
            }
            Err(e) => {
                return Err(e);
            }
        }
    }

    /// Pop 4 bytes
    pub (crate) fn pop_half(&mut self)  -> Result<u32, MemoryError> {
        if self.idx < 4 {
            return Err(MemoryError::OutOfBounds);
        }
        match self.data.get_half(self.idx-4) {
            Ok(value) => {
                if self.idx >= 4 {
                    self.idx -= 4;
                }
                return Ok(value);
            },
            Err(e) => {
                return Err(e);
            }
        }
    }

    /// Push word (8 bytes)
    pub (crate) fn push_word(&mut self, data: u64) -> Result<(), MemoryError> {
        match self.data.store_word(self.idx, data) {
            Ok(value) => {
                self.idx += 8;
                return Ok(value);
            }
            Err(e) => {
                return Err(e);
            }
        }
    }

    /// Pop word (8 bytes)
    pub (crate) fn pop_word(&mut self) -> Result<u64, MemoryError> {
        if self.idx < 8 {
            return Err(MemoryError::OutOfBounds);
        }
        match self.data.get_word(self.idx-8) {
            Ok(value) => {
                if self.idx >= 8 {
                    self.idx -= 8;
                }
                return Ok(value);
            },
            Err(e) => {
                return Err(e);
            }
        }
    }

    /// Get the current size of the stack
    pub (crate) fn size(&self) -> usize {
        
        return self.idx;
    }

    /// Get the capacity of the stack
    pub (crate) fn cap(&self) -> usize {
        return self.capacity;
    }

    /// Store a byte in memory
    pub(crate) fn store_byte(&mut self, addr: usize, data: u8) -> Result<(), MemoryError> {

        self.data.store_byte(addr, data)
    }

    /// Get a byte from memory
    pub(crate) fn get_byte(&self, addr: usize) -> Result<u8, MemoryError> {

        self.data.get_byte(addr)
    }

    /// Store two bytes in memory
    pub(crate) fn store_quarter(&mut self, addr: usize, data: u16) -> Result<(), MemoryError> {

        self.data.store_quarter(addr, data)
    }

    /// Get two bytes from memory
    pub(crate) fn get_quarter(&self, addr: usize) -> Result<u16, MemoryError> {

        self.data.get_quarter(addr)
    }

    /// Store four bytes in memory
    pub(crate) fn store_half(&mut self, addr: usize, data: u32) -> Result<(), MemoryError> {

        self.data.store_half(addr, data)
    }

    /// Get four bytes from memory
    pub(crate) fn get_half(&self, addr: usize) -> Result<u32, MemoryError> {

        self.data.get_half(addr)
    }

    /// Store eight bytes (a word) in memory
    pub(crate) fn store_word(&mut self, addr: usize, data: u64) -> Result<(), MemoryError> {

        self.data.store_word(addr, data)
    }

    /// Get eight bytes (a word) from memory
    pub(crate) fn get_word(&self, addr: usize) -> Result<u64, MemoryError> {

        self.data.get_word(addr)
    }
}

#[cfg(test)]
mod tests {

    use super::*;


    #[test]
    fn stack_test_byte() {
        let mut s  = Stack::new(10);

        assert_eq!(10, s.cap());
        assert_eq!(0,  s.size());

        for x in 0..10 {

            if let Err(_) = s.push_byte(x as u8) {
                panic!("Failed to add item to stack");
            }
        }
        assert_eq!(10, s.size());

        if let Ok(_) = s.push_byte(44) {
            panic!("Added item to full stack!");
        }

        let mut expected : u8 = 9;
        for i in 0..10 {
            if let Ok(item) = s.pop_byte() {
                assert_eq!(expected, item);
                if expected > 0 {
                    expected = expected -1;
                }
            } else {
                panic!("Unable to pop item from stack >> Iter : {}", i);
            }
        }

        assert_eq!(10, s.cap());
        assert_eq!(0,  s.size());

        if let Ok(_) = s.pop_byte() {
            panic!("Able to pop item from empty stack");
        }
    }

    #[test]
    fn stack_test_quarter() {
        let mut s  = Stack::new(10 * 2);

        assert_eq!(10 * 2, s.cap());
        assert_eq!(0,  s.size());

        for x in 0..10 {

            if let Err(_) = s.push_quarter(x as u16) {
                panic!("Failed to add item to stack");
            }
        }
        assert_eq!(10*2, s.size());

        if let Ok(_) = s.push_quarter(44) {
            panic!("Added item to full stack!");
        }

        let mut expected : u16 = 9;
        for _ in 0..10 {
            if let Ok(item) = s.pop_quarter() {
                assert_eq!(expected, item);
                if expected > 0 {
                    expected = expected -1;
                }
            } else {
                panic!("Unable to pop item from stack");
            }
        }

        assert_eq!(10*2, s.cap());
        assert_eq!(0,  s.size());

        if let Ok(_) = s.pop_quarter() {
            panic!("Able to pop item from empty stack");
        }
    }

    #[test]
    fn stack_test_half() {
        let mut s  = Stack::new(10 * 4);

        assert_eq!(10 * 4, s.cap());
        assert_eq!(0,  s.size());

        for x in 0..10 {

            if let Err(_) = s.push_half(x as u32) {
                panic!("Failed to add item to stack");
            }
        }
        assert_eq!(10*4, s.size());

        if let Ok(_) = s.push_half(44) {
            panic!("Added item to full stack!");
        }

        let mut expected : u32 = 9;
        for _ in 0..10 {
            if let Ok(item) = s.pop_half() {
                assert_eq!(expected, item);
                if expected > 0 {
                    expected = expected -1;
                }
            } else {
                panic!("Unable to pop item from stack");
            }
        }

        assert_eq!(10*4, s.cap());
        assert_eq!(0,  s.size());

        if let Ok(_) = s.pop_half() {
            panic!("Able to pop item from empty stack");
        }
    }

    #[test]
    fn stack_test_word() {
        let mut s  = Stack::new(10 * 8);

        assert_eq!(10 * 8, s.cap());
        assert_eq!(0,  s.size());

        for x in 0..10 {

            if let Err(_) = s.push_word(x as u64) {
                panic!("Failed to add item to stack");
            }
        }
        assert_eq!(10*8, s.size());

        if let Ok(_) = s.push_word(44) {
            panic!("Added item to full stack!");
        }

        let mut expected : u64 = 9;
        for _ in 0..10 {
            if let Ok(item) = s.pop_word() {
                assert_eq!(expected, item);
                if expected > 0 {
                    expected = expected -1;
                }
            } else {
                panic!("Unable to pop item from stack");
            }
        }

        assert_eq!(10*8, s.cap());
        assert_eq!(0,  s.size());

        if let Ok(_) = s.pop_word() {
            panic!("Able to pop item from empty stack");
        }
    }


    #[test]
    fn stack_test_marks_resets() {

        let mut s  = Stack::new(50);

        for x in 33..58 {
            if let Err(_) = s.push_byte(x as u8) {
                panic!("Failed to add item to stack");
            }
        }

        s.mark();

        for x in 0..25 {
            if let Err(_) = s.push_byte(x as u8) {
                panic!("Failed to add item to stack");
            }
        }

        assert_eq!(50, s.size());

        s.reset();

        assert_eq!(25, s.size());

        let mut expected : u8 = 57;
        for _ in 33..58 {
            if let Ok(item) = s.pop_byte() {
                assert_eq!(expected, item);
                if expected > 0 {
                    expected = expected -1;
                }
            } else {
                panic!("Unable to pop item from stack");
            }
        }

        // With no more marks, this should clear the stack
        s.reset();

        assert_eq!(0,  s.size());
    }
    
    #[test]
    fn stack_extend_and_frame() {

        let mut s  = Stack::new(1024);

        s.mark();
        
        if let Some(_) = s.extend_frame(40) {
            panic!("Unable to extend stack");
        }

        s.mark();
        assert_eq!(40,  s.size());
        assert_eq!(40,   s.get_fp());
        
        if let Some(_) = s.extend_frame(100) {
            panic!("Unable to extend stack");
        }

        assert_eq!(140,   s.size());
        assert_eq!(40,    s.get_fp());        // FP Should be at index prior to mark at size 140 

        s.mark();
        assert_eq!(140,    s.get_fp());

        s.reset();

        s.reset();

        assert_eq!(40,   s.size());
        assert_eq!(0,    s.get_fp());        // FP Should be at index prior to mark at size 40 

        s.reset();

        assert_eq!(0,   s.size());
        assert_eq!(0,   s.get_fp());
    }
}
