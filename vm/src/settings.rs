

pub const SETTINGS_MEMORY_SIZE_BYTES:      usize = 0x40_00_00_00; // 1024 MB
pub const SETTINGS_CALC_STACK_SIZE_BYTES:  usize = 0x80_00_00_0;  // 128 MB  
pub const SETTINGS_JUMP_STACK_SIZE_BYTES:  usize = 0xF4240;       // 1 MB    - 15625 Jumps