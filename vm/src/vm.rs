use std::convert::TryInto;
use std::collections::HashMap;

use std::sync::Arc;
use std::sync::Mutex;

use crate::VmError;
use crate::instructions::*;
use crate::settings::*;
use crate::device::DeviceWindow;
use crate::Memory;
use crate::DbpOutput;
use crate::proc_thread::ProcessThread;
use crate::proc::Proc;
use crate::device_io::IODevice;
use crate::device_resp::RESPDevice;

#[cfg(debug)]
use crate::V;

use crossbeam_channel::unbounded;

use std::thread;
use std::time;

/// Communication from a processor to the VM owner
#[derive(Debug)]
pub(crate) enum ProcToVmComm {
    KillProc{ id: u8 },
    SpawnProc{ id: u8, addr: usize },
    Halt{ id: u8 },
    Panic{ id: u8, message: String }
}

/// A structure that holds the send/receive channels that facilitate communication
/// between the processor and the VM manager
struct VmProcComm {
    receiver: crossbeam_channel::Receiver<ProcToVmComm>,
    sender  : crossbeam_channel::Sender<ProcToVmComm>
}

/// The VM - sometimes referred to as the VM Manager. T
/// his structure facilitates the spawning / destruction
/// of processor threads
pub struct Vm {

    memory          : Arc<Mutex<Memory>>,                   // Memory shared by all processors
    process_threads : HashMap<u8, Option<ProcessThread>>,   // Processor threads
    primary_processor: Proc,                                // Main-thread processor
    proc_count         : u8,                                // Maximum allowed processors (including main thread)
    active_processors  : u8,                                // Currently active processor count

    end_ins_addr  : usize,                                  // Location of last instruction in memory
    interrupt_requests: Vec<usize>,                         // List of interrupt requests

    processor_comm: VmProcComm,                              // Processor communication channels


    io_device:     Option<IODevice>,                        // IO Device
    resp_device:   Option<RESPDevice>                       // Responder Device
}

impl Vm {
    /// Create a new virtual machine with some loaded byte code
    /// It will attempt to extract the base information to create the VM from the 
    /// bytecode before creation. If the file does not match bytecode specification
    /// the behavior of this function is considered "undefined"
    pub fn new(byte_code: Vec<u8>) -> Result<Self, VmError> {
        
        if byte_code.len() < 17 { return Err(VmError::InvalidBytecodeFile); }

        let raw_code_directive_loc : [u8; 8];
        extract_bytes!(raw_code_directive_loc, byte_code, 0, 8, VmError::InvalidBytecodeFile);

        let code_directive_loc = u64::from_le_bytes(raw_code_directive_loc) as usize;

        if byte_code.len() < code_directive_loc {
            #[cfg(debug)]
            eprintln!("Program not large enough to cover given code directive location : {}", code_directive_loc);
            return Err(VmError::InvalidBytecodeFile);
        }

        if byte_code[code_directive_loc] != INS_START_CODE {
            #[cfg(debug)]
            eprintln!("Code directive expected at location {}, but {} was found instead.", code_directive_loc, byte_code[code_directive_loc]);
            return Err(VmError::InvalidBytecodeFile);
        }

        let raw_entry_point : [u8; 8];
        extract_bytes!(raw_entry_point, byte_code, 8, 16, VmError::InvalidBytecodeFile);

        let entry_point = u64::from_le_bytes(raw_entry_point);

        if byte_code.len() < entry_point as usize {
            #[cfg(debug)]
            eprintln!("Program not large enough to cover given entry point : {}", entry_point);
            return Err(VmError::InvalidBytecodeFile);
        }

        let raw_proc : [u8; 1];
        extract_bytes!(raw_proc, byte_code, 16, 17, VmError::InvalidBytecodeFile);

        let proc_count = u8::from_le_bytes(raw_proc);

        let mut memory = Memory::new(SETTINGS_MEMORY_SIZE_BYTES);
        
        let mut windows : HashMap<u32, DeviceWindow> = HashMap::new();

        // Add the contents of the file into VM memory and build the device windows
        let mut addr : usize = 0;
        let mut ig_length : usize = 0;

        for item in byte_code.iter() {

            //
            //  Scan the code looking for an INS_LOAD_DEV instruction. If found the memory will be scanned ahead
            //  to build a device window. Using ig_length, the population section will then ignore all bytes until
            //  the outer loop catches up to its scanned regions.
            //
            match item.clone() {
                
                //  Found a new device ID
                //
                i if addr < code_directive_loc && addr >= ig_length && i == INS_LOAD_DEV => {

                    let mut addr_offset = addr + 1;

                    //  Load the ID 
                    //
                    let id_bytes : [u8; 4] = match byte_code[addr_offset..addr_offset+4].try_into() {
                        Ok(v) => { v },
                        Err(_) => { return Err(VmError::InvalidBytecodeFile); }
                    };
                    let id = u32::from_le_bytes(id_bytes);

                    if windows.contains_key(&id) {
                        eprintln!("Duplicate device IDs detected");
                        return Err(VmError::InvalidBytecodeFile);
                    }
                    let mut device = DeviceWindow::new(id);

                    addr_offset += 4;

                    // Load IRQ
                    //
                    let irq_bytes : [u8; 8] = match byte_code[addr_offset..addr_offset+8].try_into() {
                        Ok(v) => { v },
                        Err(_) => { 
                            eprintln!("Unable to retrieve irq address for device '{}'", id);
                            return Err(VmError::InvalidBytecodeFile); 
                        }
                    };
                    let irq_addr = u64::from_le_bytes(irq_bytes) as usize;

                    addr_offset += 8;

                    // Load flag location
                    //
                    let flag = addr_offset;
                    let flag_data = byte_code[addr_offset];

                    if irq_addr != 0xFFFF_FFFF_FFFF_FFFF {
                        device.interrupt_addr = Some(irq_addr)
                    }

                    if flag_data != 0xFF {
                        device.flag = Some(flag);
                    }

                    ig_length = addr_offset;

                    addr_offset += 1;

                    // Load Buffer Length
                    //
                    let buff_len_bytes : [u8; 4] = match byte_code[addr_offset..addr_offset+4].try_into() {
                        Ok(v) => { v },
                        Err(_) => { 
                            eprintln!("Unable to retrieve buffer length for device '{}'", id);
                            return Err(VmError::InvalidBytecodeFile); 
                        }
                    };
                    let buffer_len = u32::from_le_bytes(buff_len_bytes);

                    addr_offset += 4;

                    // Load Buffer Address
                    //
                    let buff_addr_bytes : [u8; 8] = match byte_code[addr_offset..addr_offset+8].try_into() {
                        Ok(v) => { v },
                        Err(_) => { 
                            eprintln!("Unable to retrieve buffer address for device '{}'", id);
                            return Err(VmError::InvalidBytecodeFile); 
                        }
                    };
                    let buffer_address = u64::from_le_bytes(buff_addr_bytes) as usize;

                    if buffer_len != 0xFFFF_FFFF && buffer_address != 0xFFFF_FFFF_FFFF_FFFF {
                        device.buffer = Some((buffer_len, buffer_address));
                    }

                    #[cfg(debug)]
                    println!("Device Information >>> {:?}", device);

                    // Insert the device info
                    windows.insert(id.clone(), device);
                }

                _ => {}
            }
            
            //
            //  Here we go through and attempt to move all data over to the actual memory object. 
            //  that means that the moment this is done there will technically be two copies of the program
            //  in the underlying system's memory until the caller gets rid of the existing data. This
            //  is fine for now as most programs are of a smaller nature, but once things start to be ironed out
            //  it will be good to revisit means to populate the memory object where we move the data in its
            //  entirety with one sweep.
            //
            if let Err(e) = memory.store_byte(addr, item.clone()) {
                eprintln!("Error populating memory with byte code : '{}'", e);
                return Err(VmError::GivenMemoryCapacityInsufficient);
            }
            addr += 1;
        }

        #[cfg(debug)]
        verbosity_show!(&V);

        #[cfg(debug)]
        {
            vlow!(&V, vfunc!(), format!("VmInfo : Processors '{:#X}'", proc_count).as_str());
            vlow!(&V, vfunc!(), format!("VmInfo : Memory '{:#X}' bytes", SETTINGS_MEMORY_SIZE_BYTES).as_str());
            vlow!(&V, vfunc!(), format!("VmInfo : Calculation Stack '{:#X}' bytes", SETTINGS_CALC_STACK_SIZE_BYTES).as_str());
            vlow!(&V, vfunc!(), format!("VmInfo : Jump Stack '{:#X}' bytes", SETTINGS_JUMP_STACK_SIZE_BYTES).as_str());
            vlow!(&V, vfunc!(), format!("VmInfo : Entry Point '{:#X}'", entry_point).as_str());
        }

        let arc_mem = Arc::new(Mutex::new(memory));

        // Communication channels so processors can talk to vm manager
        let (proc_sender, proc_receiver) : (crossbeam_channel::Sender<ProcToVmComm>, 
                                            crossbeam_channel::Receiver<ProcToVmComm>) = unbounded();



        // Make the VM
        let mut vm = Self{
            memory :   arc_mem.clone(),
            process_threads: HashMap::new(),
            primary_processor: Proc::new(0, entry_point.clone() as usize, addr.clone(), arc_mem.clone(), proc_sender.clone()),
            proc_count: proc_count,
            active_processors: 1,
            end_ins_addr: addr,
            io_device:  None,
            resp_device: None,
            interrupt_requests: Vec::new(),
            processor_comm: VmProcComm{ 
                receiver: proc_receiver,
                sender  : proc_sender
            }
        };

        // Populate processor hashmap
        for idx in 0..proc_count {
            vm.process_threads.insert(idx+1, None);
        }

        //  Scan all device things that were found and set them up
        //  if they exist and are valid
        //
        for (id, window) in windows.iter() {
            match *id {

                //  Build the IO device
                //
                i if i == DEVICE_ID_IO => {
                    vm.io_device = Some(
                        IODevice::new(
                            id.clone(),
                            window.clone(),
                            arc_mem.clone()
                        )
                    );
                }

                //  Build the RESP device
                //
                i if i == DEVICE_ID_RESP => {
                    vm.resp_device = Some(
                        RESPDevice::new(
                            id.clone(),
                            window.clone(),
                            arc_mem.clone()
                        )
                    );
                }

                unknown_id => {
                    eprintln!("Unknown Device ID {}", unknown_id);
                    return Err(VmError::UnknownDeviceId);
                }
            }
        }

        //
        //  Build the VM and hand it back
        //
        Ok(vm)
    }

    /// Get a list of the assigned processors
    /// This is useful if the caller wants to setup a debug channel
    /// to a given processor
    pub fn get_active_processors(&self) -> Vec<u8> {

        let mut result : Vec<u8>  = Vec::new();
        result.push(0); // Primary proc
        for (id, item) in self.process_threads.iter() {
            if let Some(_) = item {
                result.push(id.clone());
            }
        }
        result
    }

    /// Create a channel for the processor to send off a debug signal
    /// so the owner of the VM can control what to do when a debug command is 
    /// executed by the processor
    pub fn setup_debug_channel(&mut self, proc_id: u8) -> Result<crossbeam_channel::Receiver<DbpOutput>, VmError> {

        // Make sure that the processor id is valid
        if proc_id >= self.proc_count {
            return Err(VmError::InvalidProcessorId);
        }

        // Special case for main-thread processor
        if proc_id == 0 {
            let (s, r) : (crossbeam_channel::Sender<DbpOutput>, crossbeam_channel::Receiver<DbpOutput>) = unbounded();
            self.primary_processor.set_debug_channel(s);
            return Ok(r);
        }

        // We know its okay because  we populate tha map up-to self.proc_count
        let proc = self.process_threads.get_mut(&proc_id).unwrap();

        // If the processor is active, we create a debug channel
        if let Some(p) = proc {
            let (s, r) : (crossbeam_channel::Sender<DbpOutput>, crossbeam_channel::Receiver<DbpOutput>) = unbounded();
            if let Err(e) = p.set_debug_channel(s) {
                return Err(e);
            }
            return Ok(r);
        }

        // If the processor is not active its invalid
        return Err(VmError::InvalidProcessorId);
    }

    /// Spawn a new processor
    #[inline(always)]
    fn spawn_process(&mut self, entry_point: usize) -> Result<u8, VmError> {

        // Check if there is room within the given limit
        if self.active_processors >= self.proc_count  {
            return Err(VmError::InsufficientProcessors);
        }

        // Attempt to find a free processor
        let mut id : u8 = 1;
        let mut found = false;
        for (pid, proc) in self.process_threads.iter() {
            if let None = proc {
                id = pid.clone();
                found = true;
                break;
            }
        }

        // If no free processor found, we return an error - No more room!
        if !found {
            return Err(VmError::InsufficientProcessors);
        }

        let mut process_thread = 
            ProcessThread::new(
                id.clone(), 
                entry_point, 
                self.end_ins_addr, 
                self.memory.clone(), 
                self.processor_comm.sender.clone()
            );

        process_thread.start();

        // Setup the processor to start executing at the given address of memory
        self.process_threads.insert(
            id.clone(),
            Some(
                process_thread
            )
        );

        // Increase the number of active processors
        self.active_processors += 1;
        return Ok(id);
    } 

    /// Stop all processors
    #[inline(always)]
    fn stop_processors(&mut self) {

        for (_id, proc_thread) in self.process_threads.iter_mut() {

            if let Some(proc) = proc_thread {

                #[cfg(debug)]
                println!("Killing processor ({}) ", _id);

                while !proc.stop() {
                    thread::sleep(time::Duration::from_millis(1));
                }

                #[cfg(debug)]
                println!("Processor Killed");
            }

        }
        self.process_threads.clear();

        self.active_processors = 0;
    }

    ///  Tick VM devices if there are no pending interrupts, and add interrupts if the 
    ///  device generates them
    #[inline(always)]
    fn tick_devices(&mut self) -> Result<(), VmError> {

        //  Here we check if there are any interrupts that are requested by a given device.
        //  If an interrupt is present we attempt to handle it. If it can't be handled 
        //  we will try again next iteration.
        //
        if self.interrupt_requests.len() == 0 {

            // Tick the io device. See if there is anything to do. If its processed something and requests
            // an interrupt, add the item to the interrupt requests
            if let Some(device) = &mut self.io_device {

                let (perform_interrupt, address) = device.tick();  
                
                if perform_interrupt { self.interrupt_requests.push(address);  }
            }

            // Ditto responder device
            if let Some(device) = &mut self.resp_device {

                let (perform_interrupt, address) = device.tick();  
                
                if perform_interrupt { self.interrupt_requests.push(address);  }
            }


        } else {
            //  Get one interrupt request 
            
            let interrupt_addr = self.interrupt_requests.last().unwrap().clone();

            // Attempt to use the primary processor for interrupts before checking threads

            match self.primary_processor.interrupt(interrupt_addr) {
                // Check if the interrupt was completed
                Ok(acknowledged) => {
                    if acknowledged {
                        // If the request was acknowledged, pop it off the vector
                        self.interrupt_requests.pop();
                    }
                },
                Err(e) => {
                    return Err(e);
                }
            }

            /*

            Really, any thread could totally handle the interrupts. The problem is, I *think* we should
            only have the main thread handle the interrupts. This allows us to have a single responsible 
            holder of interrupts and allows us to ensure calls to devices block on the main thread. This
            doesn't sound ideal at first, but it is handy to know that (for instance) user IO blocks 
            the execution of the program so we don't have to busy wait. Realistically, I think this will be fine.
            Until I'm 1000% certain I will leave this here.


            // Attempt to find a processor to handle the interrupt 
            for (_id, process) in self.process_threads.iter_mut() {

                if let Some(proc) = process {

                    #[cfg(debug)]
                    println!("Processor '{}' handling current interrupt", _id);

                    // If this processor had interrupts enabled, attempt to interrupt it
                    match proc.interrupt(interrupt_addr) {

                        // Check if the interrupt was completed
                        Ok(acknowledged) => {
                            if acknowledged {
                                // If the request was acknowledged, pop it off the vector
                                self.interrupt_requests.pop();
                            }
                        },
                        Err(e) => {
                            return Err(e);
                        }
                    }
                }
            }
            */
        }
        Ok(())
    }

    /// Check for communications from the processors 
    #[inline(always)]
    fn handle_comms(&mut self) -> Result<(), VmError> {

        //  Check if a signal came from the processor and check
        //  what action its requesting. It might be asking to die. 
        //  It might be asking something else.
        if let Ok(comm)  = self.processor_comm.receiver.try_recv() {
            match comm {

                //  Cause a riot ... uh .. panic
                //
                ProcToVmComm::Panic{ id, message } => {

                    self.stop_processors();

                    println!("Processor {} executed panic with message '{}'",  id, message);
                    std::process::exit(1);
                },

                // Spawn a new process
                //
                ProcToVmComm::SpawnProc{ id, addr } => {

                    #[cfg(debug)]
                    println!("Processor Spawn Proc >> id {} | addr | {}", id, addr);

                    if let Err(e) = self.spawn_process(addr) {
                        eprintln!("Failed to spawn process request by proc {}", id);
                        return Err(e);
                    }
                },

                // Halt
                //
                ProcToVmComm::Halt{ id: _id } => {

                    #[cfg(debug)]
                    println!("Processor HALT >> id {}", _id);

                    self.stop_processors();
                }

                // Die
                //
                ProcToVmComm::KillProc{ id } => {

                    #[cfg(debug)]
                    println!("Processor Kill Proc >> id {}", id);

                    // Kill primary processor
                    if id == 0 {

                        // If the main processor stops, they must all stop

                        self.stop_processors();

                    } else {

                        // Kill a particular threaded process
                        match self.process_threads.get_mut(&id) {

                            Some( proc_thread ) => {

                                if let Some(proc) = proc_thread {

                                    #[cfg(debug)]
                                    println!("Killing processor ({}) ", id);
        
                                    while !proc.stop() {
                                        thread::sleep(time::Duration::from_millis(1));
                                    }
        
                                    #[cfg(debug)]
                                    println!("Processor Killed");

                                    if self.active_processors > 0 {

                                        self.active_processors -= 1;
                                    }
                                }
                            },

                            None => {
                                #[cfg(debug)]
                                println!("Failed to retrieve processor with id : {}", id);
                            }
                        };
                    }
                }
            }
        }
        Ok(())
    }

    /// Returns Ok(completed) or Err(VmError) 
    /// If 'completed' == true all processor instructions have been executed
    #[inline(always)]
    pub fn step(&mut self) -> Result<bool, VmError> {

        //  Check for communications from processors 
        //
        if let Err(e) = self.handle_comms() {
            return Err(e);
        }

        //  Tick the devices and handle their interrupts
        //
        if let Err(e) = self.tick_devices() {
            return Err(e);
        }

        //  If there are no more active processors we are completed
        //
        if self.active_processors == 0 {
            return Ok(true)
        }

        // If not all processes are complete, we need to manually step the main thread processor
        //
        match self.primary_processor.step() {
            Ok(value) => {
                return Ok(value);
            },
            Err(e) => {
                self.stop_processors();
                return Err(e);
            }
        }
    }
}