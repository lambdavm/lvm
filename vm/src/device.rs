/// Parameters to retrieve slices from the VM memory
/// related to device interoperability. This is for use internally 
/// as it hosts meta information regarding shared memory
#[derive(Debug, Clone)]
pub(crate) struct DeviceWindow {
    pub(crate) id: u32,
    pub(crate) interrupt_addr: Option<usize>,
    pub(crate) flag:           Option<usize>,
    pub(crate) buffer:         Option<(u32, usize)> // Length and address of the buffer
}

impl DeviceWindow {

    /// Create a new device window
    pub (crate) fn new(id: u32) -> Self {
        Self {
            id: id,
            interrupt_addr: None,
            flag:           None,
            buffer:         None
        }
    }
}
