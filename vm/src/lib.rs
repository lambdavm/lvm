#[cfg(not(target_pointer_width = "64"))]
compile_error!("LVM's VM crate only supports 64-bit architectures (currently)");

extern crate derive_more;
use derive_more::{Display};

mod errors;
pub use errors::{ VmError, VmDeviceError };

#[macro_use]
mod macros;

#[allow(dead_code)]
mod memory;
use memory::Memory;

mod stack;
use stack::Stack;

mod device;

mod proc;
mod proc_thread;

#[cfg(debug)]
#[macro_use]
extern crate verbosity;

#[cfg(debug)]
#[macro_use]
extern crate lazy_static;

extern crate instructions;

mod settings;
mod device_io;
mod device_resp;

mod vm;
pub use crate::vm::Vm;

// Create the 'verbosity' object iff we are set to build debug information. 
// This object makes nice output showing function origin and whatnot for output
#[cfg(debug)]
lazy_static! {
    static ref V: verbosity::Verbosity = verbosity_high!();
}

// The output of a processor's Dbp Instruction 
// iff the channel is constructed
#[derive(Display)]
#[display(fmt = "DBP >> Instruction Pointer : {} | Registers : {:?}", ins_ptr, registers)]
pub struct DbpOutput {
    ins_ptr:   usize,
    registers: Vec<i64>
}
