

/// Extract a range of bytes from a container and return from method
/// with a given error if it fails. Requires (std::convert::TryInto)
#[macro_export]
macro_rules! extract_bytes {
    ($result:expr, $container:expr, $lower:tt, $upper:tt, $error:expr) => {
        $result = match $container[$lower..$upper].try_into() {
            Ok(value) => {
                value
            },
            Err(_) => {
                return Err($error);
            }
        }
    };
}

/// Obtain the memory mutex
macro_rules! get_mutex {
    ($self:expr, $guard:expr) => {
        
        $guard = match $self.memory.lock() {
            Ok(guard) => guard,
            Err(_)    => { return Err(VmError::MemoryAccessError); }
        };
    };
}

/// Retrieve a byte from memory at a given index
#[macro_export]
macro_rules! ret_mem_byte {
    ($result:expr, $self:expr, $idx:expr, $err:expr) => {
        {
            let memory_guard : std::sync::MutexGuard<Memory>;
            get_mutex!($self, memory_guard);

            $result = match memory_guard.get_byte($idx) {
                Ok(b)  => { b },
                Err(e) => {
                    eprintln!("Error retrieving byte from memory : {}", e);
                    
                    return Err($err);
                } 
            };
        }
    };
}

/// Retrieve a quarter-word from memory at a given index
#[macro_export]
macro_rules! ret_mem_q {
    ($result:expr, $self:expr, $idx:expr, $err:expr) => {
        {
            let memory_guard : std::sync::MutexGuard<Memory>;
            get_mutex!($self, memory_guard);
            $result = match memory_guard.get_quarter($idx) {
                Ok(b)  => { b },
                Err(e) => {
                    eprintln!("Error retrieving quarter-word from memory : {}", e);
                    
                    return Err($err);
                } 
            };
        }
    };
}

/// Retrieve a half-word from memory at a given index
#[macro_export]
macro_rules! ret_mem_h {
    ($result:expr, $self:expr, $idx:expr, $err:expr) => {
        {
            let memory_guard : std::sync::MutexGuard<Memory>;
            get_mutex!($self, memory_guard);
            $result = match memory_guard.get_half($idx) {
                Ok(b)  => { b },
                Err(e) => {
                    eprintln!("Error retrieving half-word from memory : {}", e);
                    
                    return Err($err);
                } 
            };
        }
    };
}

/// Retrieve a word from memory at a given index
#[macro_export]
macro_rules! ret_mem_word {
    ($result:expr, $self:expr, $idx:expr, $err:expr) => {
        {
            let memory_guard : std::sync::MutexGuard<Memory>;
            get_mutex!($self, memory_guard);
            $result = match memory_guard.get_word($idx) {
                Ok(b)  => { b },
                Err(e) => {
                    eprintln!("Error retrieving word from memory : {}", e);
                    
                    return Err($err);
                } 
            };
        }
    };
}

/// Store a byte in memory at a given index
#[macro_export]
macro_rules! store_mem_byte {
    ($value:expr, $self:expr, $idx:expr, $err:expr) => {
        {
            let mut memory_guard : std::sync::MutexGuard<Memory>;
            get_mutex!($self, memory_guard);
            if let Err(e) = memory_guard.store_byte($idx, $value) {
                eprintln!("Error storing byte to memory : {}", e);
                
                return Err($err);
            };
        }
    };
}

/// Store a quarter-word in memory at a given index
#[macro_export]
macro_rules! store_mem_q {
    ($value:expr, $self:expr, $idx:expr, $err:expr) => {
        {
            let mut memory_guard : std::sync::MutexGuard<Memory>;
            get_mutex!($self, memory_guard);
            if let Err(e) = memory_guard.store_quarter($idx, $value) {
                eprintln!("Error storing quarter-word to memory : {}", e);
                
                return Err($err);
            };
        }
    };
}

/// Store a half-word in memory at a given index
#[macro_export]
macro_rules! store_mem_h {
    ($value:expr, $self:expr, $idx:expr, $err:expr) => {
        {
            let mut memory_guard : std::sync::MutexGuard<Memory>;
            get_mutex!($self, memory_guard);
            if let Err(e) = memory_guard.store_half($idx, $value) {
                eprintln!("Error storing half-word to memory : {}", e);
                
                return Err($err);
            };
        }
    };
}

/// Store a word in memory at a given index
#[macro_export]
macro_rules! store_mem_word {
    ($value:expr, $self:expr, $idx:expr, $err:expr) => {
        {
            let mut memory_guard : std::sync::MutexGuard<Memory>;
            get_mutex!($self, memory_guard);
            if let Err(e) = memory_guard.store_word($idx, $value) {
                eprintln!("Error storing word to memory : {}", e);
                
                return Err($err);
            };
        }
    };
}

/// Retrieve 3 sequential bytes from memory that represent registers
#[macro_export]
macro_rules! three_reg {
    ($self:expr, $dest:expr, $lhs:expr, $rhs:expr) => {
        ret_mem_byte!($dest, $self, $self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
        ret_mem_byte!($lhs,  $self, $self.ins_ptr+2, VmError::UnableToRetrieveInstruction);
        ret_mem_byte!($rhs,  $self, $self.ins_ptr+3, VmError::UnableToRetrieveInstruction);
    };
}

/// Retrieve items for a branch instruction from memory
#[macro_export]
macro_rules! branch_items {
    ($self:expr, $lhs:expr, $rhs:expr, $addr:expr) => {
        ret_mem_byte!($lhs,  $self, $self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
        ret_mem_byte!($rhs,  $self, $self.ins_ptr+2, VmError::UnableToRetrieveInstruction);
        ret_mem_word!($addr, $self, $self.ins_ptr+3, VmError::UnableToRetrieveInstruction);
    };
}

/// Retrieve a byte from the stack at a given index
#[macro_export]
macro_rules! ret_stack_byte {
    ($result:expr, $self:expr, $idx:expr, $err:expr) => {
        $result = match $self.stack.get_byte($idx) {
            Ok(b)  => { b },
            Err(e) => {
                eprintln!("Error retrieving byte from stack : {}", e);
                return Err($err);
            } 
        };
    };
}

/// Retrieve a quarter-word from the stack at a given index
#[macro_export]
macro_rules! ret_stack_q {
    ($result:expr, $self:expr, $idx:expr, $err:expr) => {
        $result = match $self.stack.get_quarter($idx) {
            Ok(b)  => { b },
            Err(e) => {
                eprintln!("Error retrieving quarter-word from stack : {}", e);
                return Err($err);
            } 
        };
    };
}

/// Retrieve a half-word from the stack at a given index
#[macro_export]
macro_rules! ret_stack_h {
    ($result:expr, $self:expr, $idx:expr, $err:expr) => {
        $result = match $self.stack.get_half($idx) {
            Ok(b)  => { b },
            Err(e) => {
                eprintln!("Error retrieving half-word from stack : {}", e);
                return Err($err);
            } 
        };
    };
}

/// Retrieve a word from the stack at a given index
#[macro_export]
macro_rules! ret_stack_word {
    ($result:expr, $self:expr, $idx:expr, $err:expr) => {
        $result = match $self.stack.get_word($idx) {
            Ok(b)  => { b },
            Err(e) => {
                eprintln!("Error retrieving word from stack : {}", e);
                return Err($err);
            } 
        };
    };
}

/// Store a byte from the stack at a given index
#[macro_export]
macro_rules! store_stack_byte {
    ($value:expr, $self:expr, $idx:expr, $err:expr) => {
        if let Err(e) = $self.stack.store_byte($idx, $value) {
            eprintln!("Error storing byte to stack : {}", e);
            return Err($err);
        };
    };
}

/// Store a quarter-word from the stack at a given index
#[macro_export]
macro_rules! store_stack_q {
    ($value:expr, $self:expr, $idx:expr, $err:expr) => {
        if let Err(e) = $self.stack.store_quarter($idx, $value) {
            eprintln!("Error storing quarter-word to memory : {}", e);
            
            return Err($err);
        };
    };
}

/// Store a half-word from the stack at a given index
#[macro_export]
macro_rules! store_stack_h {
    ($value:expr, $self:expr, $idx:expr, $err:expr) => {
        if let Err(e) = $self.stack.store_half($idx, $value) {
            eprintln!("Error storing half-word to memory : {}", e);
            return Err($err);
        };
    };
}

/// Store a word from the stack at a given index
#[macro_export]
macro_rules! store_stack_word {
    ($value:expr, $self:expr, $idx:expr, $err:expr) => {
        if let Err(e) = $self.stack.store_word($idx, $value) {
            eprintln!("Error storing word to memory : {}", e);
            return Err($err);
        };
    };
}