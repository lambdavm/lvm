/*

    This Device was fleshed out as a POC of a real "IO" device. It follows the standards set forth by
    the "Device_IO.md" document, but its not a fantastic implementation. Rewriting this would be good
    low handing fruit.
*/

const IO_INS_READ:        u8 = 0x10;
const IO_INS_WRITE_CHAR:  u8 = 0x11;

const IO_INS_STDIO:       u8 = 0x01;
const IO_INS_STDER:       u8 = 0x02;

use std::io::{self, Write};

use crate::device::DeviceWindow;
use crate::Memory;

use std::sync::Arc;
use std::sync::Mutex;
use std::str;

/// The User IO Device
#[allow(dead_code)]
pub(crate) struct IODevice {

    id: u32,
    device_window: DeviceWindow,
    memory:        Arc<Mutex<Memory>>
}

impl IODevice {

    /// Create a new device with the id memory and window information
    pub fn new(id: u32, dw: DeviceWindow, memory: Arc<Mutex<Memory>>) -> Self {

        Self {
            id: id,
            device_window: dw,
            memory: memory
        }
    }

    /// Tick the device to check if it needs to act
    pub fn tick(&mut self) -> (bool, usize) {

        // If the flag isn't set then we can't be triggered
        let flag = match self.device_window.flag {
            Some(value) => { value },
            None        => { return (false, 0); }
        };

        let (addr_valid, interrupt_addr) = match self.device_window.interrupt_addr {
            Some(addr)  => { (true, addr) }
            None        => { (false,   0) }
        };

        // If an input buffer wasn't assigned then IO is useless so we go away
        let (_, input_start) = match self.device_window.buffer {
            Some(value) => { value },
            None        => { return (false, 0); }
        };

        // Read in the flag from memory. If the device flag is set,
        {
            // Get a memory guard
            let mut memory_guard = match self.memory.lock() {
                Ok(guard) => guard,
                Err(_)    => { return (false, 0); }
            };

            // Get the flag value, if not set we return
            let _ = match memory_guard.get_byte(flag) {
                Ok(value) => {
                    if value == 1 { true } else { return (false, 0); }
                },
                Err(_) => {
                    return (false, 0);
                }
            };

            #[cfg(debug)]
            println!("IO Device has had its flag set");

            // Reset flag
            let _ = memory_guard.store_byte(flag, 0x00);
        }


        #[cfg(debug)]
        {
            println!("FLAG LOCATION : {}", flag);
            println!("START LOC     : {}", input_start);
        }

        // read in the input buffer for request

        let mut _command : u8 = 255;
        
        {
            #[cfg(debug)]
            println!("IO Device '{}' was triggered~! ", self.id);

            let memory_guard = match self.memory.lock() {
                Ok(guard) => guard,
                Err(_)    => { return (false, 0); }
            };

            _command = match memory_guard.get_byte(input_start) {
                Ok(value) => {
                    value
                },
                Err(_) => {
                    return (false, 0);
                }
            };
        }

        #[cfg(debug)]
        println!("COMMAND : {:#04X}", _command);

        let request_interrupt = match _command {

            i if i == IO_INS_READ => {

                let mut _num_chars : u32 = 0;

                {
                    let memory_guard = match self.memory.lock() {
                        Ok(guard) => guard,
                        Err(_)    => { return (false, 0); }
                    };

                    _num_chars = match memory_guard.get_half(input_start+1) {
                        Ok(value) => {
                            value
                        },
                        Err(_) => {
                            return (false, 0);
                        }
                    };
                }


                #[cfg(debug)]
                println!("Read");
        
                self.read_line(_num_chars)
            }

            i if i == IO_INS_WRITE_CHAR => {


                #[cfg(debug)]
                println!("Write");
        
                self.write()
            }
            
            _ => {
                // Unknown Instruction
                false
            }
        };

        // If they gave us an interrupt address and we want to request an interrupt 
        // then we do so
        if addr_valid && request_interrupt {
            return (true, interrupt_addr);
        }
        // If we get and we can't or don't need to send an interrupt then don't
        (false, 0)
    }
    
    /// Read a line from stdin
    fn read_line(&mut self, n_chars: u32) -> bool {

        // If an output buffer wasn't assigned then IO is useless so we go away
        let (_, out_start) = match self.device_window.buffer {
            Some(value) => { value },
            None        => { return false }
        };

        #[cfg(debug)]
        println!("IODevice :: Read line : {}", n_chars);

        let mut input = String::new();
        if let Err(_) =  std::io::stdin().read_line(&mut input) {
            return false;
        }

        #[cfg(debug)]
        println!("IODevice :: Read : {}", input);

        if input.len() > n_chars as usize {
            input = input[0..n_chars as usize].to_string();
        }

        #[cfg(debug)]
        println!("IODevice :: Chopped down to : {}", input);

        let mut addr = out_start;

        let mut memory_guard = match self.memory.lock() {
            Ok(guard) => guard,
            Err(_)    => { return false; }
        };

        // Store the byte indicating that the reader is what will trigger interrupt
        let _ = memory_guard.store_byte(addr, IO_INS_READ);
        addr += 1;

        // Store each byte retrieved
        for single_byte in input.as_bytes().iter() {

            let _ = memory_guard.store_byte(addr, single_byte.clone());
            addr += 1;
        }

        let _ = io::stdout().flush();
        true
    }

    /// Write something out to either stderr or stdout
    fn write(&mut self) -> bool {
        // If an output buffer wasn't assigned then IO is useless so we go away
        let (len, mut start) = match self.device_window.buffer {
            Some(value) => { value },
            None        => { return false }
        };

        let mut memory_guard = match self.memory.lock() {
            Ok(guard) => guard,
            Err(_)    => { return false; }
        };

        let use_stdio = match memory_guard.get_byte(start+1) {
            Ok(value) => {

                #[cfg(debug)]
                println!("Writer Destination : {:#04X}", value);

                match value {
                    i if i == IO_INS_STDER => { false },
                    i if i == IO_INS_STDIO => { true },
                    _ => { 
                        #[cfg(debug)]
                        println!("Writer destination error");
                        return false; 
                    }
                }
            }

            Err(_) => {
                return false;
            }
        };

        start += 1;

        let perform_callback = match memory_guard.get_byte(start) {
            Ok(value) => {

                #[cfg(debug)]
                println!("Writer Perform Callback : {:#04X}", value);

                match value {
                    i if i == 1 => { true },
                    _ => { 
                        false
                    }
                }
            }

            Err(_) => {
                return false;
            }
        };

        start += 1;

        let mut r : Vec<u8> = Vec::new();

        for item in start .. start + len as usize {
            match memory_guard.get_byte(item) {
                Ok(v)  => { r.push(v); }
                Err(_) => {
                    return false;
                }
            };
        }

        let output = str::from_utf8(&r).unwrap();

        #[cfg(debug)]
        println!("Writer Data : {}", output);

        if use_stdio 
        {
            print!("{}", output); 
        } 
        else 
        {
            eprint!("{}", output);
        }

        let _ = io::stdout().flush();
        
        for x in start .. start+len as usize {
            let _ = memory_guard.store_byte(x, 0x00);
        }

        // Store the byte indicating that the writer is what will trigger interrupt
        let _ = memory_guard.store_byte(start, IO_INS_WRITE_CHAR);

        perform_callback
    }
    
}
