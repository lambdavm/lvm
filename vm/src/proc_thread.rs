use crate::proc::Proc;
use crate::VmError;
use crate::Memory;
use crate::vm::ProcToVmComm;
use crate::DbpOutput;

use std::sync::Arc;
use std::sync::Mutex;
use std::sync;
use std::thread;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;


/// This struct wraps a vm Proc (processor) with the information to allow it to be
/// threaded. This class will handle the thread and facilitate communication to the
/// processor from the owner vm
pub(crate) struct ProcessThread {
    processor: Arc<Mutex<Proc>>,
    
    handle: Option<thread::JoinHandle<Result<bool, VmError>>>,
    alive: sync::Arc<AtomicBool>,
}

impl ProcessThread {

    /// Create a new thread object. Parameters are to crate the underlying processor.
    /// Once created the 'start' method will need to be triggered to get the processor running
    pub (crate) fn new(id: u8, start_address: usize, end_address: usize, memory: Arc<Mutex<Memory>>, comm: crossbeam_channel::Sender<ProcToVmComm>) -> Self {
        Self {
            processor: Arc::new(Mutex::new(Proc::new(id, start_address, end_address, memory, comm))),
            handle: None,
            alive: sync::Arc::new(AtomicBool::new(false)),
        }
    }

/*
    This has been commented out as I'm not sure we want the threaded procs to handled interrupts. 
    See the comment at the bottom of "vm.rs"


    /// Attempt to pass an interrupt to the processor
    pub (crate) fn interrupt(&mut self, address: usize) -> Result<bool, VmError> {

        let mut process_guard = match self.processor.lock() {
            Ok(guard) => guard ,
            Err(_)    => { return Err(VmError::ProcessAccessError); }
        };

        return process_guard.interrupt(address);
    }
*/
    /// Attempt to set the debug channel
    pub (crate) fn set_debug_channel(&mut self, chan: crossbeam_channel::Sender<DbpOutput>) -> Result<(), VmError> {

        let mut process_guard = match self.processor.lock() {
            Ok(guard) => guard ,
            Err(_)    => { return Err(VmError::ProcessAccessError); }
        };

        process_guard.set_debug_channel(chan);

        return Ok(());
    }

    /// Start the thread
    pub (crate) fn start(&mut self)
    {
        // If alive, bail
        if self.alive.load(Ordering::SeqCst) {
            return;
        }

        // Indicate alive
        self.alive.store(true, Ordering::SeqCst);

        // Clone the required data to operate the processor
        let alive = self.alive.clone();
        let proc = self.processor.clone();

        // Create the thread / handle that will be stepping the processor
        self.handle = Some(thread::spawn(move || {
            let proc = proc.clone();

            // Step the processor if someone isn't trying to access it
            while alive.load(Ordering::SeqCst) {

                let mut process_guard = match proc.lock() {
                    Ok(guard) => guard ,
                    Err(_)    => { return Err(VmError::ProcessAccessError); }
                };

                if let Err(e) = process_guard.step() {
                    return Err(e);
                }
            }

            return Ok(true);
        }));
    }

    /// Stop the thread. If the thread is stopped, or isn't running already
    /// true will be returned. If the thread couldn't be joined, false will
    /// be returned 
    pub (crate) fn stop(&mut self) -> bool {
        if self.alive.load(Ordering::SeqCst) {
            self.alive.store(false, Ordering::SeqCst);
            if let Some(handle) = self.handle.take() {
                if let Err(_) = handle.join() {
                    return true;
                }
            }
            return false;
        }
        true
    }
}