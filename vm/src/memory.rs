
extern crate derive_more;
use derive_more::{Display};

/// Memory errors
#[derive(Display)]
pub enum MemoryError {

    OutOfBounds,
    InsufficientMemory
}

/// Memory object
#[derive(Debug, Clone)]
pub(crate) struct Memory {

    data:  Vec<u8>,
}

impl Memory {

    /// Create a memory object of a given size (bytes)
    pub(crate) fn new(mem_size: usize) -> Self{
        Self {
            data: vec![0;  mem_size],
        }
    }

    /// A debug-only method that can be used to dump the contents of memory
    #[cfg(debug)]
    pub(crate) fn dump(&self) {
        println!("----------------[ MEMORY DUMP ]---------------");
        let mut p : u8  = 0;
        let mut l : u64 = 1;
        for item in self.data.iter() {

            if p % 10 == 0 || p == 0 {
                print!("{}\t", l);
                l += 1;
            }

            print!("| {} ", format!("{:#04X}", item));
            p += 1;
            if p % 10 == 0 {
                p = 0;
                println!("");
            }
        }
        println!("");
        println!("----------------------------------------------");
    }

    /// A debug-only method that allows the export of the current memory data
    #[cfg(debug)]
    pub(crate) fn get_memory(&self) -> Vec<u8> {
        return self.data.clone();
    }

    /// Get a slice of memory 
    pub(crate) fn get_slice(&self, start:usize, end:usize) -> &[u8] {
        &self.data[start..end]
    }

    /// Get a mutable slice of memory
    pub(crate) fn get_mut_slice(&mut self, start:usize, end:usize) -> & mut [u8] {
        &mut self.data[start..end]
    }

    /// Get a reference to a byte in memory
    pub(crate) fn get_ref(&self, pos:usize) -> & u8 {
        &self.data[pos]
    }

    /// Get a mutable reference to a byte in memory
    pub(crate) fn get_mut_ref(&mut self, pos:usize) -> & mut u8 {
        &mut self.data[pos]
    }

    /// Store a byte in memory
    pub(crate) fn store_byte(&mut self, addr: usize, data: u8) -> Result<(), MemoryError> {

        if addr >= self.data.len() {
            return Err(MemoryError::OutOfBounds);
        }
        self.data[addr] = data;
        return Ok(());
    }

    /// Get a byte from memory
    pub(crate) fn get_byte(&self, addr: usize) -> Result<u8, MemoryError> {

        if addr >= self.data.len() {
            return Err(MemoryError::OutOfBounds);
        }
        return Ok(self.data[addr]);
    }

    /// Store two bytes in memory
    pub(crate) fn store_quarter(&mut self, addr: usize, data: u16) -> Result<(), MemoryError> {

        if addr + 1 >= self.data.len() {
            return Err(MemoryError::OutOfBounds);
        }
        self.data[addr+1]     = (data >> 8) as u8;
        self.data[addr]       = (data >> 0) as u8;
        return Ok(());
    }

    /// Get two bytes from memory
    pub(crate) fn get_quarter(&self, addr: usize) -> Result<u16, MemoryError> {

        if addr + 1 > self.data.len() {
            return Err(MemoryError::OutOfBounds);
        }
        let ret : u16 = (self.data[addr+1]   as u16) << 8 |
                        (self.data[addr] as u16);
        return Ok(ret);
    }

    /// Store four bytes in memory
    pub(crate) fn store_half(&mut self, addr: usize, data: u32) -> Result<(), MemoryError> {

        if addr + 3 >= self.data.len() {
            return Err(MemoryError::OutOfBounds);
        }
        self.data[addr+3] = (data >> 24) as u8;
        self.data[addr+2] = (data >> 16) as u8;
        self.data[addr+1] = (data >> 8 ) as u8;
        self.data[addr]   = (data >> 0 ) as u8;
        return Ok(());
    }

    /// Get four bytes from memory
    pub(crate) fn get_half(&self, addr: usize) -> Result<u32, MemoryError> {

        if addr + 3 > self.data.len() {
            return Err(MemoryError::OutOfBounds);
        }
        let ret : u32 = (self.data[addr+3] as u32) << 24 |
                        (self.data[addr+2] as u32) << 16 |
                        (self.data[addr+1] as u32) << 8  |
                        (self.data[addr]   as u32) << 0  ;
        return Ok(ret);
    }

    /// Store eight bytes (a word) in memory
    pub(crate) fn store_word(&mut self, addr: usize, data: u64) -> Result<(), MemoryError> {

        if addr+7 >= self.data.len() {
            return Err(MemoryError::OutOfBounds);
        }

        for x in 0..8 {
            self.data[addr + x] = (data >> x * 8) as u8;
        }
        return Ok(());
    }

    /// Get eight bytes (a word) from memory
    pub(crate) fn get_word(&self, addr: usize) -> Result<u64, MemoryError> {

        if addr+7 > self.data.len() {
            return Err(MemoryError::OutOfBounds);
        }

        let mut data: u64 = 0;
        for x in 0..8 {

            data = data | (((self.data[addr + x] as u64) << x * 8) as u64);
        }
        return Ok(data);
    }
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn memory_data_test_bytes() {
        let mut memory = Memory::new(20);

        for x in 0..20 {
            if let Err(_) =  memory.store_byte(x, 99) {
                panic!("Unable to store byte");
            }
        }
        if let Ok(_) =  memory.store_byte(20, 99) {
            panic!("Able to store byte out of range");
        }

        if let Err(_) = memory.store_byte(14, 214) {
            panic!("Unable to update value");
        }

        match memory.get_byte(14) {
            Ok(val) => {
                if val != 214 {
                    panic!("Failed to retrieve correct byte");
                }
            },
            Err(_) => {
                panic!("Failed to retrieve correct byte");
            }
        }
    }

    #[test]
    fn memory_data_test_quarters() {
        let mut memory = Memory::new(20*4);

        for x in 0..20 {
            if let Err(_) =  memory.store_quarter(x, 99) {
                panic!("Unable to store q");
            }
        }
        if let Ok(_) =  memory.store_quarter(20*4, 99) {
            panic!("Able to store q out of range");
        }

        if let Err(_) = memory.store_quarter(14, 214) {
            panic!("Unable to update value");
        }

        match memory.get_quarter(14) {
            Ok(val) => {
                if val != 214 {
                    panic!("Failed to retrieve correct q");
                }
            },
            Err(_) => {
                panic!("Failed to retrieve correct q");
            }
        }
    }

    #[test]
    fn memory_data_test_half() {
        let mut memory = Memory::new(20*4);

        for x in 0..20 {
            if let Err(_) =  memory.store_half(x, 99) {
                panic!("Unable to store half-word");
            }
        }
        if let Ok(_) =  memory.store_half(20*4, 99) {
            panic!("Able to store half-word out of range");
        }

        if let Err(_) = memory.store_half(14, 214) {
            panic!("Unable to update value");
        }

        match memory.get_half(14) {
            Ok(val) => {
                if val != 214 {
                    panic!("Failed to retrieve correct half-word");
                }
            },
            Err(_) => {
                panic!("Failed to retrieve correct half-word");
            }
        }
    }

    #[test]
    fn memory_data_test_words() {
        let mut memory = Memory::new(8*100);

        for x in 0..20 {
            if let Err(_) =  memory.store_word(x, 144000) {
                panic!("Unable to store word");
            }

            match memory.get_word(x) {
                Ok(val) => {
                    if val != 144000 {
                        panic!("Failed to retrieve correct word : {}", val);
                    }
                },
                Err(_) => {
                    panic!("Failed to retrieve correct byte");
                }
            }
        }

        if let Ok(_) =  memory.store_word(8*100, 99) {
            panic!("Able to store word out of range");
        }

        if let Err(_) = memory.store_word(14, 1650011) {
            panic!("Unable to update word");
        }

        match memory.get_word(14) {
            Ok(val) => {
                if val != 1650011 {
                    panic!("Failed to retrieve correct word : {}", val);
                }
            },
            Err(_) => {
                panic!("Failed to retrieve correct byte");
            }
        }
    }
}