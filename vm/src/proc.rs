
use crate::Stack;
use crate::Memory;
use crate::settings::SETTINGS_CALC_STACK_SIZE_BYTES;
use crate::settings::SETTINGS_JUMP_STACK_SIZE_BYTES;
use crate::VmError;
use crate::instructions::*;

use std::sync::Arc;
use std::sync::Mutex;

use crate::DbpOutput;

use crate::vm::ProcToVmComm;

use std::time;
use std::thread;
use std::collections::HashMap;

#[cfg(debug)]
use crate::V;


/// The VM processor structure. This structure is responsible for the execution of LVM bytecode
pub(crate) struct Proc {
    id                : u8,                                             // Processor ID
    memory            : Arc<Mutex<Memory>>,                             // VM shared memory
    stack             : Stack,                                          // Calculation / Storage stack
    jmp_stack         : Stack,                                          // Jump / Return stack
    registers         : Vec<i64>,                                       // Processor registers
    interrupts_enabled: bool,                                           // Interrupts enabled flag
    ins_ptr           : usize,                                          // Current instruction
    ins_end           : usize,                                          // End of instructions
    debug_channel     : Option<crossbeam_channel::Sender<DbpOutput>>,   // Outbound communication channel for debug instruction
    vm_comm           : crossbeam_channel::Sender<ProcToVmComm>,        // Communication channel to the VM manager
    memory_core       : HashMap < u32, Vec<u8> >                        // Processor-specific memory storage
}

impl Proc {

    /// Create a new processor
    pub(crate) fn new(id: u8, start_address: usize, end_address: usize, memory: Arc<Mutex<Memory>>, comm: crossbeam_channel::Sender<ProcToVmComm>) -> Self {

        Self {
            id: id,
            memory: memory,
            stack : Stack::new(SETTINGS_CALC_STACK_SIZE_BYTES),
            jmp_stack: Stack::new(SETTINGS_JUMP_STACK_SIZE_BYTES),
            registers: vec![0; 16],
            interrupts_enabled: true,
            ins_ptr: start_address,
            ins_end: end_address,
            debug_channel: None,
            vm_comm: comm,
            memory_core: HashMap::new()
        }
    }

    /// Set a debug channel that enables the 'DBP' instruction to report out current instruction
    /// information. This is not debug-only as it is a built-in instruction that can be run even
    /// in a release build.
    pub(crate) fn set_debug_channel(&mut self, chan: crossbeam_channel::Sender<DbpOutput>) {

        self.debug_channel = Some(chan)
    }

    /// Returns Ok(true) if the interrupt was acknowledged (interrupts were enabled)
    /// Returns Ok(false) if the interrupt was not acknowledged meaning it will have to be
    /// sent again once interrupts are enabled
    #[inline(always)]
    pub(crate) fn interrupt(&mut self, address: usize) -> Result<bool, VmError> {

        if !self.interrupts_enabled {
            return Ok(false);
        }

        #[cfg(debug)]
        vlow!(&V, vfunc!(), format!("INTERRUPT! Destination : {}", address).as_str());

        // Add the current instruction to the jmp stack
        // so a return can happen from interrupt and get things back on track
        if let Err(e) = self.jmp_stack.push_word(self.ins_ptr as u64) {
            return Err(VmError::JumpStackError{ error: e});
        }

        // Set the instruction pointer to the interrupt address
        self.ins_ptr = address;

        return Ok(true);
    }
    
    /// Step the instruction execution one step. A step is one "Fetch, decode, execute, store" iteration
    #[inline(always)]
    pub(crate) fn step(&mut self) -> Result<bool, VmError> {

        // Get the instruction. If we are out of instructions we will
        // perform a NOP instruction
        let ins_byte = if self.ins_ptr >= self.ins_end {

            INS_NOP
        } else {

            let memory_guard = match self.memory.lock() {
                Ok(guard) => guard,
                Err(_)    => { return Err(VmError::MemoryAccessError); }
            };

            match memory_guard.get_byte(self.ins_ptr) {
                Ok(b)  => { b },
                Err(e) => {
                    eprintln!("Error retrieving instruction : {}", e);
                    return Err(VmError::UnableToRetrieveInstruction);
                } 
            }
        };
        
       // #[cfg(debug)]
       // vlow!(&V, vfunc!(), format!("Position : {} | Instruction : {:X}", self.ins_ptr, ins_byte).as_str());

        match ins_byte {

            //
            //  NOP Instruction
            //  
            INS_NOP  => {   
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_NOP", self.id).as_str());
                self.ins_ptr += 1;
            }
            
            //
            //  FP Instruction
            //
            INS_FP => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_FP", self.id).as_str());

                let dest  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                
                let fp_value = self.stack.get_fp();

                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("INS_FP Value : {}", fp_value).as_str());

                self.registers[dest as usize] = fp_value as i64;

                self.ins_ptr += 2;
            }
            
            //
            //  UFP Instruction
            //
            INS_UFP => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_UFP", self.id).as_str());

                let source  : u8;
                ret_mem_byte!(source, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                
                let new_fp_value = self.registers[source as usize] as usize;

                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("INS_UFP Value : {}", new_fp_value).as_str());

                self.stack.update_fp(new_fp_value);

                self.ins_ptr += 2;
            }

            //
            //  ES Instruction
            //
            INS_ES => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_ES", self.id).as_str());

                let src  : u8;
                ret_mem_byte!(src, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                let amount = self.registers[src as usize] as u64;

                if let Some(error) = self.stack.extend_frame(amount) { 
                    return Err(VmError::StackError{ error });
                }

                self.ins_ptr += 2;
            }

            //
            //  LOAD (Memory Core) Instruction - Get data from the memory core
            // 
            INS_LOAD => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_LOAD", self.id).as_str());

                let size_reg : u8;
                let pos_reg  : u8;
                ret_mem_byte!(size_reg, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(pos_reg , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("LOAD  Size {} | Pos {} ", self.registers[size_reg as usize], self.registers[pos_reg as usize]).as_str());

                
                let size = self.registers[size_reg as usize] as u32;
                let pos  = self.registers[pos_reg  as usize] as u32;

                if let Some(data) = self.memory_core.get(&pos) {
                    let mut amount_loaded : u32 = 0;
                    'load_data: for data_byte in data.iter().rev() {
                        if amount_loaded < size {
                            if let Err(e) = self.stack.push_byte(data_byte.clone()) {
                                return Err(VmError::StackError{ error: e });
                            } else {
                                amount_loaded += 1;
                            }
                        } else {
                            break 'load_data;
                        }
                    }
                }

                self.ins_ptr += 3;
            }

            //
            //  STORE (Memory Core) Instruction - Store data to the memory core
            // 
            INS_STORE => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_STORE", self.id).as_str());

                let size_reg : u8;
                let pos_reg  : u8;
                ret_mem_byte!(size_reg, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(pos_reg , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("STORE  Size {} | Pos {} ", self.registers[size_reg as usize], self.registers[pos_reg as usize]).as_str());

                
                let size = self.registers[size_reg as usize] as u32;
                let pos  = self.registers[pos_reg  as usize] as u32;

                let mut amount_stored : u32 = 0;
                let mut staged_data: Vec<u8> = Vec::new();
                'store_data: for _ in 0..size {
                    if amount_stored < size {

                        match self.stack.pop_byte() {

                            Ok(data_to_put_in_mc) => {
                                staged_data.push(data_to_put_in_mc.clone());
                                amount_stored += 1;
                            }

                            Err(e) => {
                                return Err(VmError::StackError{ error: e });
                            }
                        }
                    } else {
                        break 'store_data;
                    }
                }

                self.memory_core.insert(pos, staged_data);

                self.ins_ptr += 3;
            }

            //
            //  LDB Instruction
            //  
            INS_LDB  => {   
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_LDB", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("LDB  Src 'r{}' => Val = {} | Dest 'r{}' ", src, self.registers[src as usize], dest).as_str());

                let value : u8;
                ret_mem_byte!(value, self, self.registers[src as usize] as usize, VmError::FailedToRetrieveValueForLoadInstruction);
        
                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 3;

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("Result :'{}' ", self.registers[dest as usize]).as_str());
            }

            //
            //  LDQ Instruction
            //  
            INS_LDQ  => {   
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_LDQ", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("LDQ  Src 'r{}' => Val = {} | Dest 'r{}' ", src, self.registers[src as usize], dest).as_str());

                let value : u16;
                ret_mem_q!(value, self, self.registers[src as usize] as usize, VmError::FailedToRetrieveValueForLoadInstruction);
        
                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 3;

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("Result :'{}' ", self.registers[dest as usize]).as_str());
            }

            //
            //  LDH Instruction
            //  
            INS_LDH  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_LDH", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("LDH  Src 'r{}' => Val = {} | Dest 'r{}' ", src, self.registers[src as usize], dest).as_str());

                let value : u32;
                ret_mem_h!(value, self, self.registers[src as usize] as usize, VmError::FailedToRetrieveValueForLoadInstruction);
        
                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 3;

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("Result :'{}' ", self.registers[dest as usize]).as_str());
            }

            //
            //  LDW Instruction
            //  
            INS_LDW  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_LDW", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("LDW  Src 'r{}' => Val = {} | Dest 'r{}' ", src, self.registers[src as usize], dest).as_str());

                let value : u64;
                ret_mem_word!(value, self, self.registers[src as usize] as usize, VmError::FailedToRetrieveValueForLoadInstruction);
        
                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 3;

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("Result :'{}' ", self.registers[dest as usize]).as_str());
            }

            //
            //  STB Instruction
            //  
            INS_STB  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_STB", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("STB  Src 'r{}'| Dest 'r{}'  => Val = {}  ", src, dest, self.registers[dest as usize]).as_str());

                store_mem_byte!(self.registers[src as usize] as u8,
                                self,
                                self.registers[dest as usize] as usize,
                                VmError::UnableToStoreValue);
                self.ins_ptr += 3;
            }

            //
            //  STQ Instruction
            //  
            INS_STQ  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_STQ", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("STQ  Src 'r{}'| Dest 'r{}'  => Val = {}  ", src, dest, self.registers[dest as usize]).as_str());

                store_mem_q!(self.registers[src as usize] as u16,
                                self,
                                self.registers[dest as usize] as usize,
                                VmError::UnableToStoreValue);
                self.ins_ptr += 3;
            }

            //
            //  STH Instruction
            //  
            INS_STH  => {   
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_STH", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("STH  Src 'r{}'| Dest 'r{}'  => Val = {}  ", src, dest, self.registers[dest as usize]).as_str());

                store_mem_h!(self.registers[src as usize] as u32,
                                self,
                                self.registers[dest as usize] as usize,
                                VmError::UnableToStoreValue);
                self.ins_ptr += 3;
            }

            //
            //  STW Instruction
            //  
            INS_STW  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_STW", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("STW  Src 'r{}'| Dest 'r{}'  => Val = {}  ", src, dest, self.registers[dest as usize]).as_str());

                store_mem_word!(self.registers[src as usize] as u64,
                                self,
                                self.registers[dest as usize] as usize,
                                VmError::UnableToStoreValue);
                self.ins_ptr += 3;
            }

            //
            //  LDSB Instruction
            //  
            INS_LDSB  => {   
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_LDSB", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("LDSB  Src 'r{}' => Val = {} | Dest 'r{}' ", src, self.registers[src as usize], dest).as_str());

                let value : u8;
                ret_stack_byte!(value, self, self.registers[src as usize] as usize, VmError::FailedToRetrieveValueForLoadInstruction);

        
                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 3;

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("Result :'{}' ", self.registers[dest as usize]).as_str());
            }

            //
            //  LDSQ Instruction
            //  
            INS_LDSQ  => {   
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_LDSQ", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("LDSQ  Src 'r{}' => Val = {} | Dest 'r{}' ", src, self.registers[src as usize], dest).as_str());

                let value : u16;
                ret_stack_q!(value, self, self.registers[src as usize] as usize, VmError::FailedToRetrieveValueForLoadInstruction);
        
                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 3;

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("Result :'{}' ", self.registers[dest as usize]).as_str());
            }

            //
            //  LDSH Instruction
            //  
            INS_LDSH  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_LDSH", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("LDSH  Src 'r{}' => Val = {} | Dest 'r{}' ", src, self.registers[src as usize], dest).as_str());

                let value : u32;
                ret_stack_h!(value, self, self.registers[src as usize] as usize, VmError::FailedToRetrieveValueForLoadInstruction);
        
                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 3;

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("Result :'{}' ", self.registers[dest as usize]).as_str());
            }

            //
            //  LDSW Instruction
            //  
            INS_LDSW  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_LDSW", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                let value : u64;
                ret_stack_word!(value, self, self.registers[src as usize] as usize, VmError::FailedToRetrieveValueForLoadInstruction);
        
                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("LDSW  Src 'r{}' => Val = {} | Dest 'r{}' ", src, value, dest).as_str());


                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 3;

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("Result :'{}' ", self.registers[dest as usize]).as_str());
            }

            //
            //  STSB Instruction
            //  
            INS_STSB  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_STSB", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("STSB  Src 'r{}'| Dest 'r{}'  => Val = {}  ", src, dest, self.registers[dest as usize]).as_str());

                store_stack_byte!(self.registers[src as usize] as u8,
                                self,
                                self.registers[dest as usize] as usize,
                                VmError::UnableToStoreValue);
                self.ins_ptr += 3;
            }

            //
            //  STSQ Instruction
            //  
            INS_STSQ  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_STSQ", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("STSQ  Src 'r{}'| Dest 'r{}'  => Val = {}  ", src, dest, self.registers[dest as usize]).as_str());

                store_stack_q!(self.registers[src as usize] as u16,
                                self,
                                self.registers[dest as usize] as usize,
                                VmError::UnableToStoreValue);
                self.ins_ptr += 3;
            }

            //
            //  STSH Instruction
            //  
            INS_STSH  => {   
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_STSH", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("STSH  Src 'r{}'| Dest 'r{}'  => Val = {}  ", src, dest, self.registers[dest as usize]).as_str());

                store_stack_h!(self.registers[src as usize] as u32,
                                self,
                                self.registers[dest as usize] as usize,
                                VmError::UnableToStoreValue);
                self.ins_ptr += 3;
            }

            //
            //  STSW Instruction
            //  
            INS_STSW  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_STSW", self.id).as_str());

                let dest : u8;
                let src  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("STSW  Src 'r{}'| Dest 'r{}'  => Val = {}  ", src, dest, self.registers[src as usize]).as_str());

                store_stack_word!(self.registers[src as usize] as u64,
                                self,
                                self.registers[dest as usize] as usize,
                                VmError::UnableToStoreValue);
                self.ins_ptr += 3;
            }

            //
            //  MOV Instruction
            //  
            INS_MOV  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_MOV", self.id).as_str());

                let variant : u8;
                ret_mem_byte!(variant, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                match variant {
                    0 => {
                        let dest : u8;
                        let src  : u8;
                        ret_mem_byte!(dest, self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);
                        ret_mem_byte!(src , self, self.ins_ptr+3, VmError::UnableToRetrieveInstruction);
                        #[cfg(debug)]
                        vmed!(&V, vfunc!(), format!("MOV (variant 1) Src 'r{}' | Dest 'r{}' ", src, dest).as_str());

                        // Initiate move
                        self.registers[dest as usize] = self.registers[src as usize];

                        // Increase ins ptr
                        self.ins_ptr += 4;
                    }
                    1 => {

                        let dest : u8;
                        let src  : u64;
                        ret_mem_byte!(dest, self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);
                        ret_mem_word!(src,  self, self.ins_ptr+3, VmError::UnableToRetrieveInstruction);

                        #[cfg(debug)]
                        vmed!(&V, vfunc!(), format!("MOV (variant 2) Val '{}' | Dest 'r{}' ", src, dest).as_str());
                        
                        // Initiate move
                        self.registers[dest as usize] = src as i64;

                        // Increase ins ptr
                        self.ins_ptr += 11;
                    }
                    _ => {
                        #[cfg(debug)]
                        vhigh!(&V, vfunc!(), format!("Unrecognized MOV variant : {}", variant).as_str());

                        
                        return Err(VmError::UnrecognizedInstruction);
                    }
                }

            }

            //
            //  PUSHW Instruction
            //  
            INS_PUSHW => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_PUSHW", self.id).as_str());
                
                let src : u8;
                ret_mem_byte!(src, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Push w 'r{}' with value '{}' ", src, self.registers[src as usize]).as_str());

                if let Err(e) = self.stack.push_word(self.registers[src as usize] as u64) {
                    return Err(VmError::StackError{ error: e });
                }

                self.ins_ptr += 2;
            }

            //
            //  POPW Instruction
            //  
            INS_POPW  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_POPW", self.id).as_str());
                
                let dest : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                let value = match self.stack.pop_word() {
                    Ok(v) => v,
                    Err(e)    => { 
                        return Err(VmError::StackError{ error : e }); 
                    }
                };

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Pop w into 'r{}' with value '{}' ", dest, value).as_str());

                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 2;
            }

            //
            //  PUSHB Instruction
            //  
            INS_PUSHB => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_PUSHB", self.id).as_str());
                
                let src : u8;
                ret_mem_byte!(src, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Push b 'r{}' with value '{}' ", src, self.registers[src as usize]).as_str());

                if let Err(e) = self.stack.push_byte(self.registers[src as usize] as u8)  {
                    return Err(VmError::StackError{ error: e });
                }


                self.ins_ptr += 2;
            }

            //
            //  POPB Instruction
            //  
            INS_POPB  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_POPB", self.id).as_str());
                
                let dest : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                let value = match self.stack.pop_byte() {
                    Ok(v) => v,
                    Err(e)    => { 
                        return Err(VmError::StackError{ error : e }); 
                    }
                };

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Pop b into 'r{}' with value '{}' ", dest, value).as_str());

                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 2;
            }

            //
            //  PUSHQ Instruction
            //  
            INS_PUSHQ => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_PUSHQ", self.id).as_str());
                
                let src : u8;
                ret_mem_byte!(src, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Push q 'r{}' with value '{}' ", src, self.registers[src as usize]).as_str());

                if let Err(e) = self.stack.push_quarter(self.registers[src as usize] as u16)  {
                    return Err(VmError::StackError{ error: e });
                }


                self.ins_ptr += 2;
            }

            //
            //  POPQ Instruction
            //  
            INS_POPQ  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_POPQ", self.id).as_str());
                
                let dest : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                let value = match self.stack.pop_quarter() {
                    Ok(v) => v,
                    Err(e)    => { 
                        return Err(VmError::StackError{ error : e }); 
                    }
                };

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Pop q into 'r{}' with value '{}' ", dest, value).as_str());

                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 2;
            }

            //
            //  PUSHH Instruction
            //  
            INS_PUSHH => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_PUSHH", self.id).as_str());
                
                let src : u8;
                ret_mem_byte!(src, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Push h 'r{}' with value '{}' ", src, self.registers[src as usize]).as_str());

                if let Err(e) = self.stack.push_half(self.registers[src as usize] as u32) {
                    return Err(VmError::StackError{ error: e });
                }

                self.ins_ptr += 2;
            }

            //
            //  POPH Instruction
            //  
            INS_POPH  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_POPH", self.id).as_str());
                
                let dest : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                let value = match self.stack.pop_half() {
                    Ok(v) => v,
                    Err(e)    => { 
                        return Err(VmError::StackError{ error : e }); 
                    }
                };

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Pop into 'r{}' with value '{}' ", dest, value).as_str());

                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 2;
            }

            //
            //  SIZE Instruction
            //  
            INS_SIZE => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_SIZE", self.id).as_str());

                let dest : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                let value = self.stack.size();

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Size into 'r{}' with value '{}' ", dest, value).as_str());

                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 2;
            }

            //
            //  CAP Instruction
            //  
            INS_CAP  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_CAP", self.id).as_str());

                let dest : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                let value = self.stack.cap();

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Cap into 'r{}' with value '{}' ", dest, value).as_str());

                self.registers[dest as usize] = value as i64;

                self.ins_ptr += 2;
            }

            //
            //  MS Instruction
            //  
            INS_MS  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_MS", self.id).as_str());

                self.stack.mark();

                self.ins_ptr += 1;
            }

            //
            //  RS Instruction
            //  
            INS_RS  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_RS", self.id).as_str());

                self.stack.reset();

                self.ins_ptr += 1;
            }

            //
            //  LSH Instruction
            //  
            INS_LSH  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_LSH", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize] << self.registers[rhs as usize];


                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("LSH 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  RSH Instruction
            //  
            INS_RSH  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_RSH", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize] >> self.registers[rhs as usize];

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("RSH 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  AND Instruction
            //  
            INS_AND  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_AND", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize] & self.registers[rhs as usize];

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("AND 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  OR Instruction
            //  
            INS_OR   => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_OR", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize] | self.registers[rhs as usize];

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("OR 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  XOR Instruction
            //  
            INS_XOR  => { 
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_XOR", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize] ^ self.registers[rhs as usize];

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("XOR 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  NOT Instruction
            //  
            INS_NOT  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_NOT", self.id).as_str());

                let dest : u8;
                let src  : u8;

                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(src,  self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                self.registers[dest as usize] = ! self.registers[src as usize];

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("NOT 'r{}' into 'r{}' | Val = {} ", src, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 3;
            }

            //
            //  ADD Instruction
            //  
            INS_ADD  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_ADD", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize] + self.registers[rhs as usize];

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Add 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  SUB Instruction
            //  
            INS_SUB  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_SUB", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize] - self.registers[rhs as usize];

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Sub 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  MUL Instruction
            //  
            INS_MUL  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_MUL", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize] * self.registers[rhs as usize];

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Mul 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  DIV Instruction
            //  
            INS_DIV  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_DIV", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize] / self.registers[rhs as usize];

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Div 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  POW Instruction
            //  
            INS_POW  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_POW", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize].pow(self.registers[rhs as usize] as u32);

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Pow 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  MOD Instruction
            //  
            INS_MOD  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_MOD", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);
                self.registers[dest as usize] = self.registers[lhs as usize] % self.registers[rhs as usize];

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Mod 'r{}' by 'r{}' into 'r{}' | Val = {} ", lhs, rhs, dest, self.registers[dest as usize]).as_str());

                self.ins_ptr += 4;
            }

            //
            //  ADDF Instruction
            //  
            INS_ADDF => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_ADDF", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);

                let f_lhs : f64 = f64::from_le_bytes( self.registers[lhs as usize].to_le_bytes() );
                let f_rhs : f64 = f64::from_le_bytes( self.registers[rhs as usize].to_le_bytes() );
                let f_result = f_lhs + f_rhs;

                self.registers[dest as usize] = i64::from_le_bytes( f_result.to_ne_bytes() );

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Addf 'r{}' ({}) by 'r{}' ({}) into 'r{}' | Val = {} ", lhs, f_lhs, rhs, f_rhs, dest, f_result).as_str());

                self.ins_ptr += 4;
            }

            //
            //  SUBF Instruction
            //  
            INS_SUBF => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_SUBF", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);

                let f_lhs : f64 = f64::from_le_bytes( self.registers[lhs as usize].to_le_bytes() );
                let f_rhs : f64 = f64::from_le_bytes( self.registers[rhs as usize].to_le_bytes() );
                let f_result = f_lhs - f_rhs;

                self.registers[dest as usize] = i64::from_le_bytes( f_result.to_ne_bytes() );

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Subf 'r{}' ({}) by 'r{}' ({}) into 'r{}' | Val = {} ", lhs, f_lhs, rhs, f_rhs, dest, f_result).as_str());

                self.ins_ptr += 4;
            }

            //
            //  MULF Instruction
            //  
            INS_MULF => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_MULF", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);

                let f_lhs : f64 = f64::from_le_bytes( self.registers[lhs as usize].to_le_bytes() );
                let f_rhs : f64 = f64::from_le_bytes( self.registers[rhs as usize].to_le_bytes() );
                let f_result = f_lhs * f_rhs;

                self.registers[dest as usize] = i64::from_le_bytes( f_result.to_ne_bytes() );

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Mulf 'r{}' ({}) by 'r{}' ({}) into 'r{}' | Val = {} ", lhs, f_lhs, rhs, f_rhs, dest, f_result).as_str());

                self.ins_ptr += 4;
            }

            //
            //  DIVF Instruction
            //  
            INS_DIVF => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_DIVF", self.id).as_str());

                let dest : u8;
                let lhs  : u8;
                let rhs  : u8;
                three_reg!(self, dest, lhs, rhs);

                let f_lhs : f64 = f64::from_le_bytes( self.registers[lhs as usize].to_le_bytes() );
                let f_rhs : f64 = f64::from_le_bytes( self.registers[rhs as usize].to_le_bytes() );
                let f_result = f_lhs / f_rhs;

                self.registers[dest as usize] = i64::from_le_bytes( f_result.to_ne_bytes() );

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("Divf 'r{}' ({}) by 'r{}' ({}) into 'r{}' | Val = {} ", lhs, f_lhs, rhs, f_rhs, dest, f_result).as_str());

                self.ins_ptr += 4;
            }

            //
            //  BLT Instruction
            //  
            INS_BLT  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_BLT", self.id).as_str());

                let lhs : u8;
                let rhs : u8;
                let addr: u64;

                branch_items!(self, lhs, rhs, addr);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("BLT 'r{}' ({}) < 'r{}' ({}) goto idx : {} ", 
                        lhs, 
                        self.registers[lhs as usize],
                        rhs, 
                        self.registers[rhs as usize],
                        addr
                    ).as_str());


                if self.registers[lhs as usize] < self.registers[rhs as usize] {

                    #[cfg(debug)]
                    vlow!(&V, vfunc!(), ">EXECUTE BRANCH<");

                    self.ins_ptr = addr as usize;
                } else {

                    self.ins_ptr += 11;
                }
            }

            //
            //  BGT Instruction
            //  
            INS_BGT  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_BGT", self.id).as_str());

                let lhs : u8;
                let rhs : u8;
                let addr: u64;

                branch_items!(self, lhs, rhs, addr);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("BGT 'r{}' ({}) > 'r{}' ({}) goto idx : {} ", 
                        lhs, 
                        self.registers[lhs as usize],
                        rhs, 
                        self.registers[rhs as usize],
                        addr
                    ).as_str());


                if self.registers[lhs as usize] > self.registers[rhs as usize] {

                    #[cfg(debug)]
                    vlow!(&V, vfunc!(), ">EXECUTE BRANCH<");

                    self.ins_ptr = addr as usize;
                } else {

                    self.ins_ptr += 11;
                }
            }

            //
            //  BEQ Instruction
            //  
            INS_BEQ  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_BEQ", self.id).as_str());

                let lhs : u8;
                let rhs : u8;
                let addr: u64;

                branch_items!(self, lhs, rhs, addr);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("BEQ 'r{}' ({}) == 'r{}' ({}) goto idx : {} ", 
                        lhs, 
                        self.registers[lhs as usize],
                        rhs, 
                        self.registers[rhs as usize],
                        addr
                    ).as_str());


                if self.registers[lhs as usize] == self.registers[rhs as usize] {

                    #[cfg(debug)]
                    vlow!(&V, vfunc!(), ">EXECUTE BRANCH<");

                    self.ins_ptr = addr as usize;
                } else {

                    self.ins_ptr += 11;
                }
            }

            //
            //  BNE Instruction
            //  
            INS_BNE  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_BNE", self.id).as_str());

                let lhs : u8;
                let rhs : u8;
                let addr: u64;

                branch_items!(self, lhs, rhs, addr);

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("BNE 'r{}' ({}) != 'r{}' ({}) goto idx : {} ", 
                        lhs, 
                        self.registers[lhs as usize],
                        rhs, 
                        self.registers[rhs as usize],
                        addr
                    ).as_str());


                if self.registers[lhs as usize] != self.registers[rhs as usize] {

                    #[cfg(debug)]
                    vlow!(&V, vfunc!(), ">EXECUTE BRANCH<");

                    self.ins_ptr = addr as usize;
                } else {

                    self.ins_ptr += 11;
                }
            }

            //
            //  BLTF Instruction
            //  
            INS_BLTF => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_BLTF", self.id).as_str());

                let lhs : u8;
                let rhs : u8;
                let addr: u64;

                branch_items!(self, lhs, rhs, addr);

                let f_lhs : f64 = f64::from_le_bytes( self.registers[lhs as usize].to_le_bytes() );
                let f_rhs : f64 = f64::from_le_bytes( self.registers[rhs as usize].to_le_bytes() );

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("BLTF 'r{}' ({}) < 'r{}' ({}) goto idx : {} ", 
                        lhs, 
                        f_lhs,
                        rhs, 
                        f_rhs,
                        addr
                    ).as_str());


                if f_lhs < f_rhs {

                    #[cfg(debug)]
                    vlow!(&V, vfunc!(), ">EXECUTE BRANCH<");

                    self.ins_ptr = addr as usize;
                } else {

                    self.ins_ptr += 11;
                }
            }

            //
            //  BGTF Instruction
            //  
            INS_BGTF => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_BGTF", self.id).as_str());

                let lhs : u8;
                let rhs : u8;
                let addr: u64;

                branch_items!(self, lhs, rhs, addr);

                let f_lhs : f64 = f64::from_le_bytes( self.registers[lhs as usize].to_le_bytes() );
                let f_rhs : f64 = f64::from_le_bytes( self.registers[rhs as usize].to_le_bytes() );

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("BGTF 'r{}' ({}) > 'r{}' ({}) goto idx : {} ", 
                        lhs, 
                        f_lhs,
                        rhs, 
                        f_rhs,
                        addr
                    ).as_str());


                if f_lhs > f_rhs {

                    #[cfg(debug)]
                    vlow!(&V, vfunc!(), ">EXECUTE BRANCH<");

                    self.ins_ptr = addr as usize;
                } else {

                    self.ins_ptr += 11;
                }
            }

            //
            //  BEQF Instruction
            //  
            INS_BEQF => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_BEQF", self.id).as_str());

                let lhs : u8;
                let rhs : u8;
                let addr: u64;

                branch_items!(self, lhs, rhs, addr);

                let f_lhs : f64 = f64::from_le_bytes( self.registers[lhs as usize].to_le_bytes() );
                let f_rhs : f64 = f64::from_le_bytes( self.registers[rhs as usize].to_le_bytes() );

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("BEQF 'r{}' ({}) == 'r{}' ({}) goto idx : {} ", 
                        lhs, 
                        f_lhs,
                        rhs, 
                        f_rhs,
                        addr
                    ).as_str());


                if f_lhs == f_rhs {

                    #[cfg(debug)]
                    vlow!(&V, vfunc!(), ">EXECUTE BRANCH<");

                    self.ins_ptr = addr as usize;
                } else {

                    self.ins_ptr += 11;
                }
            }

            //
            //  BNEF Instruction
            //  
            INS_BNEF => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_BNEF", self.id).as_str());

                let lhs : u8;
                let rhs : u8;
                let addr: u64;

                branch_items!(self, lhs, rhs, addr);

                let f_lhs : f64 = f64::from_le_bytes( self.registers[lhs as usize].to_le_bytes() );
                let f_rhs : f64 = f64::from_le_bytes( self.registers[rhs as usize].to_le_bytes() );

                #[cfg(debug)]
                vmed!(&V, 
                        vfunc!(), 
                        format!("BNEF 'r{}' ({}) != 'r{}' ({}) goto idx : {} ", 
                        lhs, 
                        f_lhs,
                        rhs, 
                        f_rhs,
                        addr
                    ).as_str());


                if f_lhs != f_rhs {

                    #[cfg(debug)]
                    vlow!(&V, vfunc!(), ">EXECUTE BRANCH<");

                    self.ins_ptr = addr as usize;
                } else {

                    self.ins_ptr += 11;
                }
            }

            //
            //  DIRQ Instruction
            //
            INS_DIRQ => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_DIRQ", self.id).as_str());

                self.interrupts_enabled = false;
                self.ins_ptr += 1;
            }

            //
            //  EIRQ Instruction
            //
            INS_EIRQ => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_EIRQ", self.id).as_str());

                self.interrupts_enabled = true;
                self.ins_ptr += 1;
            }

            //
            //  JMP Instruction
            //  
            INS_JMP  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_JMP", self.id).as_str());

                let variant : u8;
                ret_mem_byte!(variant, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                match variant {
                    0 => {
                        // Variant 1 - Register
                        #[cfg(debug)]
                        vhigh!(&V, vfunc!(), "JMP Variant '1'");

                        let src : u8;
                        ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);
                        let addr = self.registers[src as usize] as usize;
                        if let Err(e) = self.jmp_stack.push_word(self.ins_ptr as u64 + 3) {
                            return Err(VmError::JumpStackError{ error: e});
                        }
                        self.ins_ptr = addr as usize;
                    } 
                    1 => {
                        // Variant 2 - Label
                        #[cfg(debug)]
                        vhigh!(&V, vfunc!(), "JMP Variant '2'");

                        let addr : u64;
                        ret_mem_word!(addr, self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);
                        if let Err(e) = self.jmp_stack.push_word(self.ins_ptr as u64 + 10) {
                            return Err(VmError::JumpStackError{ error: e});
                        }
                        self.ins_ptr = addr as usize;
                    }
                    _ => {
                        #[cfg(debug)]
                        vhigh!(&V, vfunc!(), format!("Unrecognized JMP variant : {}", variant).as_str());
                        return Err(VmError::UnrecognizedInstruction);
                    }
                }
            }

            //
            //  GOTO Instruction
            //  
            INS_GOTO  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_GOTO", self.id).as_str());

                let variant : u8;
                ret_mem_byte!(variant, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                match variant {
                    0 => {
                        // Variant 1 - Register
                        #[cfg(debug)]
                        vhigh!(&V, vfunc!(), "GOTO Variant '1'");

                        let src : u8;
                        ret_mem_byte!(src , self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                        let addr = self.registers[src as usize] as usize;
                        self.ins_ptr = addr as usize;
                    } 
                    1 => {
                        // Variant 2 - Label
                        #[cfg(debug)]
                        vhigh!(&V, vfunc!(), "GOTO Variant '2'");

                        let addr : u64;
                        ret_mem_word!(addr, self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);
                        self.ins_ptr = addr as usize;
                    }
                    _ => {
                        #[cfg(debug)]
                        vhigh!(&V, vfunc!(), format!("Unrecognized GOTO variant : {}", variant).as_str());
                        return Err(VmError::UnrecognizedInstruction);
                    }
                }
            }

            //
            //  RET Instruction
            //  
            INS_RET  => {   
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_RET", self.id).as_str());

                match self.jmp_stack.pop_word() {
                    Ok(val) => {
                        #[cfg(debug)]
                        vlow!(&V, vfunc!(), format!("Returning to '{}'", val).as_str());

                        self.ins_ptr = val as usize
                    }
                    Err(_) => {
                        #[cfg(debug)]
                        vlow!(&V, vfunc!(), "JMP Stack empty - Exiting");
                        return Ok(true);
                    }
                }
            }

            //
            //  EXIT Instruction
            //  
            INS_EXIT => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_EXIT", self.id).as_str());
                
                if let Err(_) = self.vm_comm.try_send(ProcToVmComm::KillProc{ id: self.id.clone() } ) {
                    return Err(VmError::FailedToRequestProcess)
                }
                return Ok(true);
            }

            //
            //  HALT Instruction
            //  
            INS_HALT => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_HALT", self.id).as_str());
                
                if let Err(_) = self.vm_comm.try_send(ProcToVmComm::Halt{ id: self.id.clone() } ) {
                    return Err(VmError::FailedToRequestProcess)
                }
                return Ok(true);
            }

            //
            //  DBP Instruction
            //  
            INS_DBP => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_DBP", self.id).as_str());
                
                match &self.debug_channel {
                    Some(chan) => {
                        if let Err(_) = chan.send(DbpOutput{
                            ins_ptr: self.ins_ptr.clone(),
                            registers: self.registers.clone()
                        }) { /* Ignore the error */ }
                    }
                    None => {} 
                }
                self.ins_ptr += 1;
            }

            //
            //  PROC Instruction
            //  
            INS_PROC  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_PROC", self.id).as_str());

                let addr : u64;
                ret_mem_word!(addr, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                self.ins_ptr  += 9;

                if let Err(_) = self.vm_comm.try_send(ProcToVmComm::SpawnProc{ id: self.id.clone(), addr: addr as usize} ) {
                    return Err(VmError::FailedToRequestProcess)
                }
            }

            //
            //  ASEQ Instruction
            //  
            INS_ASEQ  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_ASEQ", self.id).as_str());

                let lhs : u8;
                let rhs  : u8;

                ret_mem_byte!(lhs, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(rhs,  self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("ASEQ 'r{}' == 'r{}'", lhs, rhs).as_str());

                if self.registers[lhs as usize] != self.registers[rhs as usize] {

                    let message = format!("ASEQ Failed! r{} != r{} >> {} != {}", lhs, rhs, self.registers[lhs as usize], self.registers[rhs as usize]).to_string();
                    
                    if let Err(_) = self.vm_comm.try_send(ProcToVmComm::Panic{ id: self.id.clone(), message} ) {
                        return Err(VmError::FailedToRequestProcess)
                    }
                }

                self.ins_ptr += 3;
            }

            //
            //  ASNE Instruction
            //  
            INS_ASNE  => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_ASNE", self.id).as_str());

                let lhs : u8;
                let rhs  : u8;

                ret_mem_byte!(lhs, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);
                ret_mem_byte!(rhs,  self, self.ins_ptr+2, VmError::UnableToRetrieveInstruction);

                #[cfg(debug)]
                vmed!(&V, vfunc!(), format!("ASNE 'r{}' != 'r{}'", lhs, rhs).as_str());

                if self.registers[lhs as usize] == self.registers[rhs as usize] {

                    let message = format!("ASNE Failed! r{} == r{} >> {} == {}", lhs, rhs, self.registers[lhs as usize], self.registers[rhs as usize]).to_string();
                    
                    if let Err(_) = self.vm_comm.try_send(ProcToVmComm::Panic{ id: self.id.clone(), message} ) {
                        return Err(VmError::FailedToRequestProcess)
                    }
                }

                self.ins_ptr += 3;
            }

            //
            //  WAIT Instruction
            //  
            INS_WAIT => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_WAIT", self.id).as_str());

                let src  : u8;
                ret_mem_byte!(src, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                self.ins_ptr += 2;

                let time = self.registers[src as usize] as u64;
                
                thread::sleep(time::Duration::from_millis(time));
            }

            //
            //  BMS Instruction
            //  
            INS_BMS => {
                #[cfg(debug)]
                vlow!(&V, vfunc!(), format!("(pid: {}) INS_BMS", self.id).as_str());

                let dest  : u8;
                ret_mem_byte!(dest, self, self.ins_ptr+1, VmError::UnableToRetrieveInstruction);

                self.ins_ptr += 2;

                self.registers[dest as usize] = self.ins_end as i64;   
            }

            //
            //  INVALID Instruction(s)
            //  
            _ => {

                #[cfg(debug)]
                {
                    vlow!(&V, vfunc!(), format!("Unrecognized instruction : {:X}", ins_byte).as_str());
                }
                
                return Err(VmError::UnrecognizedInstruction);
            }
        }

        Ok(false)
    }
}