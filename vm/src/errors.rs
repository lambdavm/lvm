use derive_more::{Display};
use crate::memory::MemoryError;

/// All of the errors that can take place within the VM
#[derive(Display)]
pub enum VmError {
    InvalidBytecodeFile,
    GivenMemoryCapacityInsufficient,
    UnableToRetrieveInstruction,
    UnableToStoreValue,
    UnrecognizedInstruction,
    FailedToRetrieveValueForLoadInstruction,
    StackError{ error: MemoryError },
    JumpStackError{ error: MemoryError },
    UnknownDeviceId,
    InsufficientProcessors,
    InvalidProcessorId,
    FailedToRequestProcess,
    MemoryAccessError,
    ProcessAccessError,
}

/// All of the errors that can occur because of device setup
#[derive(Display)]
pub enum VmDeviceError {
    InvalidRegistrationId,
    NoDeviceSlotsAvailable,

}